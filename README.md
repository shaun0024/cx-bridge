# CX Bridge

## Description
This solution was developed as an add-on to overcome some of the limitations when using Amazon Connect and Amazon Lex when requiring integration with external channels.

With this add-on, Amazon Connect and Amazon Lex can:
- be integrated with Facebook Messenger, Slack and Microsoft Teams
- be extended to use other channels
- be connected to multiple channels through a single API endpoint
- support for bi-directional attachments to be sent
- support for quick replies (e.g. buttons) to be sent to simplify and standardise response from users



## Solution Overview
The diagram below is a high level illustration of the solution design.
![Architecture](docs/imgs/high-level-architecture.png)

The following is a description of the actions taken during each step:

1. A supported channel is configured to send events (i.e. direct messages) to the ```API Gateway```.
2. The ```API Gateway``` forwards the event (i.e. inbound messages) to the ```API Responder Lambda```.
3. The ```API Responder Lambda``` queries the ```Config DynamoDB Table``` to determine if the source (e.g. Facebook Messenger app) is an authorised channel and rejects the event if it is not.
4. The ```API Responder Lambda``` obtains the credentials for authorised channels from the ```Secrets Manager``` and performs verification (e.g. event header verification).
5. If verification succeeds, the event is transformed in a message and placed into the ```Chat Message SQS Queue```. Generally, items in the queue are inbound messages (i.e. from the originating channel to the target platform) but for specific platforms (e.g. Amazon Connect), outbound messages may also be be queued. See steps 11 to 13.
6. The items in the queue are processed by the ```Message Processor Lambda```.
7. If the message being processed is inbound and is the first message of the session sent by the user, the ```Message Processor Lambda``` obtains the credentials for authorised channels from ```Secrets Manager``` to gather basic information about the user from the originating channel (e.g. first name, last name, user id). This will be stored as session attributes in Lex and Connect which can be used for personalisation. If the message is an outbound message (i.e. a reply from the target platform to the originating channel), the credentials for authorised channels will also be obtained from ```Secrets Manager``` to send the reply to the user.
8. Inbound messages will be sent to the target platform.
9. The reply (i.e outbound message) will be transformed by the ```Message Processor Lambda``` into the format required by the originating channel and sent back to the user.
10. An item is created or updated in the ```Active Sessions DynamoDB Table``` with attributes (e.g. message receipts, session duration, etc). Each item includes a TTL attribute.
11. **For Amazon Connect:** Once a contact has been established, a chat stream is created and events (e.g. outbound messages, participant events, typing events, etc.) are streamed to a ```Chat Stream SNS Topic```.
12. **For Amazon Connect:** The events in the topic trigger the ```Chat Stream Processor Lambda```.
13. **For Amazon Connect:** The ```Chat Stream Processor Lambda``` which processes and formats outbound messages and places them into the ```Chat Message SQS Queue```.
14. When the item in the ```Active Sessions DynamoDB Table``` has expired (i.e. the TTL), the item will be deleted.
15. The deletion event will trigger the ```End Sessions Lambda``` that will end the session in the supported platform.


## LIMITATIONS
Currently, all platforms (e.g. Amazon Connect and Amazon Lex) must be in the same account as the CX Bridge.


## IMPORTANT NOTES
This solution should not be used as is and needs to be customised for production environments. Some of the considerations that need to be taken include:

- Place a Web Application Firewall in front of the API Gateway.
- Use a custom domain and disable the default execution endpoint in the API Gateway.
- Include provisioned concurrency to the ```API Responder``` and ```Message Processor``` to improve the response times.
- Inspect the IAM policies and modify as necessary to further secure the solution.


## Deployment Guide
Please refer to [deployment guide](docs/01-deployment.md).