'use strict';

/**
 * MODULES AND OTHER DEPENDENCIES
 */
const session = require('./base/session');

/**
 * LOGGER
 */
const logger = require('pino')({
    'base': null,
    'timestamp': false,
    'name': 'MESSAGE_PROCESSOR', 
    'level': ('LOGGING_LEVEL' in process.env) ? process.env.LOGGING_LEVEL.toLowerCase() : 'info'
});

/**
 * MANDATORY ENVIRONMENT VARIABLE CHECKS
 */
if (
    !process.env.SESSIONS_TABLE || 
    !process.env.SESSIONS_TABLE_INDEX ||
    !process.env.CHAT_MESSAGE_QUEUE_URL
) throw 'One or more environment variables and/or parameters not defined';

/**
 * GLOBAL VARIABLES
 */
const sessionsTable = process.env.SESSIONS_TABLE;
const sessionsTableIndex = process.env.SESSIONS_TABLE_INDEX;



exports.handler = async (event) => {
    /**
     * Basic handler to forward events to logical functions.
     * 
     * @event {object} An SQS formatted event.
     * @return {null} Returns nothing.
     */

    if ('warmup' in event) return;

    const loggerBase = { 'operation': 'QUEUE_EVENT' };

    logger.debug({ 
        'event': event, 
        ...loggerBase 
    });

    await controller(event);

    return;
};



async function controller(event) {
    /**
     * Forwards SQS records from the event for further processing.
     * 
     * @event {object} An SQS formatted event.
     * @return {null} Returns nothing.
     */

    const loggerBase = { 'operation': 'CONTROLLER' };

    let sessionName, sqsRecord;
    for (let i = 0; i < event.Records.length; i++) {
        sessionName = event.Records[i].attributes.MessageGroupId;
        sqsRecord = JSON.parse(event.Records[i].body);

        logger.debug({ 
            'sqsRecord': sqsRecord, 
            ...loggerBase
        });

        (sqsRecord.direction == 'toPlatform') ? await sendMessageToPlatform(sessionName, sqsRecord) : await sendMessageToChannel(sessionName, sqsRecord);
    }
    
    return;
}


async function sendMessageToPlatform(sessionName, sqsRecord) {
    /**
     * Sends messages and attachments to the target platform (e.g. Amazon Connect, Lex).
     * 
     * @sessionName {string} The name of the session (i.e. SQS message group id).
     * @sqsRecord {object} The body of the SQS record that contains the config and message object.
     * @return {null} Returns nothing.
     */

    const loggerBase = { 
        'type': 'SEND_MESSAGE_TO_PLATFORM',
        'sessionName': sessionName
    };

    logger.debug({ 
        'sqsRecord': sqsRecord,
        ...loggerBase
    });

    const { config, message } = sqsRecord;

    const sessionData = await session.getSession(sessionsTable, sessionsTableIndex, 'sessionName', sessionName);

    logger.debug({ 
        'sessionData': sessionData,
        ...loggerBase
    });

    const platform = require(`./platform/${config.Platform.SubCategory}`);

    await platform.entrypoint(config, message, sessionName, sessionData);

    return;
}



async function sendMessageToChannel(sessionName, sqsRecord) {
    /**
     * Sends messages and attachments to the channel.
     * 
     * @sessionName {string} The name of the session (i.e. SQS message group id).
     * @sqsRecord {object} The body of the SQS record that contains the config and message object.
     * @return {string} Returns nothing.
     */

    const loggerBase = { 
        'type': 'SEND_MESSAGE_TO_CHANNEL',
        'sessionName': sessionName
    };

    logger.debug({ 
        'sqsRecord': sqsRecord, 
        ...loggerBase
    });

    const { config, message } = sqsRecord;

    const channel = require(`./channels/${config.Channel.SubCategory}`);

    const channelMessageId = await channel.outboundEntrypoint(config, message);

    if (channelMessageId == null) return;

    logger.info({ 
        'sessionName': sessionName, 
        'receipt': { 
            'messageIdFromChannel': channelMessageId, 
            'messageIdFromPlatform': message.MessageId 
        }, 
        ...loggerBase 
    });

    if (!message.SkipUpdate ) {
        try {
            const sessionExpiration = Math.round(Date.now() / 1000) + (60 * (config.Channel.Timeout - 1));

            await session.updateLastMessageSent(sessionsTable, sessionName, 'system', message.MessageId, sessionExpiration);
        } catch (error) {
            logger.warn({ 
                'error': error.stack, 
                ...loggerBase
            });
        }
        
    }

    return;
}