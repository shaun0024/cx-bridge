'use strict';

/**
 * MODULES AND OTHER DEPENDENCIES
 */
const { 
    ConnectClient, 
    StartTaskContactCommand, 
    StartChatContactCommand, 
    StartContactStreamingCommand, 
    StopContactCommand
} = require('@aws-sdk/client-connect');
const connect = new ConnectClient({ 'apiVersion': '2017-08-08' });
const { 
    ConnectParticipantClient, 
    CreateParticipantConnectionCommand, 
    SendMessageCommand, 
    GetAttachmentCommand, 
    StartAttachmentUploadCommand, 
    CompleteAttachmentUploadCommand 
} = require('@aws-sdk/client-connectparticipant');
const participant = new ConnectParticipantClient({ 'apiVersion': '2018-09-07' });
const axios = require('axios');
const WebSocket = require('ws');
const session = require('../base/session');

/**
 * LOGGER
 */
const logger = require('pino')({
    'base': null,
    'timestamp': false, 
    'level': ('LOGGING_LEVEL' in process.env) ? process.env.LOGGING_LEVEL.toLowerCase() : 'info'
}).child({ 'module': 'PLATFORM/CONNECT' });

/**
 * GLOBAL VARIABLES
 */
const maxAttachmentSize = 20000000;
const sessionsTable = process.env.SESSIONS_TABLE;



async function entrypoint(config, message, sessionName, sessionData, force = false) {
    /**
     * Processes inbound messages from AWS SQS to be sent to Amazon Connect.
     *
     * @config {object} Config object containing details of platform, the channel and integration.
     * @message {object} The message to be sent.
     * @sessionName {string} The message to be sent.
     * @sessionData {object} The session object containing details of the existing session.
     * @force {boolean} Optional. Forces a new session to be created.
     * @return {null} Returns nothing.
     */

    const loggerBase = { 
        'type': 'SEND_MESSAGE_TO_CONNECT', 
        'sessionName': sessionName 
    };

    logger.debug({
        'config': config,
        'message': message,
        'sessionData': sessionData,
        'force': force,
        ...loggerBase
    });

    let platformMessageId;

    switch(message.ContentType) {
        case ('email'):
        case ('task'):
            platformMessageId = await startTask(config, message);
            break;
        
        default:
            if (sessionData == null || force) {
                return await createSession(config, message, sessionName);
            } else {
                return await continueSession(config, message, sessionName, sessionData);
            }
    }
}



async function createSession(config, message, sessionName) {
    /**
     * Creates a new chat session in Amazon Connect.
     *
     * @config {object} Config object containing details of platform, the channel and integration.
     * @message {object} The message to be sent.
     * @sessionName {string} The message to be sent.
     * @return {null} Returns nothing.
     */

    const loggerBase = { 
        'type': 'CREATE_NEW_SESSION', 
        'sessionName': sessionName 
    };

    logger.debug({
        'config': config,
        'message': message,
        ...loggerBase
    });

    const channel = require(`../channels/${config.Channel.SubCategory}`);

    const patron = await channel.getName(config, message.Patron.ChannelId, null, message.Metadata);

    message.Patron = patron;

    const connectionData = await startChat(sessionName, config, message);

    logger.debug({
        'connectionData': connectionData,
        ...loggerBase
    });

    if (message.ContentType == 'attachment') {
        const attachment = await channel.downloadAttachment(config, message);
        await sendAttachment(sessionName, connectionData.ConnectionToken, attachment);
    }
}




async function continueSession(config, message, sessionName, sessionData) {
    /**
     * Continues using an existing chat session in Amazon Connect.
     *
     * @config {object} Config object containing details of platform, the channel and integration.
     * @message {object} The message to be sent.
     * @sessionName {string} The message to be sent.
     * @sessionData {object} The session object containing details of the existing session.
     * @return {null} Returns nothing.
     */

    const loggerBase = { 
        'type': 'CONTINUE_SESSION', 
        'sessionName': sessionName
    };

    logger.debug({
        'config': config,
        'sessionData': sessionData,
        'message': message,
        ...loggerBase
    });

    const channel = require(`../channels/${config.Channel.SubCategory}`);

    logger.debug({
        'currentMessageId': message.MessageId,
        'lastMessageId': sessionData.LastMessageId.FromChannel,
        'isDuplicate': (message.MessageId == sessionData.LastMessageId.FromChannel) ? true : false,
        ...loggerBase
    });

    if (message.MessageId == sessionData.LastMessageId.FromChannel) return;

    try {
        let platformMessageId, sessionExpiration;

        if (message.ContentType == 'text/plain') {
            platformMessageId = await sendMessage(sessionName, sessionData.ConnectionToken, message);
            if (!message.SkipUpdate) {
                try {
                    sessionExpiration = Math.round(Date.now() / 1000) + (60 * (config.Channel.Timeout - 1));
                    await session.updateLastMessageSent(sessionsTable, sessionName, 'customer', message.MessageId, sessionExpiration);
                } catch (error) {
                    logger.error({ 
                        'error': error.stack,
                        ...loggerBase
                    });
                }
            }
        }
    
        if (message.ContentType == 'attachment') {
            platformMessageId = await sendAttachment(sessionName, sessionData.ConnectionToken, await channel.downloadAttachment(config, message));
            if (!message.SkipUpdate) {
                try {
                    sessionExpiration = Math.round(Date.now() / 1000) + (60 * (config.Channel.Timeout - 1));
                    await session.updateLastMessageSent(sessionsTable, sessionName, 'customer', message.MessageId, sessionExpiration);
                } catch (error) {
                    logger.error({ 
                        'error': error.stack,
                        ...loggerBase
                    });
                }
            }
        }

        return platformMessageId;
    } catch (error) {
        logger.error({ 
            'error': error.stack,
            ...loggerBase
        });
        await entrypoint(config, message, sessionName, sessionData, true);
    }
}



async function startTask(config, message) {
    /**
     * Starts a task with Amazon Connect.
     * 
     * @config {object} Config object containing details of Amazon Connect, the channel and integration.
     * @message {object} The details of the task to be sent to Amazon Connect.
     * @return {string} Returns the contact id of the task.
     */

    const loggerBase = { 'operation': 'START_TASK' };    

    logger.debug({ 
        'config': config, 
        'message': message, 
        ...loggerBase 
    });

    const taskParams = {
        'InstanceId': config.Platform.InstanceId,
        'ContactFlowId': config.Channel.ContactFlowId,
        'Name': message.Patron.DisplayName,
        'Description': message.Content,
        'Attributes': {
            '_meta_Channel': config.Channel.SubCategory,
            '_meta_ChannelName': config.Channel.FriendlyName,
            '_meta_PatronChannelId': message.Patron.ChannelId,
            '_meta_PatronDisplayName': message.Patron.DisplayName
        },
        'References': message.Metadata
    };
    
    logger.debug({ 
        'taskParams': taskParams, 
        ...loggerBase
    });

    const response = await connect.send(new StartTaskContactCommand(taskParams));

    logger.debug({ 
        'response': response, 
        ...loggerBase
    });

    return response.ContactId;
}



async function startChat(sessionName, config, message, customAttributes = {}) {
    /**
     * Starts a new chat session with Amazon Connect.
     * 
     * @sessionName {string} The name of the session.
     * @config {object} Config object containing details of platform, the channel and integration.
     * @message {object} The message to be sent to Amazon Connect.
     * @customAttributes {object} Any additional key value pairs to be added as contact attributes.
     * @return {object} Returns an object containing the connection data (e.g. websocket url, tokens, etc).
     */

    async function createWebsocketConnection(websocketUrl, sessionName) {
        /**
         * Initiates and subscribes to the topic of the Amazon Connect chat session.
         * NOTE: Still required when using streaming chat, to start the session but 
         * the socket does not need to remain open. 
         * 
         * @websocketUrl {string} The URL of the websocket.
         * @sessionName {string} The name of the session.
         * @return {null} Does not return any values.
         */
    
        const wsClient = new WebSocket(websocketUrl);
    
        wsClient.on('open', async (event) => {
            const loggerBase = {
                'operation': 'WSCLIENT_ON_OPEN',
                'sessionName': sessionName
            };
    
            logger.debug({ 
                'event': event, 
                'websocketUrl': websocketUrl,
                ...loggerBase
            });
    
            wsClient.send(
                JSON.stringify({
                    'topic': 'aws/subscribe',
                    'content': { 
                        'topics': [ 'aws/chat' ] 
                    }
                })
            );
        });
    }

    const loggerBase = {
        'operation': 'START_CHAT',
        'sessionName': sessionName
    };
    
    logger.debug({
        'config': config,
        'message': message,
        'customAttributes': customAttributes,
        ...loggerBase
    });

    let initialMessage;
    if (message.ContentType == 'text/plain') {
        initialMessage = message.Content;
    }

    const chatParams = {
        'InstanceId': config.Platform.InstanceId,
        'ContactFlowId': config.Channel.ContactFlowId,
        'ParticipantDetails': {
            'DisplayName': message.Patron.DisplayName
        },
        'Attributes': {
            '_meta_Channel': config.Channel.SubCategory,
            '_meta_ChannelName': config.Channel.FriendlyName,
            '_meta_PatronChannelId': message.Patron.ChannelId,
            '_meta_PatronDisplayName': message.Patron.DisplayName,
            '_meta_PatronFirstName': message.Patron.FirstName,
            '_meta_PatronLastName': message.Patron.LastName,
            ...customAttributes
        }
    };

    if (initialMessage) chatParams.InitialMessage = { 'ContentType': 'text/plain', 'Content': initialMessage };

    if ('IntegrationName' in message.Patron && 'IntegrationId' in message.Patron) {
        chatParams.Attributes._meta_IntegrationName = message.Patron.IntegrationName;
        chatParams.Attributes._meta_PatronIntegrationId = message.Patron.IntegrationId;
        chatParams.Attributes._meta_PatronUrl = message.Patron.RecordUrl;
    }

    logger.debug({
        'chatParams': chatParams, 
        ...loggerBase 
    });

    // const participantData = await connect.startChatContact(chatParams).promise();
    const participantData = await connect.send(new StartChatContactCommand(chatParams));

    const connectionData = {
        'ContactId': participantData.ContactId,
        'ParticipantId': participantData.ParticipantId,
        'ParticipantToken': participantData.ParticipantToken
    };

    const sessionData = {
        'SessionName': sessionName,
        'ContactId': `${config.Platform.InstanceId}:${participantData.ContactId}`,
        'ParticipantId': participantData.ParticipantId,
        'ParticipantToken': participantData.ParticipantToken,
        'SessionExpiration': Math.round(Date.now() / 1000) + (60 * (config.Channel.Timeout - 1)),
        'Config': config
    };

    logger.debug({ 
        'connectionData': connectionData, 
        'sessionData': sessionData,
        ...loggerBase 
    });

    let websocketData;

    if (config.Channel.SubCategory != 'webchat') {
        const participantParams = {
            'ParticipantToken': participantData.ParticipantToken,
            'Type': [ 'WEBSOCKET', 'CONNECTION_CREDENTIALS' ]
        };

        logger.debug({ 
            'participantParams': participantParams, 
            ...loggerBase 
        });

        // websocketData = await participant.createParticipantConnection(participantParams).promise();
        websocketData = await participant.send(new CreateParticipantConnectionCommand(participantParams));

        logger.debug({ 
            'websocketData': websocketData, 
            ...loggerBase 
        });

        sessionData.Metadata = message.Metadata;
        sessionData.ConnectionToken = websocketData.ConnectionCredentials.ConnectionToken;
        sessionData.IsAgent = null;
        sessionData.LastMessageId = {
            'FromConnect': null,
            'FromChannel': message.MessageId
        };
    }

    await session.saveSession(sessionsTable, sessionName, sessionData);

    if (config.Channel.SubCategory != 'webchat') {
        await createWebsocketConnection(websocketData.Websocket.Url, sessionName);

        const streamingParams = { 
            'ChatStreamingConfiguration': { 
                'StreamingEndpointArn': config.Platform.SnsStreamArn 
            },
            'InstanceId': config.Platform.InstanceId,
            'ContactId': participantData.ContactId
        };

        logger.debug({ 
            'streamingParams': streamingParams, 
            ...loggerBase 
        });

        // const streamingId = await connect.startContactStreaming(streamingParams).promise();
        const streamingId = await connect.send(new StartContactStreamingCommand(streamingParams));

        logger.debug({ 
            'streamingId': streamingId, 
            ...loggerBase 
        });
    }

    return connectionData; 
}



// async function resumeChat(sessionName, participantToken) {
//     /**
//      * Continues an existing chat session with Amazon Connect by requestiong a new connection token.
//      * 
//      * @sessionName {string} The name of the session.
//      * @participantToken {object} Config object containing details of Amazon Connect, the channel and integration.
//      * @return {object} Returns an object containing the connection data (e.g. websocket url, tokens, etc).
//      */

//     const loggerBase = {
//         'operation': 'RESUME_CHAT',
//         'sessionName': sessionName,
//     };

//     try {
//         const participantParams = {
//             'ParticipantToken': participantToken,
//             'Type': [ 'WEBSOCKET', 'CONNECTION_CREDENTIALS' ]
//         };

//         const websocketData = await participant.createParticipantConnection(participantParams).promise();

//         logger.info({ 'status': 'SUCCESS', ...loggerBase });
//         logger.debug({ 'status': 'VERBOSE', 'websocketData': websocketData, ...loggerBase });

//         return { 
//             'WebsocketUrl': websocketData.Websocket.Url,
//             'ConnectionToken': websocketData.ConnectionCredentials.ConnectionToken,
//             // 'Expiry': websocketData.ConnectionCredentials.Expiry
//             'SessionExpiration': (new Date(websocketData.ConnectionCredentials.Expiry)).getTime() / 1000
//         };
//     } catch (error) {
//         logger.error({ 'status': 'FAILED', 'error': error, ...loggerBase });
//         throw error;
//     }
// }



async function endSession(sessionData) {
    /**
     * Ends an existing contact session with Amazon Connect based on the initial contact id.
     * 
     * @sessionData {object} The session object containing details of the existing session.
     * @return {null} Returns nothing.
     */

    const { SessionName, Config, ContactId } = sessionData;

    const loggerBase = {
        'operation': 'STOP_CHAT',
        'sessionName': SessionName,
    };

    const chatParams = {
        'InstanceId': Config.Platform.InstanceId,
        'ContactId': ContactId.split(':')[1]
    };

    logger.debug({ 
        'chatParams': chatParams, 
        ...loggerBase 
    });

    const response = await connect.send(new StopContactCommand(chatParams));

    logger.debug({  
        'response': response, 
        ...loggerBase 
    });

    return;
}



async function sendMessage(sessionName, connectionToken, message) {
    /**
     * Sends a message to the Amazon Connect chat session.
     * 
     * @sessionName {string} The name of the session.
     * @connectionToken {object} The connection token of the chat session.
     * @message {object} The message to be sent to Amazon Connect.
     * @return {string} Returns the receipt id of the message from Amazon Connect.
     */

    const loggerBase = {
        'operation': 'SEND_MESSAGE',
        'sessionName': sessionName
    };

    logger.debug({ 
        'message': message, 
        ...loggerBase 
    });

    const messageParams = {
        'ConnectionToken': connectionToken,
        'Content': message.Content,
        'ContentType': 'text/plain'
    };

    logger.debug({ 
        'messageParams': messageParams, 
        ...loggerBase 
    });

    // const response = await participant.sendMessage(messageParams).promise();
    const response = await participant.send(new SendMessageCommand(messageParams));

    logger.debug({ 
        'response': response, 
        ...loggerBase
    });

    return response.Id;
}



async function getAttachmentUrl(attachmentId, connectionToken) {
    /**
     * Gets a pre-signed S3 URL to an attachment shared during the Amazon Connect chat session.
     * 
     * @attachmentId {string} The id of the attachment.
     * @connectionToken {object} The connection token of the chat session.
     * @return {object} Returns an object containing the url to the attachment and the expiration time.
     */

    const loggerBase = {
        'operation': 'GET_ATTACHMENT_URL',
        'attachmentId': attachmentId
    };

    const attachmentParams = {
        'AttachmentId': attachmentId,
        'ConnectionToken': connectionToken
    };

    logger.debug({ 
        'attachmentParams': attachmentParams, 
        ...loggerBase 
    });

    // const response = await participant.getAttachment(attachmentParams).promise();
    const response = await participant.send(new GetAttachmentCommand(attachmentParams));

    logger.debug({ 
        'response': response, 
        ...loggerBase
    });

    return { 
        'Url': response.Url, 
        'UrlExpiry': (new Date(response.UrlExpiry)).getTime()
    };
}



async function sendAttachment(sessionName, connectionToken, attachment) {
    /**
     * Uploads an attachment to an Amazon Connect chat session.
     * 
     * @sessionName {string} The id of the attachment.
     * @connectionToken {object} The connection token of the chat session.
     * @attachment {object} The connection token of the chat session.
     * @return {object} Returns an object containing the connection data (e.g. websocket url, tokens, etc).
     */

    const loggerBase = {
        'operation': 'SEND_ATTACHMENT_TO_CONNECT',
        'sessionName': sessionName
    };

    const attachmentParams = {
        'ConnectionToken': connectionToken,
        'AttachmentName': attachment.AttachmentName,
        'AttachmentSizeInBytes': attachment.AttachmentSizeInBytes,
        'ContentType': attachment.ContentType
    };

    logger.debug({ 
        'attachmentParams': attachmentParams, 
        ...loggerBase
    });

    try {
        if (attachment.AttachmentSizeInBytes > maxAttachmentSize) {
            logger.warn({ 'error': 'Skipping as attachment is above size limit', ...loggerBase });
            return await sendMessage(
                sessionName, 
                connectionToken, 
                { 'Content': '[WARNING] Attachment sent is above size limit.' }
            );
        }
    
        const attachmentData = await participant.send(new StartAttachmentUploadCommand(attachmentParams));
        logger.debug({ 
            'attachmentData': attachmentData, 
            ...loggerBase
        });
    
        const axiosConfig = {
            'url': attachmentData.UploadMetadata.Url,
            'method': 'put',
            'headers': attachmentData.UploadMetadata.HeadersToInclude,
            'data': attachment.Content
        };
    
        await axios(axiosConfig);
    
        await participant.send(new CompleteAttachmentUploadCommand({ 
            'AttachmentIds': [ attachmentData.AttachmentId ], 
            'ConnectionToken': connectionToken
        }));
    
        return attachmentData.AttachmentId;
    } catch (error) {
        logger.error({ 
            'error': error.stack,
            ...loggerBase
        });
        return await sendMessage(
            sessionName, 
            connectionToken, 
            { 'Content': '[WARNING] Failed to receive attachment.' });
    }
}



module.exports = { 
    entrypoint, 
    endSession, 
    sendMessage, 
    getAttachmentUrl, 
    sendAttachment 
};