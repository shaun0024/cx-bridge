'use strict';

/**
 * MODULES AND OTHER DEPENDENCIES
 */
const { LexRuntimeV2Client, RecognizeTextCommand, DeleteSessionCommand } = require('@aws-sdk/client-lex-runtime-v2');
const lex = new LexRuntimeV2Client({ apiVersion: '2020-08-07' });
const { SQSClient, SendMessageCommand } = require('@aws-sdk/client-sqs');
const sqs = new SQSClient({ 'apiVersion': '2012-11-05' });
const axios = require('axios');
const session = require('../base/session');

/**
 * LOGGER
 */
const logger = require('pino')({
    'base': null,
    'timestamp': false, 
    'level': ('LOGGING_LEVEL' in process.env) ? process.env.LOGGING_LEVEL.toLowerCase() : 'info'
}).child({ 'module': 'PLATFORM/LEX' });

/**
 * GLOBAL VARIABLES
 */
const chatMessageQueueUrl = process.env.CHAT_MESSAGE_QUEUE_URL;
const maxAttachmentSize = 20000000;
const sessionsTable = process.env.SESSIONS_TABLE;


async function entrypoint(config, message, sessionName, sessionData) {
    /**
     * Processes inbound messages from AWS SQS to be sent to Amazon Lex.
     *
     * @config {object} Config object containing details of platform, the channel and integration.
     * @message {object} The message to be sent.
     * @sessionName {string} The message to be sent.
     * @sessionData {object} The session object containing details of the existing session.
     * @return {null} Returns nothing.
     */

    if (message.ContentType == 'text/plain') {
        await manageSession(sessionName, sessionData, config, message);
    }

    return;
}



async function manageSession(sessionName, sessionData, config, message, customAttributes = {}) {
    /**
     * Starts or continues an existing Amazon Lex session.
     * 
     * @sessionName {string} The name of the session.
     * @sessionData {object} The session object containing details of the existing session.
     * @config {object} Config object containing details of platform, the channel and integration.
     * @message {object} The message to be sent to Amazon Lex.
     * @customAttributes {object} Any additional key value pairs to be added as contact attributes.
     * @return {object} Returns an object containing the connection data (e.g. websocket url, tokens, etc).
     */

    const loggerBase = {
        'operation': 'MANAGE_SESSION',
        'sessionName': sessionName
    };

    logger.debug({ 
        'config': config, 
        'message': message, 
        'customAttributes': customAttributes,
        ...loggerBase 
    });

    const sessionParams = {
        'botId': config.Platform.BotId,
        'botAliasId': config.Platform.BotAliasId,
        'localeId': config.Platform.LocaleId,
        'sessionId': sessionName,
        'text': message.Content
    };

    if (sessionData == null) {
        const channel = require(`../channels/${config.Channel.SubCategory}`);

        const patron = await channel.getName(config, message.Patron.ChannelId, null, message.Metadata);
    
        message.Patron = patron;

        sessionParams.requestAttributes = {
            '_meta_Channel': config.Channel.SubCategory,
            '_meta_ChannelName': config.Channel.FriendlyName,
            '_meta_PatronChannelId': message.Patron.ChannelId,
            '_meta_PatronDisplayName': message.Patron.DisplayName,
            '_meta_PatronFirstName': message.Patron.FirstName,
            '_meta_PatronLastName': message.Patron.LastName,
            ...customAttributes
        }

        if ('IntegrationName' in message.Patron && 'IntegrationId' in message.Patron) {
            sessionParams.requestAttributes._meta_IntegrationName = message.Patron.IntegrationName;
            sessionParams.requestAttributes._meta_PatronIntegrationId = message.Patron.IntegrationId;
            sessionParams.requestAttributes._meta_PatronUrl = message.Patron.RecordUrl;
        }
    }

    logger.debug({ 
        'sessionParams': sessionParams,
        ...loggerBase
    });

    const response = await lex.send(new RecognizeTextCommand(sessionParams));

    const sessionExpiration = Math.round(Date.now() / 1000) + (60 * (config.Channel.Timeout - 1));

    if (sessionData == null) {
        sessionData = {
            'SessionName': sessionName,
            'Config': config,
            'SessionExpiration': sessionExpiration,
            'Metadata': message.Metadata,
            'LastMessageId': {
                'FromPlatform': null,
                'FromChannel': message.MessageId
            }
        };
        await session.saveSession(sessionsTable, sessionName, sessionData);
    } else {
        await session.updateLastMessageSent(sessionsTable, sessionName, 'customer', message.MessageId, sessionExpiration);
    }

    logger.debug({
        'response': response,
        'sessionData': sessionData,
        ...loggerBase
    });

    if (response.sessionState.dialogAction.type.toLowerCase() == 'close') await endSession(sessionData);

    if (!('messages' in response)) return;

    let reply;
    for (let i = 0; i < response.messages.length; i++) {
        reply =  JSON.stringify({
            'direction': 'toChannel',
            'config': config,
            'message': {
                'Patron': {
                    'ChannelId': message.Patron.ChannelId
                },
                'MessageId': `${response.sessionState.originatingRequestId}:${i}`,
                'ContentType': 'text/plain',
                'Content': response.messages[i].content,
                'Metadata': sessionData.Metadata,
                'SkipUpdate': false
            }
        });

        logger.debug({ 
            'reply': reply,
            ...loggerBase
        });

        await sqs.send(new SendMessageCommand({
            'QueueUrl': chatMessageQueueUrl,
            'MessageGroupId': sessionName,
            'MessageBody': reply
        }));
    }
}



async function endSession(sessionData) {
    /**
     * Ends an existing session with Amazon Lex based on the session id.
     * 
     * @sessionData {object} The session object containing details of the existing session.
     * @return {null} Returns nothing.
     */

    const { SessionName, Config } = sessionData;

    const loggerBase = {
        'operation': 'STOP_CHAT',
        'sessionName': SessionName,
    };

    logger.debug({
        'config': Config,
        ...loggerBase
    })

    try {
        const sessionParams = {
            'botId': Config.Platform.BotId,
            'botAliasId': Config.Platform.BotAliasId,
            'localeId': Config.Platform.LocaleId,
            'sessionId': SessionName,
        };

        logger.debug({ 
            'sessionParams': sessionParams, 
            ...loggerBase 
        });

        const response = await lex.send(new DeleteSessionCommand(sessionParams));

        logger.debug({ 
            'response': response, 
            ...loggerBase 
        });

        await session.deleteSession(sessionsTable, SessionName);

        return;
    } catch (error) {
        logger.error({ 
            'error': error.stack, 
            ...loggerBase 
        });
    }
}


module.exports = { 
    entrypoint,
    endSession
};