'use strict';

/**
 * MODULES AND OTHER DEPENDENCIES
 */
const { DynamoDBClient } = require('@aws-sdk/client-dynamodb');
const { 
    DynamoDBDocumentClient, 
    QueryCommand, 
    GetCommand, 
    PutCommand, 
    UpdateCommand, 
    DeleteCommand 
} = require('@aws-sdk/lib-dynamodb');
const dynamo = DynamoDBDocumentClient.from(
    new DynamoDBClient({ 'apiVersion': '2012-08-10' }), 
    { 
        'marshallOptions': { 
            'removeUndefinedValues': true 
        } 
    }
);

/**
 * LOGGER
 */
const logger = require('pino')({
    'base': null,
    'timestamp': false,
    'level': ('LOGGING_LEVEL' in process.env) ? process.env.LOGGING_LEVEL.toLowerCase() : 'info'
}).child({ 'module': 'SESSION' });




async function getSession(table, indexName, hashKey, hashValue) {
    /**
     * Checks to see if an existing chat session has been initiated before.
     * 
     * @table {string} The name of the session.
     * @sessionName {string} Config object containing details of Amazon Connect, the channel and integration.
     * @return {object} Returns an object containing the connection data (e.g. websocket url, tokens, etc).
     */

     async function queryBySessionName() {
        const loggerBase = { 'operation': 'GET_SESSION_BY_SESSIONNAME' };

        const query = {
            'TableName': table,
            'Key': {
                'SessionName': hashValue
            }
        };

        logger.debug({ 
            'query': query, 
            ...loggerBase 
        });

        const result = await dynamo.send(new GetCommand(query));

        logger.debug({ 
            'result': result, 
            ...loggerBase 
        });

        return result;
    }

    async function queryByContactId() {
        const loggerBase = { 'operation': 'GET_SESSION_BY_CONTACTID' };

        const query = {
            'TableName': table,
            'IndexName': indexName,
            'KeyConditionExpression': 'ContactId = :contactId',
            'ExpressionAttributeValues': { 
                ':contactId': hashValue
            }
        };

        logger.debug({ 
            'query': query, 
            ...loggerBase 
        });

        const results = await dynamo.send(new QueryCommand(query));

        logger.debug({ 
            'results': results, 
            ...loggerBase
        });

        let result;
        if (results.Count == 1) {
            result = { 'Item': results.Items[0] };
        } else {
            result = {};
        }

        return result;
    }


    const loggerBase = { 'operation': 'GET_SESSION' };

    let result;

    if (hashKey == 'sessionName') {
        result = await queryBySessionName();
    } else if (hashKey == 'contactId') {
        result = await queryByContactId();
    } else {
        throw `Accepted values for hash key is sessionName or contactId, value provided is ${hashKey}`;
    }

    if (!('Item' in result)) return null;

    if (result.Item && result.Item.SessionExpiration < Math.round(Date.now() / 1000)) return null;

    return result.Item;
}




async function saveSession(table, sessionName, sessionData) {
    /**
     * Saves configuration and connection details from a newly initiated Amazon Connect chat session.
     * 
     * @table {string} The name of the session.
     * @sessionName {string} Config object containing details of Amazon Connect, the channel and integration.
     * @sessionData {object}
     * @return {object} Returns an object containing the connection data (e.g. websocket url, tokens, etc).
     */

    const loggerBase = { 'operation': 'SAVE_SESSION' };

    const query = {
        'TableName': table,
        'Item': {
            'SessionName': sessionName,
            ...sessionData
        }
    };

    logger.debug({ 
        'query': query, 
        ...loggerBase
    });

    await dynamo.send(new PutCommand(query));

    logger.debug({ 
        'sessionData': query.Item, 
        ...loggerBase 
    });

    return query.Item;
}



async function updateLastMessageSent(table, sessionName, sender, messageId, sessionExpiration) {
    /**
     * Updates last message receipt ids sent from Amazon Connect or the channel.
     * 
     * @table {string} The name of the session.
     * @sessionName {string} Config object containing details of Amazon Connect, the channel and integration.
     * @sender {string} The sender of the message.
     * @messageId {string} The receipt of of the last message sent.
     * @sessionExpiration {integer} The expiration time of the session.
     * @return {object} Returns an object containing the connection data (e.g. websocket url, tokens, etc).
     */

    const loggerBase = { 'operation': 'UPDATE_LAST_MESSAGE_SENT' };

    const senders = [ 'agent', 'customer', 'system' ];

    if (senders.indexOf(sender.toLowerCase()) < 0) throw `Sender must be one of ${senders.join(', ')}`;

    const query = {
        'TableName': table,
        'ReturnValues': 'ALL_NEW',
        'Key': {
            'SessionName': sessionName
        }
    };

    query.UpdateExpression = (sender.toLowerCase() == 'system' || sender.toLowerCase() == 'agent' ) ? 'set LastMessageId.FromPlatform = :messageId, SessionExpiration = :sessionExpiration' : 'set LastMessageId.FromChannel = :messageId, SessionExpiration = :sessionExpiration';

    query.ExpressionAttributeValues = {
        ':messageId': messageId,
        ':sessionExpiration': sessionExpiration
    };

    logger.debug({ 
        'query': query,
        ...loggerBase 
    });

    const result = await dynamo.send(new UpdateCommand(query));

    logger.debug({ 
        'sessionData': result.Attributes, 
        ...loggerBase
    });

    return result.Attributes;
}



async function updateConnectionToken(table, sessionName, connectionToken) {
    /**
     * Updates the connection token to an existing Amazon Connect chat session.
     * 
     * @table {string} The name of the session.
     * @sessionName {string} Config object containing details of Amazon Connect, the channel and integration.
     * @connectionToken {string}
     * @return {object} Returns an object containing the connection data (e.g. websocket url, tokens, etc).
     */

    const loggerBase = { 'operation': 'UPDATE_CONNECTION_TOKEN' };

    const query = {
        'TableName': table,
        'ReturnValues': 'ALL_NEW',
        'UpdateExpression': 'set ConnectionToken = :connectionToken',
        'ExpressionAttributeValues': {
            ':connectionToken': connectionToken
        },
        'Key': {
            'SessionName': sessionName
        }
    };

    logger.debug({ 
        'query': query, 
        ...loggerBase 
    });

    const result = await dynamo.send(new UpdateCommand(query));

    logger.debug({ 
        'sessionData': result.Attributes, 
        ...loggerBase 
    });

    return result.Attributes;
}



async function updateCurrentParticipant(table, sessionName, isAgent) {
    /**
     * Updates the connection token to an existing Amazon Connect chat session.
     * 
     * @table {string} The name of the session.
     * @sessionName {string} Config object containing details of Amazon Connect, the channel and integration.
     * @albCookie {string}
     * @return {object} Returns an object containing the connection data (e.g. websocket url, tokens, etc).
     */

    const loggerBase = { 'operation': 'UPDATE_CURRENT_PARTICIPANT' };

    const query = {
        'TableName': table,
        'ReturnValues': 'ALL_NEW',
        'UpdateExpression': 'set IsAgent = :isAgent',
        'ExpressionAttributeValues': {
            ':isAgent': isAgent
        },
        'Key': {
            'SessionName': sessionName
        }
    };

    logger.debug({ 
        'query': query, 
        ...loggerBase 
    });

    const result = await dynamo.send(new UpdateCommand(query));

    logger.debug({ 
        'sessionData': result.Attributes, 
        ...loggerBase 
    });

    return result.Attributes;
}



async function deleteSession(table, sessionName) {
    /**
     * Deletes configuration and connection details to an existing Amazon Connect chat session when it ends.
     * 
     * @table {string} The name of the session.
     * @sessionName {string} Config object containing details of Amazon Connect, the channel and integration.
     * @albCookie {string}
     * @return {object} Returns an object containing the connection data (e.g. websocket url, tokens, etc).
     */

    const loggerBase = { 'operation': 'DELETE_SESSION' };

    const query = {
        'TableName': table,
        'Key': {
            'SessionName': sessionName
        }
    };

    logger.debug({ 
        'query': query, 
        ...loggerBase 
    });

    await dynamo.send(new DeleteCommand(query));

    return;
}



module.exports = { 
    getSession, 
    saveSession, 
    updateLastMessageSent, 
    updateConnectionToken, 
    updateCurrentParticipant, 
    deleteSession
};