'use strict'

/**
 * MODULES AND OTHER DEPENDENCIES
 */
const { SecretsManagerClient, GetSecretValueCommand } = require("@aws-sdk/client-secrets-manager");
const axios = require('axios');
const crypto = require('crypto');

/**
 * LOGGER
 */
const logger = require('pino')({
    'base': null,
    'timestamp': false, 
    'level': ('LOGGING_LEVEL' in process.env) ? process.env.LOGGING_LEVEL.toLowerCase() : 'info'
}).child({ 'module': 'UTILS' });


async function verifySnsSignature(snsPayload) {
    /**
     * Performs verification of SNS signature.
     * 
     * @snsPayload {object} The SNS payload received.
     * @return {boolean} Returns true if verification succeeded, false if failed.
     */

    const loggerBase = { 'operation': 'VERIFY_SNS_SIGNATURE' };

    logger.debug({
        'snsPayload': snsPayload
    });

    try {

        if (snsPayload.SignatureVersion != '1') throw 'Only SNS Signature V1 is supported'; 

        let verifyKeys;

        if (snsPayload.Type == 'SubscriptionConfirmation' || snsPayload.Type == 'UnsubscribeConfirmation') {
            verifyKeys = [ 'Message', 'MessageId', 'SubscribeURL', 'SubscribeUrl', 'Timestamp', 'Token', 'TopicArn', 'Type' ];
        } else {
            verifyKeys = [ 'Message', 'MessageId', 'Subject', 'Timestamp', 'TopicArn', 'Type' ];
        }
    
        const verifyFunction = crypto.createVerify('sha1WithRSAEncryption');
    
        for (let i = 0; i < verifyKeys.length; i++) {
            if (verifyKeys[i] == 'Subject' && snsPayload.Subject == null) continue; 
    
            if (verifyKeys[i] in snsPayload) verifyFunction.write(`${verifyKeys[i]}\n${snsPayload[verifyKeys[i]]}\n`);
        }
    
        const signingCertUrl = snsPayload.SigningCertURL;
        const publicKey = (await axios({ 
            'url': signingCertUrl, 
            'method': 'get'
        })).data;
        
        const verified = verifyFunction.verify(publicKey, snsPayload.Signature, 'base64');

        logger.debug({ 
            'verified': true, 
            ...loggerBase 
        }); 

        return verified;
    } catch (error) {
        logger.debug({ 
            'verified': false, 
            ...loggerBase 
        });   
        throw error;
    }
}


// async function assumeRole(roleArn, externalId) {
//     /**
//      * Assumes a role to perform necessary operations.
//      * 
//      * @roleArn {string} The ARN of the IAM role to assume.
//      * @externalId {string} The external id of the IAM role to assume.
//      * @return {object} Response object with HTTML status code and message.
//      */

//     const loggerBase = {
//         'operation': 'ASSUME_ROLE',
//         'roleArn': roleArn,
//         'externalId': externalId
//     };

//     const roleToAssume = { 
//         'ExternalId': externalId,
//         'RoleArn': roleArn,
//         'RoleSessionName': Math.random().toString(36).slice(2),
//         'DurationSeconds': 900
//     };

//     try {
//         const sts = new AWS.STS({ 'apiVersion': '2011-06-15' });

//         const credentials = (await sts.assumeRole(roleToAssume).promise()).Credentials;

//         logger.info({ 'status': 'SUCCESS', ...loggerBase }); 

//         return {
//             'accessKeyId': credentials.AccessKeyId,
//             'secretAccessKey': credentials.SecretAccessKey,
//             'sessionToken': credentials.SessionToken    
//         };
//     } catch (error) {
//         logger.error({ 'status': 'FAILED', 'error': error, ...loggerBase });     
//         throw error;
//     }
// }


async function getSecrets(secretArn) {
    /**
     * Gets the value of a secret from the AWS Secrets Manager. The secret is expected to be stored as a JSON string.
     * 
     * @secretArn {string} The ARN of the secret.
     * @return {object} The value of the secret in a JSON format.
     */

    const loggerBase = { 'Operation': 'GET_SECRETS' }

    const secret = new SecretsManagerClient({ 'apiVersion': '2017-10-17' });
    const command = new GetSecretValueCommand({ 'SecretId': secretArn });
    const secretValues = await secret.send(command);

    logger.info({
        'SecretArn': secretArn,
        ...loggerBase
    }); 

    return JSON.parse(secretValues.SecretString);

}



// async function updateMetrics(metricType, metricData, credentials = null) {
//     function formatMetric (metricType, metricData) {
//         switch (metricType) {
//             case 'message':
//                 return {
//                     'Namespace': '/Bridge/ChatMessages',
//                     'MetricData': [
//                         {
//                           'MetricName': '/Bridge/ChatMessages',
//                           'Timestamp' : new Date(),
//                           'Unit': 'Count',
//                           'Value': 1,
//                           'Dimensions': [
//                                 {
//                                     'Name': 'ConnectAlias',
//                                     'Value': metricData.ConnectAlias
//                                 },
//                                 {
//                                     'Name': 'ChannelType',
//                                     'Value': metricData.ChannelType
//                                 },
//                                 {
//                                     'Name': 'ChannelName',
//                                     'Value': metricData.ChannelName
//                                 },
//                                 {
//                                     'Name': 'MessageType',
//                                     'Value': metricData.MessageType
//                                 },
//                                 {
//                                     'Name': 'Direction',
//                                     'Value': metricData.Direction
//                                 },
//                             ]
//                         }
//                     ],
//                 };

//             case 'session':
//                 return {
//                     'Namespace': '/Bridge/ActiveSessions',
//                     'MetricData': [
//                         {
//                           'MetricName': '/Bridge/ActiveSessions',
//                           'Timestamp': new Date(),
//                           'Unit': 'Count',
//                           'Value': metricData.ActiveSessions,
//                           'Dimensions': [
//                                 {
//                                     'Name': 'Hostname',
//                                     'Value': metricData.Hostname
//                                 }
//                             ],
            
//                         }
//                     ]
//                 };
//         }
//     }

//     const loggerBase = {
//         'operation': 'UPDATE_METRICS',
//         'metricType': metricType,
//         'metricData': metricData
//     };

//     try {
//         // let cloudwatch;

//         logger.debug({ 'status': 'VERBOSE', 'credentials': credentials, ...loggerBase })
//         // if (credentials) {
//         //     cloudwatch = new AWS.CloudWatch(await assumeRole(credentials.RoleArn, credentials.ExternalId), { 'apiVersion': '2010-08-01', 'region': process.env.AWS_REGION });
//         // } else {
//         //     cloudwatch = new AWS.CloudWatch({ 'apiVersion': '2010-08-01', 'region': process.env.AWS_REGION });
//         // }
    
//         const formattedMetric = formatMetric(metricType, metricData);
//         await cloudwatch.putMetricData(formattedMetric).promise();

//         logger.debug({ 'status': 'VERBOSE', 'formattedMetric': formattedMetric, ...loggerBase })
//         logger.info({ 'status': 'SUCCESS', ...loggerBase })
//     } catch (error) {
//         logger.error({ 'status': 'FAILED', 'error': error, ...loggerBase });
//     }
// }

module.exports = { verifySnsSignature, getSecrets }