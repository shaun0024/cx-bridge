'use strict'

/**
 * MODULES AND OTHER DEPENDENCIES
 */
const { DynamoDBClient } = require('@aws-sdk/client-dynamodb');
const { DynamoDBDocumentClient, QueryCommand } = require('@aws-sdk/lib-dynamodb');
const dynamo = DynamoDBDocumentClient.from(new DynamoDBClient({ 'apiVersion': '2012-08-10' }));

/**
 * LOGGER
 */
 const logger = require('pino')({
     'base': null,
     'timestamp': false,
     'level': ('LOGGING_LEVEL' in process.env) ? process.env.LOGGING_LEVEL.toLowerCase() : 'info'
}).child({ 'module': 'CONFIG' });



async function getConfigForRequest(table, platformType, platformName, channelType, channelName) {
    /**
     * Gets the Amazon Connect, associated channel and integration configuration.
     * 
     * @table {string} The name of the DynamoDB table.
     * @platformName {string} The Amazon Connect platformName.
     * @channelType {string} The channel type. This can be fbmessenger, twitter or sms. Optional, used when the the configuration of the channel is required and must be provided together with channelId.
     * @channelName {string} The unique id of the channel. Optional, used when the the configuration of the channel is required and must be provided together with channelType.
     * @return {object} Response object with HTTML status code and message.
     */

    const loggerBase = { 'operation': 'GET_CONFIG' };

    const query = {
        'TableName': table,
        'KeyConditionExpression': 'PKey = :DestinationId and begins_with(SKey, :DestinationId)',
        'ExpressionAttributeValues': {
            ':DestinationId': `${platformType}::${platformName}`,
            ':Platform': platformType,
            ':PlatformName': platformName,
            ':PlatformLabel': 'platform',
            ':Channel': channelType,
            ':ChannelName': channelName,
            ':ChannelLabel': 'channel',
            ':IntegrationLabel': 'integration'
        },
        'FilterExpression': '(Category = :PlatformLabel and SubCategory = :Platform and FriendlyName = :PlatformName) or (Category = :ChannelLabel and SubCategory = :Channel and FriendlyName = :ChannelName) or (Category = :IntegrationLabel)'
    };

    const command = new QueryCommand(query);

    logger.debug({ 
        'query': query, 
        ...loggerBase 
    });
    
    const result = await dynamo.send(command);

    logger.debug({ 
        'result': result, 
        ...loggerBase 
    });

    if (result.Count < 2) {
        logger.error({ 'error': `Unable to find platform ${platformType} ${platformName} and / or channel ${channelType} with name ${channelName}`, ...loggerBase });
        return null;
    } 

    const platform = result.Items.filter(
        item => item.Category == 'platform' && 
        item.SubCategory == platformType && 
        item.FriendlyName == platformName
    )[0];
    const channel = result.Items.filter(
        item => item.Category == 'channel' && 
        item.SubCategory == channelType && 
        item.FriendlyName == channelName
    )[0];

    if (!platform || !channel) {
        logger.error({ 'error': `Unable to find platform ${platformType} ${platformName} and / or channel ${channelType} with name ${channelName}`, ...loggerBase });
        return null;
    } 

    const integrations = result.Items.filter(item => item.SKey == channel.IntegrationDirectory);

    if ((channel.IntegrationDirectory != null) && (integrations.length != 1)) {
        logger.warn({ 'warning': `Unable to find integration directory ${channel.IntegrationDirectory}, setting to null` });
    }

    const config = {
        'Platform': platform,
        'Channel': channel,
        'Integration': (integrations.length == 1) ? integrations[0] : null
    };

    logger.debug({ 
        'config': config, 
        ...loggerBase
    });

    return config;
}


module.exports = { getConfigForRequest };