'use strict';

/**
 * MODULES AND OTHER DEPENDENCIES
 */
const axios = require('axios');
const integrations = require('../integrations/integrations');
const utils = require('../base/utils');
const crypto = require('crypto');
const FormData = require('form-data');
const random = require('randomstring');

/**
 * LOGGER
 */
 const logger = require('pino')({
    'base': null,
    'timestamp': false,
    'level': ('LOGGING_LEVEL' in process.env) ? process.env.LOGGING_LEVEL.toLowerCase() : 'info'
}).child({ 'module': 'CHANNEL/SLACK' });

/**
 * GLOBAL VARIABLES
 */
const apiBaseUrl = 'https://slack.com/api';


function channelType() { return 'slack'; }



async function inboundEntrypoint(config, event) {
    /**
     * Processes the inbound events from AWS API Gateway and returns a HTML response required 
     * by Slack and additional data, if any.
     *
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @event {object} The complete event from the AWS API Gateway.
     * @return {object} Returns an object containing one to two keys: response and messages.
     */

    if (event.body.includes('url_verification')) {
        return await verifyWebhook(config, event.body);
    } else {
        return await processInboundMessages(config, event);
    }
}


async function outboundEntrypoint(config, message) {
    /**
     * Processes outbound messages from AWS SQS to be sent to Slack.
     *
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @message {object} The message to be sent.
     * @return {string} Returns the receipt id of the message from Slack.
     */

    const loggerBase = { 'operation': 'OUTBOUND' };

    logger.debug({
        'config': config,
        'message': message,
        ...loggerBase
    });

    const supportedContentTypes = [
        'text/plain',
        'attachment'
    ];

    if (!(supportedContentTypes.includes(message.ContentType))) {
        logger.error({
            'error': 'Unsupported content type',
            ...loggerBase
        });
        return null;
    }

    let receiptId;

    if (message.ContentType == 'text/plain') {
        receiptId = await sendMessage(config, message);
    }

    if (message.ContentType == 'attachment') {
        receiptId = await sendAttachment(config, message);
    }

    await setSenderAction(config, message.Patron.ChannelId, 'mark_seen');

    return receiptId;
}


async function verifyWebhook(config, body) {
    /**
     * Performs verification of webhook registration from Slack.
     * 
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @body {object} The raw request body sent as part of the Slack verification process.
     * @return {object} Returns an object containing one to two keys: response and messages.
     */

    const loggerBase = { 'operation': 'VERIFY_WEBHOOK' };

    logger.debug({
        'config': config,
        'body': body,
        ...loggerBase
    });

    const { token, challenge } = JSON.parse(body);
    const secrets = await utils.getSecrets(config.Channel.SecretArn);

    if (token === secrets.verifyToken) {
        logger.info({
            'verified': true,
            ...loggerBase
        });
        return {
            'response': formatApiResponse(200, challenge)
        };
    } else {
        logger.error({
            'verified': false,
            ...loggerBase
        });
        return {
            'response': formatApiResponse(403)
        };
    }

}



async function getName(config, patronChannelId, secrets = null) {
    /**
     * Gets the basic details of the patron directly from Slack or 
     * from an integration point (e.g. Salesforce, ServiceNow).
     *
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @patronChannelId {string} The Slack id of the patron.
     * @secrets {object} Optional. The object containing the secrets for Slack.
     * @return {string} Returns the details of the patron.
     */

    async function getNameFromSlack(config, patronChannelId, secrets = null) {
        /**
         * Gets the basic details of the patron directly from Slack.
         * 
         * @config {object} Config object containing details of the platform, the channel and integration.
         * @patronChannelId {string} The Slack id of the patron.
         * @secrets {object} Optional. The object containing the secrets for Slack.
         * @return {string} Returns the details of the patron.
         */
    
        const loggerBase = { 'operation': 'GET_NAME_FROM_SLACK' };
    
        try {
            secrets = (!secrets) ? await utils.getSecrets(config.Channel.SecretArn) : secrets;

            const axiosConfig = {
                'url': `${apiBaseUrl}/users.profile.get?user=${patronChannelId}&pretty=1`,
                'method': 'get',
                'headers': { 
                    'Authorization': `Bearer ${secrets.oauthToken}`, 
                    'Content-Type': 'application/json;charset=utf-8'
                }
            };

            logger.debug({
                'axiosConfig': axiosConfig,
                ...loggerBase
            });

            const response = await axios(axiosConfig);
    
            logger.debug({
                'response': response.data,
                ...loggerBase
            });
    
            return { 
                'ChannelId': patronChannelId, 
                'DisplayName': response.data.profile.real_name.trim(),
                'FirstName': response.data.profile.first_name,
                'LastName': response.data.profile.last_name
            };
        } catch (error) {
            logger.error({ 
                'error': error.stack, 
                ...loggerBase
            });
    
            return {
                'ChannelId': patronChannelId,
                'DisplayName': 'unknown',
                'FirstName': 'unknown',
                'LastName': 'unknown'
            };
        }
    }

    const loggerBase = { 'operation': 'GET_NAME' };

    logger.debug({
        'config': config,
        'patronChannelId': patronChannelId,
        ...loggerBase
    });

    let patron;
    if (config.Integration) {
        patron = await integrations.getCustomerDetails(config, patronChannelId);

        if (!patron) {
            patron = await getNameFromSlack(config, patronChannelId, secrets);
            patron.IntegrationName = config.Integration.FriendlyName;
            patron.IntegrationId = 'unknown';
        }
    } else {
        patron = await getNameFromSlack(config, patronChannelId, secrets);
    }

    logger.debug({
        'patron': patron,
        ...loggerBase
    });

    return patron;
}



async function processInboundMessages(config, event) {
    /**
     * Process messages sent from the Slack webhook into 
     * the Bridge format for queueing and further processing.
     * 
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @event {string} The complete event from the AWS API Gateway.
     * @return [{object}] Returns an array of formatted messages to be queued for further processing.
     */


    async function verifyHeader(config, headerSignature, timestamp, bodyRaw, secrets = null) {
        /**
         * Performs verification of request headers for webhook events sent from Slack.
         * 
         * @config {object} Config object containing details of the platform, the channel and integration.
         * @headerSignature {string} The signature in the header sent from Slack as part of the request.
         * @bodyRaw {string} The raw body of the request.
         * @secrets {object} Optional. The object containing the secrets for Slack.
         * @return {string} Returns true if verified, false if not.
         */
    
    
        const loggerBase = { 'operation': 'VERIFY_HEADER' };
    
        logger.debug({
            'config': config,
            'headerSignature': headerSignature,
            'bodyRaw': bodyRaw,
            ...loggerBase
        });
    

        secrets = (!secrets) ? await utils.getSecrets(config.Channel.SecretArn) : secrets;

        const craftedSignature = `v0:${timestamp}:${bodyRaw}`;

        const hmac = crypto.createHmac('sha256', secrets.signingSecret).update(craftedSignature, 'utf8').digest('hex');

        if (headerSignature == `v0=${hmac}`) {
            logger.info({
                'verifiedHeader': true,
                ...loggerBase
            });
            return true;
        } else {
            logger.error({
                'verifiedHeader': false,
                ...loggerBase
            });
           return false;
        }
    }


    function formatMessage(config, patron, messageId, contentType, content, conversationData, direction = 'toPlatform') {
        /**
         * Formats the message into the Bridge format based on the content type.
         *
         * @config {object} Config object containing details of the platform, the channel and integration.
         * @patron {object} Object containing the details of the patron / customer.
         * @messageId {string} The id of the message.
         * @contentType {string} The type of content. Accepted values are 'text/plain' and 'attachment'
         * @content {string || object} The string of the message to be sent or an object containt details of the attachment.
         * @direction {string} Optional. The destination of the message. Accepted values are 'toChannel' or 'toPlatform'.
         * @return {object} Returns a message in the Bridge format that can be queued.
         */

        const formatted = {
            'direction': direction,
            'config': config,
            'message': {
                'Patron': patron, 
                'MessageId': messageId,
                'ContentType': contentType,
                'Metadata': conversationData
            }
        };

        switch (contentType) {
            case 'text/plain':
                formatted.message.Content = content;
                break;

            case ('attachment'):
                formatted.message.Content = {
                    'Filename': content.name,
                    'Type': content.mimetype,
                    'Url': content.url_private
                };
                break;
        }

        return formatted;
    }


    async function messageEvent(config, body, secrets, messages) {
        if (body.token != secrets.verifyToken || body.api_app_id != secrets.appId) return messages;

        if (body.event.bot_id == secrets.botId || body.event.user == secrets.userId) return messages;

        if (body.event.type != 'message') return messages;

        if (body.event.channel_type != 'im' && body.event.channel_type != 'channel') return messages;

        let patron;
        if (body.event.channel_type == 'im') {
            const patron = { 
                'ChannelId': body.event.user
            };

            if (body.event.text) {
                messages.push(formatMessage(config, patron, body.event.client_msg_id, 'text/plain', body.event.text));
            }

            if ('files' in body.event) {
                for (let i = 0; i < body.event.files.length; i++) {
                    messages.push(formatMessage(config, patron, body.event.files[i].id, 'attachment', body.event.files[i]));
                }
            }
        } else if (body.event.channel_type == 'channel' && config.Channel.RespondProactively) {
            if (config.Channel.MonitorChannels.includes(body.event.channel)) {
                patron = {
                    'ChannelId': body.event.channel
                };
                messages.push(formatMessage(config, patron, body.event.ts, 'publicComment', body.event.text));
            }
        }

        return messages;
    }


    async function actionEvent(config, body, secrets, messages) {
        if (body.token != secrets.verifyToken || body.api_app_id != secrets.appId) return messages;

        if (body.message.bot_id == secrets.botId && body.user.id == secrets.userId) return messages;

        // const patron = await getName(config, body.user.id, secrets);
        const patron = { 
            'ChannelId': body.user.id
        };

        if (body.actions[0].value) {
            messages.push(formatMessage(config, patron, body.actions[0]['action_ts'], 'text/plain', body.actions[0]['value']));
        }

        return messages;
    }


    const loggerBase = { 'operation': 'PROCESS_MESSAGES' };

    let messages;

    const secrets = await utils.getSecrets(config.Channel.SecretArn);

    await verifyHeader(config, event.headers['X-Slack-Signature'], event.headers['X-Slack-Request-Timestamp'], event.body, secrets);

    let body;
    if (event.body.startsWith('payload=')) {
        body = JSON.parse(decodeURIComponent(event.body.replace('payload=', '')));
        messages = await actionEvent(config, body, secrets, []);
    } else {
        body = JSON.parse(event.body);
        messages = await messageEvent(config, body, secrets, []);
    }
    
    logger.debug({
        'messages': messages,
        ...loggerBase
    });

    return {
        'response': formatApiResponse(200, 'OK'),
        'messages': messages
    };
}



async function downloadAttachment(config, message) {
    /**
     * Processes inbound attachments from the queue and downloads them from Slack.
     * 
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @message {object} The message from the queue containing the details of the attachment to be downloaded.
     * @return {object} Returns an attachment object that will be used to upload the file into the platform.
     */

    const loggerBase = { 'operation': 'DOWNLOAD_ATTACHMENT_FROM_SLACK' };

    logger.debug({
        'config': config,
        'message': message,
        ...loggerBase
    });

    const secrets = await utils.getSecrets(config.Channel.SecretArn);

    const axiosConfig = {
        'url': message.Content.Url,
        'method': 'get',
        'responseType': 'arraybuffer',
        'headers': { 
            'Authorization': `Bearer ${secrets.oauthToken}`
        }
    };

    logger.debug({
        'axiosConfig': axiosConfig,
        ...loggerBase
    });

    const response = await axios(axiosConfig);

    const attachment = {
        'MessageId': message.MessageId,
        'AttachmentName': message.Content.Filename, 
        'AttachmentSizeInBytes': response.data.byteLength, 
        'ContentType': message.Content.Type, 
        'Content': response.data
    };

    logger.debug({ 
        'messageId': message.MessageId,
        'attachmentName': message.Content.Filename, 
        'attachmentSizeInBytes': response.data.byteLength, 
        'contentType': message.Content.Type,
        ...loggerBase
    });

    return attachment;
}



async function sendAttachment(config, message) {
    /**
     * Processes outbound attachments from the queue and uploads into Slack.
     * 
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @message {object} The message from the queue containing the details of the attachment to be uploaded to Slack.
     * @return {string} Returns the message id / receipt from Slack.
     */

    const loggerBase = { 'operation': 'SEND_ATTACHMENT_TO_SLACK' };

    logger.debug({ 
        'config': config,
        'message': message, 
        ...loggerBase
    });

    let messageId;

    if (config.Channel.UploadAttachments) {
        const file = await axios.get(message.Content.Url, { 'responseType': 'stream' });

        const fileType = message.Content.Filename.split('.')[(message.Content.Filename.split('.').length - 1)];
        const fileName = message.Content.Filename.replace(`.${fileType}`, `-${random.generate(6)}.${fileType}`);

        const body = new FormData();
        body.append('channels', message.Patron.ChannelId);
        body.append('filename', fileName);
        body.append('filetype', fileType);
        body.append('file', file.data);

        const secrets = await utils.getSecrets(config.Channel.SecretArn);

        const url = `${apiBaseUrl}/files.upload`;

        const axiosConfig = {
            'url': url,
            'method': 'post',
            'headers': { 
                'Authorization': `Bearer ${secrets.oauthToken}`,
                'Content-Type': body.getHeaders()['content-type']
            },
            'data': body
        };

        const response = await axios(axiosConfig);

        messageId = response.data.file.id;
    } else {
        const replyMessage = {
            'channel': message.Patron.ChannelId,
            'blocks': [
                {
                    'type': 'section',
                    'text': {
                        'type': 'plain_text',
                        'text': config.Channel.AttachmentMessage
                    },
                    'accessory': {
                        'type': 'button',
                        'text': {
                            'type': 'plain_text',
                            'text': message.Content.Filename,
                        },
                        'value': message.Content.Filename,
                        'url': message.Content.Url,
                        'action_id': 'button-action'
                    }
                }
            ]
        };

        messageId = await sendMessage(config, replyMessage, true);
    }

    return messageId;
}



async function sendMessage(config, message, formatted = false) {
    /**
     * Processes text messages from the queue to be sent to Slack.
     * 
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @message {object} The message from the queue containing the details of the message.
     * @return {string} Returns the message id from Slack.
     */

    const loggerBase = { 'operation': 'SEND_MESSAGE_TO_SLACK' };

    logger.debug({
        'config': config, 
        'message': message, 
        ...loggerBase 
    });

    const url = `${apiBaseUrl}/chat.postMessage`;

    let body;

    if (!formatted) {
        let messageData;

        body = { 'channel': message.Patron.ChannelId };

        try {
            messageData = JSON.parse(message.Content.replace('\"', '"'));
        } catch (error) {
            messageData = { 'txt': message.Content };
        }

        if ('btns' in messageData) {
            let blocks = [];
            let buttons = [];

            blocks.push({
                'type': 'section',
                'text': {
                    'type': 'plain_text',
                    'text': messageData.txt
                }
            });

            messageData.btns.forEach((button) => {
                buttons.push({
                    'type': 'button',
                    'text': {
                        'type': 'plain_text',
                        'text': button.text
                    },
                    'value': button.value
                });
            });

            blocks.push({
                'type': 'actions',
                'elements': buttons
            });

            body.blocks = blocks;
        } else {
            body.text = messageData.txt;
        }
    } else {
        body = message;
    }

    const secrets = await utils.getSecrets(config.Channel.SecretArn);

    const axiosConfig = {
        'url': url,
        'method': 'post',
        'headers': { 
            'Authorization': `Bearer ${secrets.oauthToken}`, 
            'Content-Type': 'application/json;charset=utf-8'
        },
        'data': body
    };

    logger.debug({ 
        'axiosConfig': axiosConfig,
        ...loggerBase
    });

    const response = await axios(axiosConfig);
    
    logger.debug({ 
        'response': response.data,
        ...loggerBase
    });

    return response.data.message.ts;
}



// async function sendReaction(config, message, sentiment) {
//     /**
//      * Processes text messages from the queue to be sent to Twitter.
//      * 
//      * @config {object} Config object containing details of Amazon Connect, the channel and integration.
//      * @message {object} The message from the queue.
//      * @return {string} Returns the message id from Twitter.
//      */

//     const loggerBase = { 
//         'operation': 'SEND_REACTION',
//         'channelType': config.Channel.SubCategory,
//         'channelName': config.Channel.FriendlyName
//     };

//     logger.debug({ 'status': 'VERBOSE', 'config': config, 'message': message, ...loggerBase });

//     if (sentiment != 'positive' && sentiment != 'negative') {
//         return;
//     }

//     try {
//         const secrets = await utils.getSecrets(config.Channel.SecretArn);

//         const url = `${apiBaseUrl}/reactions.add`;

//         const body = {
//             'channel': message.Patron.ChannelId,
//             'name': (sentiment == 'positive') ? 'smile' : 'rage',
//             'timestamp': message.MessageId
//         };

//         const axiosConfig = {
//             'url': url,
//             'method': 'post',
//             'headers': { 
//                 'Authorization': `Bearer ${secrets.oauthToken}`, 
//                 'Content-Type': 'application/json;charset=utf-8'
//             },
//             'data': body
//         };
    
//         logger.debug({ 'status': 'VERBOSE', 'axiosConfig': axiosConfig, ...loggerBase });
    
//         const response = await axios(axiosConfig);
        
//         logger.info({ 'status': 'SUCCESS', ...loggerBase });

//         return response.data.id_str;
//     } catch (error) {
//         logger.error({ 'status': 'FAILED', 'error': error, ...loggerBase });
//         throw error;
//     }
// }







async function setSenderAction(config, patronChannelId, action, messageId = null) {
    /**
     * Send an action (e.g. mark read, typing indicator) to Slack.
     * 
     * @config {object} Config object containing details of Amazon Connect, the channel and integration.
     * @patronChannelId {object} The Slack id of the patron.
     * @return {null} Returns nothing.
     */

    const supportedActions = {
        'typing': 'typing_on',
        'mark_seen': 'mark_seen'
    };

    const loggerBase = { 'operation': 'SEND_ACTION_TO_SLACK' };

    logger.debug({
        'patronChannelId': patronChannelId,
        'action': action,
        ...loggerBase
    });

    try {
        if (action.toLowerCase() == 'mark_seen') {
            const secrets = await utils.getSecrets(config.Channel.SecretArn);

            const url = `${apiBaseUrl}/conversations.mark`;
    
            const body = {
                'channel': patronChannelId,
                'ts': messageId
            };
    
            const axiosConfig = {
                'url': url,
                'method': 'post',
                'headers': { 
                    'Authorization': `Bearer ${secrets.oauthToken}`, 
                    'Content-Type': 'application/json;charset=utf-8'
                },
                'data': body
            };

            logger.debug({
                'axiosConfig': axiosConfig,
                ...loggerBase
            });

            await axios(axiosConfig);
        }
    } catch (error) {
        logger.error({
            'error': error.stack,
            ...loggerBase
        });
    }

    return;
}


function formatApiResponse(responseCode, customMessage = null) {
    /**
     * Formats an HTML response based on the HTML code provided. 
     * 
     * @responseCode {int} The HTTP response code.
     * @customMessage {sring} Optional. A custom message to be sent back as part of thee response.
     * @return {object} Response object with HTTML status code and message.
     */

    switch (responseCode) {
        case 200:
            return {
                'headers': {
                    'Content-Type': 'text/plain'
                },
                'statusCode': 200,
                'body': (customMessage) ? customMessage : 'OK'
            };

        case 400:
            return {
                'headers': {
                    'Content-Type': 'text/plain'
                },
                'statusCode': 200,
                'body': (customMessage) ? customMessage : 'BAD REQUEST'
            };

        case 403:
            return {
                'headers': {
                    'Content-Type': 'text/plain'
                },
                'statusCode': 403,
                'body': (customMessage) ? customMessage : 'FORBIDDEN'
            };

        case 500:
            return {
                'headers': {
                    'Content-Type': 'text/plain'
                },
                'statusCode': 500,
                'body': customMessage
            };
    }
}




module.exports = { 
    channelType, 
    inboundEntrypoint, 
    outboundEntrypoint, 
    getName, 
    downloadAttachment, 
    sendMessage, 
    sendAttachment, 
    setSenderAction
};