'use strict';

/**
 * MODULES AND OTHER DEPENDENCIES
 */
const axios = require('axios');
const { CloudAdapter, ConfigurationBotFrameworkAuthentication, TeamsInfo } = require('botbuilder');
const integrations = require('../integrations/integrations');
const utils = require('../base/utils');
const crypto = require('crypto');
const random = require('randomstring');

/**
 * LOGGER
 */
 const logger = require('pino')({
    'base': null,
    'timestamp': false,
    'level': ('LOGGING_LEVEL' in process.env) ? process.env.LOGGING_LEVEL.toLowerCase() : 'info'
}).child({ 'module': 'CHANNEL/MSTEAMS' });

/**
 * GLOBAL VARIABLES
 */
const openidUrl = 'https://login.botframework.com/v1/.well-known/openidconfiguration';



function channelType() { return 'msteams'; }



async function inboundEntrypoint(config, event) {
    /**
     * Processes the inbound events from AWS API Gateway and returns a HTML response required 
     * by the Azure Bot Service and additional data, if any.
     *
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @event {object} The complete event from the AWS API Gateway.
     * @return {object} Returns an object containing one to two keys: response and messages.
     */

    logger.debug({
        'operation': 'INBOUND',
        'config': config,
        'event': event
    });

    return await processInboundMessages(config, event);
}



async function outboundEntrypoint(config, message) {
    /**
     * Processes outbound messages from AWS SQS to be sent to the Azure Bot Service.
     *
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @message {object} The message to be sent.
     * @return {string} Returns the receipt id of the message from the Azure Bot Service.
     */

    const loggerBase = { 'operation': 'OUTBOUND' };

    logger.debug({
        'config': config,
        'message': message,
        ...loggerBase
    });

    const supportedContentTypes = [
        'text/plain',
        'attachment',
        'fileConsent/invoke'
    ];

    if (!(supportedContentTypes.includes(message.ContentType))) {
        logger.error({
            'error': 'Unsupported content type',
            ...loggerBase
        });
        return null;
    }

    let receiptId;

    if (message.ContentType == 'text/plain') {
        receiptId = await sendMessage(config, message);
    } else if (message.ContentType == 'attachment') {
        receiptId = await sendAttachment(config, message);
    } else if (message.ContentType == 'fileConsent/invoke') {
        receiptId = await uploadAttachmentAfterConsent(config, message);
    }

    return receiptId;
}



async function getName(config, patronChannelId, secrets = null, conversationData = null) {
    /**
     * Gets the basic details of the patron directly from Azure AD or from an integration point (e.g. Salesforce, ServiceNow).
     * 
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @patronChannelId {string} The signature in the header sent from Azure object id of the user as part of the request.
     * @secrets {object} Optional. The object containing the secrets for the Azure Bot Service.
     * @return {string} Returns the details of the patron.
     */

    const loggerBase = { 'operation': 'GET_NAME' };

    logger.debug({
        'config': config,
        'patronChannelId': patronChannelId,
        ...loggerBase
    });

    // if (config.Integration != null) {
    //     return await integrations.getCustomerDetails(config, patronChannelId);
    // }



    try {
        secrets = (!secrets) ? await utils.getSecrets(config.Channel.SecretArn) : secrets;

        const botFrameworkAuthentication = new ConfigurationBotFrameworkAuthentication({
            'MicrosoftAppId': secrets.applicationId,
            'MicrosoftAppPassword': secrets.applicationSecret,
            'MicrosoftAppTenantId': secrets.tenantId,
            'MicrosoftAppType': 'MultiTenant'
        });
    
        const adapter = new CloudAdapter(botFrameworkAuthentication);
    
        let patron;
        await adapter.continueConversationAsync(secrets.applicationId, conversationData, async turnContext => { 
            const response = await TeamsInfo.getMember(turnContext, patronChannelId); 
            patron = {
                'ChannelId': response.email, 
                'DisplayName': response.name,
                'FirstName': response.givenName,
                'LastName': response.surname
            };
        });

        return patron;
    } catch (error) {
        logger.error({ 
            'error': error.stack, 
            ...loggerBase
        });

        return {
            'ChannelId': patronChannelId,
            'DisplayName': conversationData.user.name
        };
    }
}



async function processInboundMessages(config, event) {
    /**
     * Process messages sent from the Azure Bot Service webhook into the Bridge format for queueing and further processing.
     * 
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @event {string} The request sent from Azure Bot Service that includes the headers and body.
     * @return [{object}] Returns an array of formatted messages to be queued for further processing.
     */


    async function verifyHeader(headerSignature, serviceUrl, secrets = null) {
        /**
         * Performs verification of request headers for webhook events sent from the Azure Bot Service.
         * 
         * @headerSignature {string} The signature in the header sent from Azure Bot Service as part of the request.
         * @serviceUrl {string} The service endpoint from Azure that sent the request.
         * @secrets {object} Optional. The object containing the secrets for Azure Bot Service.
         * @return {string} Returns the challenge if verification is successful.
         */


        const loggerBase = { 'operation': 'VERIFY_HEADER' };
        
        logger.debug({
            'headerSignature': headerSignature,
            'serviceUrl': serviceUrl
        });

        const jwtData = headerSignature.replace('Bearer ', '').split('.');
        const jwtHeader = JSON.parse(Buffer.from(jwtData[0], 'base64').toString('ascii'));
        const jwtPayload = JSON.parse(Buffer.from(jwtData[1], 'base64').toString('ascii'));
        const jwtSignature = jwtData[2].replace(/-/g, '+').replace(/_/g, '/'); // Converted from base64url to base64

        const knownKeys = await axios(
            { 
                'url': (await axios({ 'url': openidUrl, 'method': 'get' })).data.jwks_uri, 
                'method': 'get'
            }
        ).then((response) => { 
            return response.data.keys;
        });
        const key = knownKeys.filter(key => key.kid == jwtHeader.kid)[0];
        const publicKey = `-----BEGIN CERTIFICATE-----\n${key.x5c[0].replace(/.{64}/g, '$&\n')}\n-----END CERTIFICATE-----`;

        const verifyFunction = crypto.createVerify('RSA-SHA256');
        verifyFunction.write(`${jwtData[0]}.${jwtData[1]}`);
        verifyFunction.end();

        if (verifyFunction.verify(publicKey, jwtSignature, 'base64')) {
            if ((jwtPayload.iss == 'https://api.botframework.com') && (jwtPayload.aud == secrets.applicationId) && (jwtPayload.serviceurl == serviceUrl)) {
                logger.info({
                    'verifiedHeader': true,
                    ...loggerBase
                });
                return true;
            } else {
                logger.error({
                    'verifiedHeader': false,
                    ...loggerBase
                });
                return false;
            }
        } else {
            logger.error({
                'verifiedHeader': false,
                ...loggerBase
            });
            return false;
        }
    }


    function formatMessage(config, patron, messageId, contentType, content, conversationData, replyToId, direction = 'toPlatform') {
        /**
         * Formats the message into the Bridge format based on the content type.
         *
         * @config {object} Config object containing details of the platform, the channel and integration.
         * @patron {object} Object containing the details of the patron / customer.
         * @messageId {string} The id of the message.
         * @contentType {string} The type of content. Accepted values are 'text/plain' and 'attachment'
         * @content {string || object} The string of the message to be sent or an object containt details of the attachment.
         * @conversationData {object} Additional data required to send a reply to the Azure Bot Service.
         * @direction {string} Optional. The destination of the message. Accepted values are 'toChannel' or 'toPlatform'.
         * @return {object} Returns a message in the Bridge format that can be queued.
         */

        const formatted = {
            'direction': direction,
            'config': config,
            'message': {
                'Patron': patron, 
                'MessageId': messageId,
                'ContentType': contentType,
                'Metadata': conversationData
            }
        };

        switch (contentType) {
            case 'text/plain':
                formatted.message.Content = content;
                break;
            
            case 'attachment':
                formatted.message.Content = {
                    'Type': content.content.fileType,
                    'Url': content.content.downloadUrl,
                    'Filename': content.name
                };
                break;

            case 'fileConsent/invoke':
                formatted.message.Content = {
                    'ReplyToId': replyToId,
                    'UploadData': content.uploadInfo,
                    'AttachmentData': content.context
                };
                break;
        }

        return formatted;
    }


    const loggerBase = { 'operation': 'PROCESS_MESSAGES' };

    const messages = [];

    const body = JSON.parse(event.body);

    if (body.channelId != 'msteams') return messages;

    const secrets = await utils.getSecrets(config.Channel.SecretArn);

    if (event.headers['x-ms-tenant-id'] != secrets.tenantId) return messages;

    if (!(await verifyHeader(event.headers['Authorization'], body.serviceUrl, secrets))) {
        return formatApiResponse(403, 'Header verification failed');
    }

    // const patron = await getName(config, body.from.aadObjectId, secrets);
    const patron = {
        'ChannelId': body.from.aadObjectId
    };

    if ((body.type != 'message') && (body.type != 'invoke')) return messages;

    const conversationData = {
        'activityId': body.id,
        'user': body.from,
        'bot': body.recipient,
        'conversation': body.conversation,
        'channelId': body.channelId,
        'locale': body.locale,
        'serviceUrl': body.serviceUrl     
    };

    if (body.textFormat == 'plain') messages.push(formatMessage(config, patron, body.id, 'text/plain', body.text, conversationData));

    if ((body.hasOwnProperty('attachments') && (body.attachments.length > 0))) {
        for (let i = 0; i < body.attachments.length; i++) {
            if (body.attachments[i].contentType != 'text/html') {
                messages.push(formatMessage(config, patron, body.attachments[i].content.uniqueId, 'attachment', body.attachments[i], conversationData));
            }
        }
    }

    if ((body.type == 'invoke') && (body.value.type == 'fileUpload')) {
        if (Date.now() >= body.value.context.UrlExpiry) {
            messages.push(formatMessage(config, patron, body.id, 'text/plain', config.Channel.ExpiredAttachmentMessage, conversationData, 'toChannel'));
        } else if ((body.value.type == 'fileUpload') && (body.value.action == 'accept')) {
            messages.push(formatMessage(config, patron, body.id, 'fileConsent/invoke', body.value, conversationData, body.replyToId, 'toChannel'));
        } else if ((body.value.type == 'fileUpload') && (body.value.action == 'decline')) {
            const declineReply = { 
                'txt': `${config.Channel.DeclineAttachmentMessage.replace('@FileLink', body.value.context.Url)}`,
                'btns': [
                    {
                        'type': 'openUrl',
                        'text': body.value.context.Filename,
                        'value': body.value.context.Url
                    }
                ]
            };
            messages.push(formatMessage(config, patron, body.id, 'text/plain', JSON.stringify(declineReply), conversationData, body.replyToId, 'toChannel'));
        }
    }

    logger.debug({
        'messages': messages,
        ...loggerBase
    });

    return {
        'response': formatApiResponse(200, 'OK'),
        'messages': messages
    };
}



async function downloadAttachment(config, message) {
    /**
     * Processes inbound attachments from the queue and downloads them from the Azure Bot Service.
     * 
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @message {object} The message from the queue.
     * @return {object} Returns an attachment object that will be used to upload the file into the platform.
     */

    const loggerBase = { 'operation': 'DOWNLOAD_ATTACHMENT_FROM_MSTEAMS' };

    logger.debug({
        'config': config,
        'message': message,
        ...loggerBase
    });

    const axiosConfig = {
        'url': message.Content.Url,
        'method': 'get',
        'responseType': 'arraybuffer'
    };

    logger.debug({
        'axiosConfig': axiosConfig,
        ...loggerBase
    });

    const response = await axios(axiosConfig);

    const attachment = {
        'MessageId': message.MessageId,
        'AttachmentName': message.Content.Filename, 
        'AttachmentSizeInBytes': response.data.byteLength, 
        'ContentType': response.headers['content-type'], 
        'Content': response.data
    };

    logger.debug({ 
        'messageId': message.MessageId,
        'attachmentName': message.Content.Filename, 
        'attachmentSizeInBytes': response.data.byteLength, 
        'contentType': message.Content.Type,
        ...loggerBase
    });

    return attachment;
}



async function sendMessage(config, message) {
    /**
     * Processes text messages from the queue to be sent to the Azure Bot Service.
     * 
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @message {object} The message from the queue.
     * @return {string} Returns the message id from the Azure Bot Service.
     */

    const loggerBase = { 'operation': 'SEND_MESSAGE_TO_MSTEAMS' };

    logger.debug({
        'config': config, 
        'message': message, 
        ...loggerBase 
    });


    const secrets = await utils.getSecrets(config.Channel.SecretArn);

    let messageData;
    
    try {
        messageData = JSON.parse(message.Content.replace('\"', '"'));
    } catch (error) {
        messageData = { 'txt': message.Content };
    }

    let formattedMessage;
    if ('btns' in messageData) {
        const buttons = [];

        messageData.btns.forEach((button) => {
            buttons.push({
                'type': ('type' in button) ? button.type : 'imBack',
                'title': button.text,
                'value': button.value
            });
        });

        formattedMessage = {
            'attachments': [
                {
                    'contentType': 'application/vnd.microsoft.card.hero',
                    'content': {
                        'text': messageData.txt,
                        'buttons': buttons
                    }
                }
            ]
        };
    } else {
        formattedMessage = messageData.txt;
    }

    const botFrameworkAuthentication = new ConfigurationBotFrameworkAuthentication({
        'MicrosoftAppId': secrets.applicationId,
        'MicrosoftAppPassword': secrets.applicationSecret,
        'MicrosoftAppTenantId': secrets.tenantId,
        'MicrosoftAppType': 'MultiTenant'
    });

    const adapter = new CloudAdapter(botFrameworkAuthentication);

    let messageId;

    await adapter.continueConversationAsync(secrets.applicationId, message.Metadata, async turnContext => { 
        const response = await turnContext.sendActivity(formattedMessage);

        logger.debug({ 
            'response': response.data,
            ...loggerBase
        });  
        
        messageId = response.id;
    });

    logger.info({ 'status': 'SUCCESS', ...loggerBase });

    return messageId;
}



async function sendAttachment(config, message) {
    /**
     * Processes attachments from the queue to be sent to the Azure Bot Service.
     * 
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @message {object} The message from the queue.
     * @return {string} Returns the message id from the Azure Bot Service.
     */

    const loggerBase = { 'operation': 'SEND_ATTACHMENT_TO_MSTEAMS' };

    logger.debug({ 
        'config': config,
        'message': message, 
        ...loggerBase
    });

    const secrets = await utils.getSecrets(config.Channel.SecretArn);

    const formattedMessage = { 'attachments': [] };

    const fileType = message.Content.Filename.split('.')[(message.Content.Filename.split('.').length - 1)];
    const fileName = message.Content.Filename.replace(`.${fileType}`, `-${random.generate(6)}.${fileType}`);

    if (config.Channel.UploadAttachments) {
        if (message.Content.Type.includes('image')) {
            formattedMessage.attachments.push({
                'name': fileName, 
                'contentType': message.Content.Type, 
                'contentUrl': message.Content.Url
            });
        } else {
            formattedMessage.attachments.push({
                'contentType': 'application/vnd.microsoft.teams.card.file.consent',
                'name': fileName,
                'content': {
                    'sizeInBytes': message.Content.Length,
                    'acceptContext': {
                        'Filename': fileName,
                        'Length': message.Content.Length,
                        'ContentType': message.Content.Type, 
                        'Url': message.Content.Url,
                        'UrlExpiry': message.Content.UrlExpiry
                    },
                    'declineContext': {
                        'Filename': fileName,
                        'Url': message.Content.Url,
                        'UrlExpiry': message.Content.UrlExpiry
                    }
                }
            });
        }
    } else {
        formattedMessage.attachments.push({
            'contentType': 'application/vnd.microsoft.card.hero',
            'content': {
                'text': fileName,
                'buttons': [
                    {
                        'type': 'openUrl',
                        'title': fileName,
                        'value': message.Content.Url
                    }
                ]
            }
        });
    }

    logger.debug({ 
        'formattedMessage': formattedMessage,
        ...loggerBase
    });

    const botFrameworkAuthentication = new ConfigurationBotFrameworkAuthentication({
        'MicrosoftAppId': secrets.applicationId,
        'MicrosoftAppPassword': secrets.applicationSecret,
        'MicrosoftAppTenantId': secrets.tenantId,
        'MicrosoftAppType': 'MultiTenant'
    });

    const adapter = new CloudAdapter(botFrameworkAuthentication);

    let messageId;

    await adapter.continueConversationAsync(secrets.applicationId, message.Metadata, async turnContext => { 
        const response = await turnContext.sendActivity(formattedMessage);

        logger.debug({ 
            'response': response.data,
            ...loggerBase
        });    
        
        messageId = response.id;
    });

    return messageId;
}



async function uploadAttachmentAfterConsent(config, message) {
    /**
     * Processes attachments from the queue to be sent to the Azure Bot Service.
     * 
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @message {object} The message from the queue.
     * @return {string} Returns the message id from the Azure Bot Service.
     */

     const loggerBase = { 'operation': 'UPLOAD_ATTACHMENT_TO_MSTEAMS' };

    logger.debug({ 
        'config': config, 
        'message': message, 
        ...loggerBase 
    });

    let axiosConfig;

    const secrets = await utils.getSecrets(config.Channel.SecretArn);

    axiosConfig = {
        'url': message.Content.AttachmentData.Url,
        'method': 'get',
        'responseType': 'arraybuffer'
    };

    logger.debug({ 
        'axiosConfig': axiosConfig, 
        ...loggerBase 
    });

    const file = await axios(axiosConfig);

    axiosConfig = {
        'url': message.Content.UploadData.uploadUrl,
        'method': 'put',
        'headers': {
            'Content-Length': message.Content.AttachmentData.Length,
            'Content-Range': `bytes 0-${message.Content.AttachmentData.Length - 1}/${message.Content.AttachmentData.Length}`
        }
    };

    logger.debug({ 
        'axiosConfig': axiosConfig, 
        ...loggerBase 
    });

    axiosConfig.data = file.data;

    await axios(axiosConfig);

    const formattedMessage = {
        'attachments': [
            {
                'contentType': 'application/vnd.microsoft.teams.card.file.info',
                'contentUrl': message.Content.UploadData.contentUrl,
                'name': message.Content.UploadData.name,
                'content': {
                    'uniqueId': message.Content.UploadData.uniqueId,
                    'fileType': message.Content.UploadData.fileType
                }
            }
        ]
    };

    const botFrameworkAuthentication = new ConfigurationBotFrameworkAuthentication({
        'MicrosoftAppId': secrets.applicationId,
        'MicrosoftAppPassword': secrets.applicationSecret,
        'MicrosoftAppTenantId': secrets.tenantId,
        'MicrosoftAppType': 'MultiTenant'
    });

    const adapter = new CloudAdapter(botFrameworkAuthentication);

    let messageId;

    await adapter.continueConversationAsync(secrets.applicationId, message.Metadata, async turnContext => { 
        await turnContext.deleteActivity(message.Content.ReplyToId);

        const response = await turnContext.sendActivity(formattedMessage);

        logger.debug({ 
            'response': response, 
            ...loggerBase 
        });    
        
        messageId = response.id;
    });

    return messageId;
}



async function setSenderAction(config, patronChannelId, action, conversationData) {
    /**
     * Send an action (e.g. typing indicator) to the Azure Bot Service.
     * 
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @patronChannelId {object} The Microsoft Teams id of the patron.
     * @return {null} Returns nothing.
     */

    const supportedActions = {
        'typing': 'typing'
    };

    const loggerBase = { 'operation': 'SEND_ACTION_TO_MSTEAMS' };

    logger.debug({
        'patronChannelId': patronChannelId,
        'action': action,
        ...loggerBase
    });

    if (!(action.toLowerCase() in supportedActions)) return;

    try {
        const secrets = await utils.getSecrets(config.Channel.SecretArn);

        const botFrameworkAuthentication = new ConfigurationBotFrameworkAuthentication({
            'MicrosoftAppId': secrets.applicationId,
            'MicrosoftAppPassword': secrets.applicationSecret,
            'MicrosoftAppTenantId': secrets.tenantId,
            'MicrosoftAppType': 'MultiTenant'
        });
    
        const adapter = new CloudAdapter(botFrameworkAuthentication);

        await adapter.continueConversationAsync(secrets.applicationId, conversationData, async turnContext => { 
            await turnContext.sendActivity({ 'type': supportedActions[action] });
        });
    } catch (error) {
        logger.error({
            'error': error.stack,
            ...loggerBase
        });
    }

    return;
}



function formatApiResponse(responseCode, customMessage = null) {
    /**
     * Formats an HTML response based on the HTML code provided. 
     * 
     * @responseCode {int} The HTTP response code.
     * @customMessage {sring} Optional. A custom message to be sent back as part of thee response.
     * @return {object} Response object with HTTML status code and message.
     */

    switch (responseCode) {
        case 200:
            return {
                'headers': {
                    'Content-Type': 'text/plain'
                },
                'statusCode': 200,
                'body': (customMessage) ? customMessage : 'OK'
            };

        case 400:
            return {
                'headers': {
                    'Content-Type': 'text/plain'
                },
                'statusCode': 200,
                'body': (customMessage) ? customMessage : 'BAD REQUEST'
            };

        case 403:
            return {
                'headers': {
                    'Content-Type': 'text/plain'
                },
                'statusCode': 403,
                'body': (customMessage) ? customMessage : 'FORBIDDEN'
            };

        case 500:
            return {
                'headers': {
                    'Content-Type': 'text/plain'
                },
                'statusCode': 500,
                'body': customMessage
            };
    }
}



module.exports = { 
    channelType,
    inboundEntrypoint,
    outboundEntrypoint,
    getName, 
    downloadAttachment, 
    sendMessage, 
    sendAttachment, 
    setSenderAction, 
    uploadAttachmentAfterConsent
};