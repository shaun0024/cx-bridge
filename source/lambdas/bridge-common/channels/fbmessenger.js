'use strict';

/**
 * MODULES AND OTHER DEPENDENCIES
 */
const axios = require('axios');
const crypto = require('crypto');
const integrations = require('../integrations/integrations');
const utils = require('../base/utils');

/**
 * LOGGER
 */
const logger = require('pino')({
    'base': null,
    'timestamp': false,
    'level': ('LOGGING_LEVEL' in process.env) ? process.env.LOGGING_LEVEL.toLowerCase() : 'info'
}).child({ 'module': 'CHANNEL/FBMESSENGER' });

/**
 * GLOBAL VARIABLES
 */
const apiBaseUrl = 'https://graph.facebook.com';
const apiVersion = '16.0';
const fields = [ 'first_name', 'last_name' ];



function channelType() { return 'fbmessenger'; }



async function inboundEntrypoint(config, event) {
    /**
     * Processes the inbound events from AWS API Gateway and returns a HTML response required 
     * by Facebook and additional data, if any.
     *
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @event {object} The complete event from the AWS API Gateway.
     * @return {object} Returns an object containing one to two keys: response and messages.
     */

    logger.debug({
        'operation': 'INBOUND',
        'config': config,
        'event': event
    });

    if (event.httpMethod.toLowerCase() == 'get') {
        return await verifyWebhook(config, event.queryStringParameters);
    } else if (event.httpMethod.toLowerCase() == 'post') {
        return await processInboundMessages(config, event);
    }
}



async function outboundEntrypoint(config, message) {
    /**
     * Processes outbound messages from AWS SQS to be sent to Facebook.
     *
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @message {object} The message to be sent.
     * @return {string} Returns the receipt id of the message from Facebook.
     */

    const loggerBase = { 'operation': 'OUTBOUND' };

    logger.debug({
        'config': config,
        'message': message,
        ...loggerBase
    });

    const supportedContentTypes = [
        'text/plain',
        'attachment'
    ];

    if (!(supportedContentTypes.includes(message.ContentType))) {
        logger.error({
            'error': 'Unsupported content type',
            ...loggerBase
        });
        return null;
    }

    let receiptId;

    if (message.ContentType == 'text/plain') {
        receiptId = await sendMessage(config, message);
    }

    if (message.ContentType == 'attachment') {
        receiptId = await sendAttachment(config, message);
    }

    await setSenderAction(config, message.Patron.ChannelId, 'mark_seen');

    return receiptId;
}



async function verifyWebhook(config, queryStringParameters) {
    /**
     * Performs verification of webhook registration from Facebook.
     *
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @queryStringParameters {string} The query string sent from Facebook as part of the verification process.
     * @return {object} Returns an object containing one to two keys: response and messages.
     */

    const loggerBase = { 'operation': 'VERIFY_WEBHOOK' };

    logger.debug({
        'config': config,
        'queryStringParameters': queryStringParameters,
        ...loggerBase
    });

    const secrets = await utils.getSecrets(config.Channel.SecretArn);

    const mode = queryStringParameters['hub.mode'];
    const verifyToken = queryStringParameters['hub.verify_token'];
    const challenge = queryStringParameters['hub.challenge'];

    if (!mode && !verifyToken) {
        logger.error({
            'error': 'One or query strings for webhook verification is missing',
            ...loggerBase
        });
        return {
            'response': formatApiResponse(400, 'One or query strings for webhook verification is missing')
        };
    }

    if (mode === 'subscribe' && verifyToken === secrets.verifyToken) {
        logger.info({
            'verified': true,
            ...loggerBase
        });
        return {
            'response': formatApiResponse(200, challenge)
        };
    } else {
        logger.error({
            'verified': false,
            ...loggerBase
        });
        return {
            'response': formatApiResponse(403)
        };
    }
}



async function getName(config, patronChannelId, secrets = null) {
    /**
     * Gets the basic details of the patron directly from Facebook or 
     * from an integration point (e.g. Salesforce, ServiceNow).
     *
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @patronChannelId {string} The Facebook id of the patron.
     * @secrets {object} Optional. The object containing the secrets for Facebook.
     * @return {string} Returns the details of the patron.
     */

    async function getNameFromFacebook(config, patronChannelId, secrets = null) {
        /**
         * Gets the basic details of the patron directly from Facebook.
         *
         * @config {object} Config object containing details of the platform, the channel and integration.
         * @patronChannelId {string} The Facebook id of the patron.
         * @secrets {object} Optional. The object containing the secrets for Facebook.
         * @return {object} Returns the details of the patron.
         */

        const loggerBase = { 'operation': 'GET_NAME_FROM_FACEBOOK' };

        try {
            secrets = (!secrets) ? await utils.getSecrets(config.Channel.SecretArn) : secrets;

            const axiosConfig = {
                'url': `${apiBaseUrl}/${patronChannelId}?fields=${fields.join(',')}&access_token=${secrets.accessToken}`,
                'method': 'get'
            };

            logger.debug({
                'axiosConfig': axiosConfig,
                ...loggerBase
            });

            const response = await axios(axiosConfig);

            logger.debug({
                'response': response.data,
                ...loggerBase
            });

            return {
                'ChannelId': response.data.id,
                'FirstName': response.data.first_name,
                'LastName': response.data.last_name,
                'DisplayName': `${response.data.first_name} ${response.data.last_name}`.trim()
            };
        } catch (error) {
            logger.error({ 
                'error': error.stack, 
                ...loggerBase
            });

            return {
                'ChannelId': patronChannelId,
                'FirstName': 'unknown',
                'LastName': 'unknown',
                'DisplayName': 'unknown'
            };
        }
    }

    const loggerBase = { 'operation': 'GET_NAME' };

    logger.debug({
        'config': config,
        'patronChannelId': patronChannelId,
        ...loggerBase
    });

    let patron;
    if (config.Integration) {
        patron = await integrations.getCustomerDetails(config, patronChannelId);

        if (!patron) {
            patron = await getNameFromFacebook(config, patronChannelId, secrets);
            patron.IntegrationName = config.Integration.FriendlyName;
            patron.IntegrationId = 'unknown';
        }
    } else {
        patron = await getNameFromFacebook(config, patronChannelId, secrets);
    }

    logger.debug({
        'patron': patron,
        ...loggerBase
    });

    return patron;
}



async function processInboundMessages(config, event) {
    /**
     * Process messages sent from the Facebook webhook into 
     * the Bridge format for queueing and further processing.
     *
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @event {object} The complete event from the AWS API Gateway.
     * @return [{object}] Returns an array of formatted messages to be queued for further processing.
     */

    async function verifyHeader(config, headerSignature, bodyRaw, secrets = null) {
        /**
         * Performs verification of request headers for webhook events sent from Facebook.
         *
         * @config {object} Config object containing details of the platform, the channel and integration.
         * @headerSignature {string} The signature in the header sent from Facebook as part of the request.
         * @bodyRaw {string} The raw body of the request.
         * @secrets {object} Optional. The object containing the secrets for Facebook.
         * @return {boolean} Returns true if verified, false if not.
         */

        const loggerBase = { 'operation': 'VERIFY_HEADER' };

        logger.debug({
            'config': config,
            'headerSignature': headerSignature,
            'bodyRaw': bodyRaw,
            ...loggerBase
        });

        secrets = (!secrets) ? await utils.getSecrets(config.Channel.SecretArn) : secrets;

        const hmac = (crypto.createHmac('sha1', secrets.appSecret)).update(bodyRaw, 'utf-8');

        if (headerSignature == `sha1=${hmac.digest('hex')}`) {
            logger.info({
                'verifiedHeader': true,
                ...loggerBase
            });
            return true;
        } else {
            logger.error({
                'verifiedHeader': false,
                ...loggerBase
            });
           return false;
        }
    }


    function formatMessage(config, patron, messageId, contentType, content, direction = 'toPlatform') {
        /**
         * Formats the message into the Bridge format based on the content type.
         *
         * @config {object} Config object containing details of the platform, the channel and integration.
         * @patron {object} Object containing the details of the patron / customer.
         * @messageId {string} The id of the message.
         * @contentType {string} The type of content. Accepted values are 'text/plain' and 'attachment'
         * @content {string || object} The string of the message to be sent or an object containt details of the attachment.
         * @direction {string} Optional. The destination of the message. Accepted values are 'toChannel' or 'toPlatform'.
         * @return {object} Returns a message in the Bridge format that can be queued.
         */

        const formatted = {
            'direction': direction,
            'config': config,
            'message': {
                'Patron': patron,
                'MessageId': messageId,
                'ContentType': contentType,
                'Metadata': null
            }
        };

        switch (contentType) {
            // case ('publicComment'):
            //     formatted.message.Content = content;
            //     break;

            case ('text/plain'):
                formatted.message.Content = content;
                break;

            case ('attachment'):
                formatted.message.Content = {
                    'Type': content.type,
                    'Url': content.payload.url
                };
                break;
        }

        return formatted;
    }

    const loggerBase = { 'operation': 'PROCESS_MESSAGES' };

    const messages = [];

    const body = JSON.parse(event.body);

    if ((body.object != 'page') || (body.entry.length == 0)) return messages;

    const secrets = await utils.getSecrets(config.Channel.SecretArn);

    if (!(await verifyHeader(config, event.headers['X-Hub-Signature'], event.body, secrets))) {
        return formatApiResponse(403, 'Header verification failed');
    }

    let record, patron;
    for (let i = 0; i < body.entry.length; i++) {
        if ('messaging' in body.entry[0]) {
            for (let j = 0; j < body.entry[i].messaging.length; j++) {
                record = body.entry[i].messaging[j];

                patron = {
                    'ChannelId': record.sender.id
                };

                if ('message' in record) {
                    if (('text' in record.message) && !('quick_reply' in record.message)) {
                        messages.push(formatMessage(config, patron, record.message.mid, 'text/plain', record.message.text));
                    } else if (('text' in record.message) && ('quick_reply' in record.message)) {
                        messages.push(formatMessage(config, patron, record.message.mid, 'text/plain', record.message.quick_reply.payload));
                    }

                    if ('attachments' in record.message) {
                        for (let k = 0; k < record.message.attachments.length; k++) {
                            messages.push(formatMessage(config, patron, record.message.mid, 'attachment', record.message.attachments[k]));
                        }
                    }
                }
            }
        }
        // } else if ('changes' in body.entry[0]) {
        //     for (let j = 0; j < body.entry[i].changes.length; j++) {
        //         record = body.entry[i].changes[j];
        //         if (record.value.item == 'comment') {

        //             patron = {
        //                 'ChannelId': record.value.from.id
        //             };

        //             messages.push(formatMessage(config, patron, record.value.comment_id, 'publicComment', record.value.message));
        //         }
        //     }
        // }
    }

    logger.debug({
        'messages': messages,
        ...loggerBase
    });

    return {
        'response': formatApiResponse(200, 'OK'),
        'messages': messages
    };
}



async function downloadAttachment(config, message) {
    /**
     * Processes inbound attachments from the queue and downloads them from Facebook.
     *
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @message {object} The message from the queue containing the details of the attachment to be downloaded.
     * @return {object} Returns an attachment object that will be used to upload the file into the platform.
     */

    const loggerBase = { 'operation': 'DOWNLOAD_ATTACHMENT_FROM_FACEBOOK' };

    logger.debug({
        'config': config,
        'message': message,
        ...loggerBase
    });

    const axiosConfig = {
        'url': message.Content.Url,
        'method': 'get',
        'responseType': 'arraybuffer'
    };

    logger.debug({
        'axiosConfig': axiosConfig,
        ...loggerBase
    });

    const response = await axios(axiosConfig);

    const attachment = {
        'MessageId': message.MessageId,
        'AttachmentName': response.config.url.split('/').pop().split('?')[0],
        'AttachmentSizeInBytes': response.data.byteLength,
        'ContentType': response.headers['content-type'],
        'Content': response.data
    };

    logger.debug({
        'MessageId': message.MessageId,
        'AttachmentName': response.config.url.split('/').pop().split('?')[0],
        'AttachmentSizeInBytes': response.data.byteLength,
        'ContentType': response.headers['content-type'],
        ...loggerBase
    });

    return attachment;
}



async function sendAttachment(config, message) {
    /**
     * Processes outbound attachments from the queue and uploads into Facebook.
     *
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @message {object} The message from the queue containing the details of the attachment to be uploaded to Facebook.
     * @return {string} Returns the message id / receipt from Facebook.
     */

    const loggerBase = { 'operation': 'SEND_ATTACHMENT_TO_FACEBOOK' };

    logger.debug({ 
        'config': config,
        'message': message, 
        ...loggerBase
    });

    const secrets = await utils.getSecrets(config.Channel.SecretArn);

    let fileType, body;

    // if (message.Content.Type.includes('image')) {
    //     fileType = 'image';
    // } else {
    //     fileType = 'file';
    // }

    if (config.Channel.UploadAttachments) {
        body = {
            'recipient': {
                'id': message.Patron.ChannelId
            },
            'message': {
                "attachment": {
                    'type': (message.Content.Type.includes('image')) ? 'image' : 'file',
                    'payload': {
                        'url': message.Content.Url,
                        'is_reusable': false
                    }
                }
            }
        };
    } else {
        body = {
            'recipient': {
                'id': message.Patron.ChannelId
            },
            'message': {
                "attachment": {
                    'type': 'template',
                    'payload': {
                        'template_type': 'button',
                        'text': config.Channel.AttachmentMessage,
                        'buttons':[
                            {
                                'type': 'web_url',
                                'url': message.Content.Url,
                                'title': message.Content.Filename,
                                'webview_height_ratio': 'compact'
                            }
                        ]
                    }
                }
            }
        };
    }

    const axiosConfig = {
        'url': `${apiBaseUrl}/v${apiVersion}/${secrets.pageId}/messages?access_token=${secrets.accessToken}`,
        'method': 'post',
        'data': body
    };

    logger.debug({ 
        'axiosConfig': axiosConfig, 
        ...loggerBase
    });

    const response = await axios(axiosConfig);

    return response.data.message_id;
}



async function sendMessage(config, message) {
    /**
     * Processes text messages from the queue to be sent to Facebook.
     *
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @message {object} The message from the queue containing the details of the message.
     * @return {string} Returns the message id from Facebook.
     */

    const loggerBase = { 'operation': 'SEND_MESSAGE_TO_FACEBOOK' };

    logger.debug({
        'config': config, 
        'message': message, 
        ...loggerBase 
    });

    const secrets = await utils.getSecrets(config.Channel.SecretArn);

    let messageData;
    try {
        messageData = JSON.parse(message.Content.replace('\"', '"'));
    } catch (error) {
        messageData = { 'txt': message.Content };
    }

    const body = {
        'recipient': {
            'id': message.Patron.ChannelId
        },
        'messaging_type': 'RESPONSE',
        'message': {
            'text': messageData.txt
        }
    };

    if ('btns' in messageData) {
        const buttons = [];

        messageData.btns.forEach((button) => {
            buttons.push({
                'content_type': 'text',
                'title': button.text,
                'payload': button.value
            });
        });

        body.message.quick_replies = buttons;
    }

    const axiosConfig = {
        'url': `${apiBaseUrl}/v${apiVersion}/${secrets.pageId}/messages?access_token=${secrets.accessToken}`,
        'method': 'post',
        'data': body
    };

    logger.debug({ 
        'axiosConfig': axiosConfig,
        ...loggerBase
    });

    const response = await axios(axiosConfig);

    logger.debug({ 
        'response': response.data,
        ...loggerBase
    });

    logger.info({ 
        'receiptId': response.data.message_id,
        ...loggerBase
    });

    return response.data.message_id;
}


// async function replyToComment(config, message) {
//     /**
//      * Processes text messages from the queue to be sent to Twitter.
//      *
//      * @config {object} Config object containing details of the platform, the channel and integration.
//      * @message {object} The message from the queue.
//      * @return {string} Returns the message id from Twitter.
//      */

//     const loggerBase = { 'operation': 'REPLY_TO_COMMENT' };

//     logger.debug({ 
//         'config': config, 
//         'message': message, ...loggerBase
//     });

//     const secrets = await utils.getSecrets(config.Channel.SecretArn);

//     const replyMessage = config.Channel.ResponseMessage.replace('FacebookId', `[${message.Patron.ChannelId}]`);

//     const url = `${apiBaseUrl}/${message.MessageId}/comments?message=${replyMessage}&access_token=${secrets.accessToken}`;

//     const axiosConfig = {
//         'url': url,
//         'method': 'post'
//     };

//     logger.debug({ 
//         'axiosConfig': axiosConfig, 
//         ...loggerBase
//     });

//     const response = await axios(axiosConfig);

//     return response.data.id_str;
// }



async function setSenderAction(config, patronChannelId, action) {
    /**
     * Send an action (e.g. mark read, typing indicator) to Facebook.
     *
     * @config {object} Config object containing details of the platform, the channel and integration.
     * @patronChannelId {object} The Facebook scoped id of the user to send the action to.
     * @action {string} Accepted values are 'typing_on', 'typing_off', 'mark_seen'
     * @return {null} Returns nothing.
     */

    const supportedActions = {
        'typing': 'typing_on',
        'mark_seen': 'mark_seen'
    };

    const loggerBase = { 'operation': 'SEND_ACTION_TO_FACEBOOK' };

    logger.debug({
        'patronChannelId': patronChannelId,
        'action': action,
        ...loggerBase
    });

    if (!(action.toLowerCase() in supportedActions)) return;

    try {
        const secrets = await utils.getSecrets(config.Channel.SecretArn);

        const body = {
            'sender_action': supportedActions[action],
            'recipient': {
                'id': patronChannelId
            }
        };
    
        const axiosConfig = {
            'url': `${apiBaseUrl}/v${apiVersion}/${secrets.pageId}/messages?access_token=${secrets.accessToken}`,
            'method': 'post',
            'data': body
        };
    
        logger.debug({
            'axiosConfig': axiosConfig,
            ...loggerBase
        });
    
        await axios(axiosConfig);
    } catch (error) {
        logger.error({
            'error': error.stack,
            ...loggerBase
        });
    }

    return;
}


function formatApiResponse(responseCode, customMessage = null) {
    /**
     * Formats an HTML response based on the HTML code provided. 
     * 
     * @responseCode {int} The HTTP response code.
     * @customMessage {sring} Optional. A custom message to be sent back as part of thee response.
     * @return {object} Response object with HTTML status code and message.
     */

    switch (responseCode) {
        case 200:
            return {
                'headers': {
                    'Content-Type': 'text/plain'
                },
                'statusCode': 200,
                'body': (customMessage) ? customMessage : 'OK'
            };

        case 400:
            return {
                'headers': {
                    'Content-Type': 'text/plain'
                },
                'statusCode': 200,
                'body': (customMessage) ? customMessage : 'BAD REQUEST'
            };

        case 403:
            return {
                'headers': {
                    'Content-Type': 'text/plain'
                },
                'statusCode': 403,
                'body': (customMessage) ? customMessage : 'FORBIDDEN'
            };

        case 500:
            return {
                'headers': {
                    'Content-Type': 'text/plain'
                },
                'statusCode': 500,
                'body': customMessage
            };
    }
}



module.exports = {
    channelType,
    inboundEntrypoint,
    outboundEntrypoint,
    getName,
    downloadAttachment,
    sendMessage,
    sendAttachment,
    setSenderAction
};