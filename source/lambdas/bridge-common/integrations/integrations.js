'use strict';

/**
 * MODULES AND OTHER DEPENDENCIES
 */
const salesforce = require('./integrations/salesforce');
// const servicenow = require('./integrations/servicenow');
// const azuread = require('./integrations/azuread');


/**
 * LOGGER
 */
  const logger = require('pino')({
     'base': null,
     'timestamp': false,
     'level': ('LOGGING_LEVEL' in process.env) ? process.env.LOGGING_LEVEL.toLowerCase() : 'info'
 }).child({ 'module': 'INTEGRATIONS' });



async function getCustomerDetails(config, patronChannelId) {
    const loggerBase = { 'operation': 'GET_CUSTOMER_DETAILS', 'integration': config.Integration.SubCategory };

    switch(config.Integration.SubCategory) {
        case 'salesforce':
            logger.debug({ 'supported': true, ...loggerBase });
            return await salesforce.getCustomerDetails(config, patronChannelId);

        // case 'servicenow':
        //     logger.debug({ 'supported': true, ...loggerBase });
        //     return await servicenow.getCustomerDetails(config, patronChannelId);

        // case 'azuread':
        //     logger.debug({ 'supported': true, ...loggerBase });
        //     return await azuread.getCustomerDetails(config, patronChannelId);

        default:
            logger.debug({ 'supported': false, ...loggerBase });
            return;
    }
}







module.exports = { getCustomerDetails };