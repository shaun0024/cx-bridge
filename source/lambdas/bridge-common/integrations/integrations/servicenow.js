'use strict';

// Modules required by this file
const axios = require('axios');
const utils = require('../../base/utils');
// const helpers = require('../utils/helpers');

const apiVersion = 'v2';

async function getCustomerDetails(channelConfig, customerChannelId) {
    const logData = {
        'type': 'SERVICENOW',
        'operation': 'GET_CUSTOMER_DETAILS',
        'metadata': {}
    };

    const secrets = await utils.getSecrets(channelConfig.RoleArn, channelConfig.ExternalId, channelConfig.Integration.SecretArn);

    const accessData = await getAccessToken(secrets)

    // let soqlQuery = `SELECT Id, ${channelConfig.Integration.FirstNameField}, ${channelConfig.Integration.LastNameField} FROM ${channelConfig.Integration.ContactTable}`

    let queryFields;
    if (channelConfig.Channel == 'fbmessenger') {
        queryFields = channelConfig.Integration.FbMessengerFields;
    } else if (channelConfig.Channel == 'twitter') {
        queryFields = channelConfig.Integration.TwitterFields;
    } else if (channelConfig.Channel == 'sms') {
        queryFields = channelConfig.Integration.PhoneFields;
    }

    // active=true^phone=+61481231679^NQactive=true^mobile_phone=+61481231679

    let sysparmQuery;
    for (let i = 0; i < queryFields.length; i++) {
        if (i == 0) {
            sysparmQuery += `active=true^${queryFields[i]}=${customerChannelId}`
        } else {
            sysparmQuery += `^NQactive=true^${queryFields[i]}=${customerChannelId}`
        }
    }

    const axiosConfig = {
        'url': `${accessData.InstanceUrl}/api/now/${apiVersion}/table/${channelConfig.Integration.ContactTable}?sysparm_limit=2&sysparm_fields=${queryFields.join()}&sysparm_query=${encodeURI(sysparmQuery)}`,
        'method': 'get',
        'headers': { 
            'Authorization': `Bearer ${accessData.AccessToken}`,
            'Content-Type': 'application/json'
        }
    };

    logData.metadata.axiosConfig = axiosConfig;

    return axios(axiosConfig).then(async (result) => {
        if (result.data.result.length == 1) {
            logData.metadata.status = 'SUCCESS';
            console.debug(JSON.stringify(logData));

            return {
                'IntegrationSource': 'servicenow',
                'IntegrationId': channelConfig.Integration.IntegrationId,
                'CustomerChannelId': customerChannelId,
                'CustomerIntegrationId': result.data.result[0].sys_id,
                'Name': `${result.data.result[0][channelConfig.Integration.FirstNameField]} ${result.data.result[0][channelConfig.Integration.LastNameField]}`,
                'FirstName': result.data.result[0][channelConfig.Integration.FirstNameField],
                'LastName': result.data.result[0][channelConfig.Integration.LastNameField]
            };
        } else {
            logData.metadata.status = 'FAILED';
            logData.metadata.error = 'More than 1 record found';

            return null;
        }
    }).catch((error) => {
        logData.metadata.status = 'FAILED';
        logData.metadata.error = error.response.data;
        console.error(JSON.stringify(logData));

        return null;
    });
}


async function getAccessToken(secrets) {
    const logData = {
        'type': 'SERVICENOW',
        'operation': 'GET_ACCESS_TOKEN',
        'metadata': {}
    };

    const url = `https://${secrets.instanceName}.service-now.com/oauth_token.do`;

    const axiosConfig = {
        'url': url,
        'method': 'post',
        'params': {
            'grant_type': 'password',
            'username': secrets.username,
            'password': `${secrets.password}${secrets.securityToken}`,
            'client_id': secrets.consumerKey,
            'client_secret': secrets.consumerSecret
        }
    };

    logData.metadata.axiosConfig = axiosConfig;

    return axios(axiosConfig).then(async (result) => {
        logData.metadata.status = 'SUCCESS';
        console.debug(JSON.stringify(logData));

        return { 'InstanceUrl': `https://${secrets.instanceName}.service-now.com`, 'AccessToken': result.data.access_token };
    }).catch((error) => {
        logData.metadata.status = 'FAILED';
        logData.metadata.error = error.response.data;
        console.error(JSON.stringify(logData));

        return null;
    });
}

module.exports = { getCustomerDetails };