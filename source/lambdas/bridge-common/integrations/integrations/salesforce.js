'use strict';

/**
 * MODULES AND OTHER DEPENDENCIES
 */
const axios = require('axios');
const utils = require('../../base/utils');


/**
 * LOGGER
 */
 const logger = require('pino')({
    'base': null,
    'timestamp': false,
    'level': ('LOGGING_LEVEL' in process.env) ? process.env.LOGGING_LEVEL.toLowerCase() : 'info'
}).child({ 'module': 'INTEGRATIONS/SALESFORCE' });


async function getCustomerDetails(config, patronChannelId) {
    /**
     * Gets the basic details of the patron directly from Twitter or from an integration point (e.g. Salesforce, ServiceNow).
     * 
     * @config {object} Config object containing details of Amazon Connect, the channel and integration.
     * @patronChannelId {string} The signature in the header sent from Twitter as part of the request.
     * @secrets {object} Optional. The object containing the secrets for Twitter.
     * @return {string} Returns the details of the patron.
     */

     const loggerBase = { 
        'operation': 'GET_NAME',
        'integrationType': config.Integration.SubCategory,
        'integrationName': config.Integration.FriendlyName
    };

    logger.debug({ 'status': 'VERBOSE', 'config': config, 'patronChannelId': patronChannelId, ...loggerBase });

    try {
        const secrets = await utils.getSecrets(config.Integration.SecretArn, config.Connect.RoleArn, config.Connect.ExternalId);

        const accessData = await getAccessToken(secrets, config.Integration.IsTest);

        let queryEndpoint, querySyntax;
        if (config.Channel.SubCategory == 'sms') {
            queryEndpoint = 'search';

            querySyntax = `FIND {\\${patronChannelId}} IN PHONE FIELDS RETURNING ${config.Integration.Sms.Table}(Id, ${config.Integration.FirstNameField}, ${config.Integration.LastNameField})`;
        } else {
            let queryFields;

            queryEndpoint = 'query';

            querySyntax = `SELECT Id, ${config.Integration.FirstNameField}, ${config.Integration.LastNameField} FROM `;

            switch (config.Channel.SubCategory) {
                case 'fbmessenger':
                    querySyntax += config.Integration.FbMessenger.Table
                    queryFields = config.Integration.FbMessenger.Fields;
                    break;
    
                case 'twitter':
                    querySyntax += config.Integration.Twitter.Table
                    queryFields = config.Integration.Twitter.Fields;
                    break;

                case 'slack':
                    querySyntax += config.Integration.Slack.Table
                    queryFields = config.Integration.Slack.Fields;
                    break;
                    
                default:
                    throw 'unsupported';
            }
    
            for (let i = 0; i < queryFields.length; i++) {
                if (i == 0) {
                    querySyntax += ` WHERE ${queryFields[i]} = '${patronChannelId}'`;
                } else {
                    querySyntax += ` OR ${queryFields[i]} = '${patronChannelId}'`;
                }
            }
        }


        const axiosConfig = {
            'url': `${accessData.InstanceUrl}/services/data/v${config.Integration.ApiVersion}/${queryEndpoint}`,
            'method': 'get',
            'headers': { 
                'Authorization': `Bearer ${accessData.AccessToken}`,
                'Content-Type': 'application/json'
            },
            'params': {
                'q': querySyntax
            }
        };

        logger.debug({ 'status': 'VERBOSE', 'axiosConfig': axiosConfig, ...loggerBase});

        const response = await axios(axiosConfig);

        logger.debug({ 'status': 'VERBOSE', 'response': response.data, ...loggerBase});
        logger.info({ 'status': 'SUCCESS', ...loggerBase });

        let records;
        if (queryEndpoint == 'search') {
            records = response.data.searchRecords;
        } else {
            records = response.data.records;
        }

        if (records.length != 1) {
            return null;
        }

        return {
            'ChannelId': patronChannelId,
            'IntegrationName': config.Integration.FriendlyName,
            'IntegrationId': records[0].Id,
            'DisplayName': `${records[0][config.Integration.FirstNameField]} ${records[0][config.Integration.LastNameField]}`,
            'FirstName': records[0][config.Integration.FirstNameField],
            'LastName': records[0][config.Integration.LastNameField],
            'RecordUrl': `${accessData.InstanceUrl.replace('.my.salesforce.com', '.lightning.force.com')}/lightning/r/${records[0].attributes.type}/${records[0].Id}/view`
        };
    } catch (error) {
        logger.error({ 'status': 'FAILED', 'error': error, ...loggerBase });     
        return null;
    }
}


async function getAccessToken(secrets, isTest) {
    /**
     * Obtains an access token for interacting with the Microsoft Graph API.
     * 
     * @config {object} Config object containing details of Amazon Connect, the channel and integration.
     * @secrets {object} Optional. The body of the request.
     * @return {string} Returns the message id from Twitter.
     */

    const loggerBase = { 'operation': 'GET_ACCESS_TOKEN', 'isTest': isTest };

    try {
        const url = (isTest) ? 'https://test.salesforce.com/services/oauth2/token' : 'https://login.salesforce.com/services/oauth2/token';

        const axiosConfig = {
            'url': url,
            'method': 'post',
            'params': {
                'grant_type': 'password',
                'username': secrets.username,
                'password': `${secrets.password}${secrets.securityToken}`,
                'client_id': secrets.consumerKey,
                'client_secret': secrets.consumerSecret
            }
        };
    
        logger.debug({ 'status': 'VERBOSE', 'axiosConfig': axiosConfig, ...loggerBase});
    
        const response = await axios(axiosConfig);
    
        logger.info({ 'status': 'SUCCESS', ...loggerBase});
    
        return {
            'InstanceUrl': response.data.instance_url, 
            'AccessToken': response.data.access_token
        };
    } catch (error) {
        logger.error({ 'status': 'FAILED', 'error': error, ...loggerBase });     
        throw error;
    }
}

module.exports = { getCustomerDetails };