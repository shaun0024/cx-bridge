'use strict';

// Modules required by this file
const axios = require('axios');
const utils = require('../../base/utils');
// const helpers = require('../helpers');



async function getCustomerDetails(channelConfig, customerChannelId) {
    const logData = {
        'type': 'AZUREAD',
        'operation': 'GET_CUSTOMER_DETAILS',
        'metadata': {}
    };

    const secrets = await utils.getSecrets(channelConfig.RoleArn, channelConfig.ExternalId, channelConfig.Integration.SecretArn);

    const accessData = await getAccessToken(secrets)

    let soqlQuery = `SELECT Id, ${channelConfig.Integration.FirstNameField}, ${channelConfig.Integration.LastNameField} FROM ${channelConfig.Integration.ContactTable}`

    let queryFields;
    if (channelConfig.Channel == 'msteams') {
        queryFields = channelConfig.Integration.MsTeamsFields;
    } else if (channelConfig.Channel == 'sms') {
        queryFields = channelConfig.Integration.PhoneFields;
    }


    for (let i = 0; i < queryFields.length; i++) {
        if (i == 0) {
            soqlQuery += ` WHERE ${queryFields[i]} = '${customerChannelId}'`
        } else {
            soqlQuery += ` OR ${queryFields[i]} = '${customerChannelId}'`
        }
    }

    const axiosConfig = {
        'url': `${accessData.InstanceUrl}/services/data/v${secrets.apiVersion}/query`,
        'method': 'get',
        'headers': { 
            'Authorization': `Bearer ${accessData.AccessToken}`,
            'Content-Type': 'application/json'
        },
        'params': {
            'q': soqlQuery
        }
    };

    logData.metadata.axiosConfig = axiosConfig;

    return axios(axiosConfig).then(async (result) => {
        if (result.data.totalSize == 1) {
            logData.metadata.status = 'SUCCESS';
            console.debug(JSON.stringify(logData));

            return {
                'IntegrationSource': 'salesforce',
                'IntegrationId': channelConfig.Integration.IntegrationId,
                'CustomerChannelId': customerChannelId,
                'CustomerIntegrationId': result.data.records[0].Id,
                'Name': `${result.data.records[0][channelConfig.Integration.FirstNameField]} ${result.data.records[0][channelConfig.Integration.LastNameField]}`,
                'FirstName': result.data.records[0][channelConfig.Integration.FirstNameField],
                'LastName': result.data.records[0][channelConfig.Integration.LastNameField]
            };
        } else {
            logData.metadata.status = 'FAILED';
            logData.metadata.error = 'More than 1 record found';

            return null;
        }
    }).catch((error) => {
        logData.metadata.status = 'FAILED';
        logData.metadata.error = error.response.data;
        console.error(JSON.stringify(logData));

        return null;
    });
}


async function getAccessToken(secrets) {
    const logData = {
        'type': 'AZUREAD',
        'operation': 'GET_ACCESS_TOKEN',
        'metadata': {}
    };

    const url = `https://login.microsoftonline.com/${secrets.tenantId}/oauth2/token`;

    const axiosConfig = {
        'url': url,
        'method': 'post',
        'params': {
            'grant_type': 'password',
            'username': secrets.username,
            'password': `${secrets.password}`,
            'client_id': secrets.applicationId,
            'client_secret': secrets.applicationSecret,
            'resource': 'https://graph.microsoft.com'
        }
    };

    logData.metadata.axiosConfig = axiosConfig;

    return axios(axiosConfig).then(async (result) => {
        logData.metadata.status = 'SUCCESS';
        console.debug(JSON.stringify(logData));

        return result.data.access_token;
    }).catch((error) => {
        logData.metadata.status = 'FAILED';
        logData.metadata.error = error.response.data;
        console.error(JSON.stringify(logData));

        return null;
    });
}

module.exports = { getCustomerDetails };