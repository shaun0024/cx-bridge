'use strict';

/**
 * MODULES AND OTHER DEPENDENCIES
 */
const { SQSClient, SendMessageCommand } = require('@aws-sdk/client-sqs');
const sqs = new SQSClient({ 'apiVersion': '2012-11-05' });
const channelConfig = require('./base/config');

/**
 * LOGGER
 */
const logger = require('pino')({
    'base': null,
    'timestamp': false,
    'name': 'API_RESPONDER', 
    'level': ('LOGGING_LEVEL' in process.env) ? process.env.LOGGING_LEVEL.toLowerCase() : 'info'
});

/**
 * MANDATORY ENVIRONMENT VARIABLE CHECKS
 */
if (
    !process.env.CHAT_MESSAGE_QUEUE_URL || 
    !process.env.CONFIG_TABLE
) throw 'One or more environment variables and/or parameters not defined';

/**
 * GLOBAL VARIABLES
 */
const configTable = process.env.CONFIG_TABLE;
const chatMessageQueueUrl = process.env.CHAT_MESSAGE_QUEUE_URL;
const publicEngagementQueueUrl = process.env.PUBLIC_ENGAGEMENT_QUEUE_URL;



exports.handler = async (event) => {
    /**
     * Basic handler to forward events to logical functions.
     * 
     * @event {object} An API Gateway formatted event.
     * @return {object} Response object with HTTML status code and message.
     */

    if ('warmup' in event) return;

    const loggerBase = { 'operation': 'INCOMING_EVENT' };

    logger.debug({
        'event': event, 
        ...loggerBase
    });
    
    const response = await controller(event);

    logger.debug({
        'response': response, 
        ...loggerBase
    });

    return response;
};



async function controller(event) {
    /**
     * Checks if configuration exists using path parameters and 
     * queues messages in SQS for further processing.
     * 
     * @event {object} An API Gateway formatted event.
     * @return {object} Response object with HTTML status code and message.
     */

    const loggerBase = { 
        'operation': 'CONTROLLER',
        'event': event
    };

    if (event.queryStringParameters == null) {
        return formatApiResponse(400, 'Missing required query strings');
    }

    if (!(event.queryStringParameters.hasOwnProperty('channel_uid'))) {
        return formatApiResponse(400, 'Missing channel_uid');
    }

    try {
        let channel;
        if (event.resource.toLowerCase().includes('/fbmessenger/')) {
            channel = require('./channels/fbmessenger');
        } else if (event.resource.toLowerCase().includes('/slack/')) {
            channel = require('./channels/slack');
        } else if (event.resource.toLowerCase().includes('/msteams/')) {
            channel = require('./channels/msteams');
        // } else if (event.resource.toLowerCase().includes('/twitter/')) {
        //     channelType = 'twitter';
        // } else if (event.resource.toLowerCase().includes('/sms/twilio/')) {
        //     channelType = 'sms/twilio';
        // } else if (event.resource.toLowerCase().includes('/sms/messagemedia/')) {
        //     channelType = 'sms/messagemedia';
        // } else if (event.resource.toLowerCase().includes('/sms/pinpoint/')) {
        //     channelType = 'sms/pinpoint';
        // }
        } else {
            return formatApiResponse(404);
        }

        let platformType;
        if (event.resource.toLowerCase().includes('/connect/')) {
            platformType = 'connect';
        } else if (event.resource.toLowerCase().includes('/lex/')) {
            platformType = 'lex';
        } else {
            return formatApiResponse(404);
        }

        const platformName = event.pathParameters.PlatformName;
        const channelName = event.pathParameters.ChannelName;
        const channelUid = event.queryStringParameters['channel_uid'];

        const config = await channelConfig.getConfigForRequest(configTable, platformType, platformName, channel.channelType(), channelName);
    
        if (config == null) {
            return formatApiResponse(404);
        }

        if (channelUid != config.Channel.Uid) {
            return formatApiResponse(403);
        }

        const { response, messages } = await channel.inboundEntrypoint(config, event);

        let command, messageData;
        if (messages != null) {
            let sessionName;
            for (let i = 0; i < messages.length; i++) {
                sessionName = `${platformType}::${platformName}::${channel.channelType()}::${channelName}::${messages[i].message.Patron.ChannelId}`;

                messageData = {
                    'QueueUrl': (messages[i].message.ContentType == 'publicComment') ? publicEngagementQueueUrl : chatMessageQueueUrl,
                    'MessageGroupId': sessionName,
                    'MessageBody': JSON.stringify(messages[i])
                };
                command = new SendMessageCommand(messageData);
        
                logger.debug({ 
                    'messageData': messageData, 
                    ...loggerBase
                });
        
                try {
                    await sqs.send(command);
                } catch (error) {
                    logger.error({ 
                        'error': error.stack, 
                        ...loggerBase
                    });     
                }
            }
        }

        return response;
    } catch (error) {
        logger.error({ 
            'error': error.stack,
            ...loggerBase
        });
        return formatApiResponse(500, error.stack);
    }
}



function formatApiResponse(responseCode, customMessage = null) {
    /**
     * Formats an HTML response based on the HTML code provided for general non 2xx responses. 
     * 
     * @responseCode {int} The HTTP response code.
     * @customMessage {sring} Optional. A custom message to be sent back as part of thee response.
     * @return {object} Response object with HTTML status code and message.
     */

    const loggerBase = { 'operation': 'RETURN_RESPONSE' };

    logger.debug({ 
        'level': 'VERBOSE', 
        'responseCode': responseCode, 
        ...loggerBase
    });

    switch (responseCode) {
        case 400:
            return {
                'headers': {
                    'Content-Type': 'text/plain'
                },
                'statusCode': 400,
                'body': (customMessage) ? customMessage : 'BAD REQUEST'
            };

        case 403:
            return {
                'headers': {
                    'Content-Type': 'text/plain'
                },
                'statusCode': 403,
                'body': 'Forbidden'
            };

        case 404:
            return {
                'headers': {
                    'Content-Type': 'text/plain'
                },
                'statusCode': 404,
                'body': 'No such platform or channel'
            };

        case 500:
            return {
                'headers': {
                    'Content-Type': 'text/plain'
                },
                'statusCode': 500,
                'body': customMessage
            };
    }
}