'use strict';

/**
 * MODULES AND OTHER DEPENDENCIES
 */
var { unmarshall } = require('@aws-sdk/util-dynamodb');

/**
 * LOGGER
 */
const logger = require('pino')({
    'base': null,
    'timestamp': false,
    'name': 'END_SESSION', 
    'level': ('LOGGING_LEVEL' in process.env) ? process.env.LOGGING_LEVEL.toLowerCase() : 'info'
});



exports.handler = async (event) => {
    /**
     * Basic handler to forward events to logical functions.
     * 
     * @event {object} A DynamoDB stream formatted event.
     * @return {null} 
     */

    if ('warmup' in event) return;

    const loggerBase = { 'operation': 'INCOMING_EVENT' };

    logger.debug({
        'event': event, 
        ...loggerBase
    });

    await controller(event);

    return;
};



async function controller(event) {
    /**
     * Stops the active session based on attributes from the DynamoDB stream.
     * 
     * @event {object} A DynamoDB stream formatted event.
     * @return {null}
     */

    const loggerBase = {
        'operation': 'CONTROLLER',
        'event': event
    };

    let record, sessionData;
    for (let i = 0; i < event.Records.length; i++) {
        if (event.Records[i].eventName == 'REMOVE') {
            record = event.Records[i];

            sessionData = unmarshall(record.dynamodb.OldImage);

            logger.debug({ 'status': 'VERBOSE', 'recordToDelete': event.Records[i], ...loggerBase });

            let platform;
            try {
                if (sessionData.Config.Platform.SubCategory == 'connect') {
                    platform = require('./platform/connect');
                } else if (sessionData.Config.Platform.SubCategory == 'lex') {
                    platform = require('./platform/lex');
                }

                await platform.endSession(sessionData)
            } catch (error) {
                logger.error({ 
                    'error': error.stack, 
                    ...loggerBase
                });
            }
        }
    }
    
    return;
}