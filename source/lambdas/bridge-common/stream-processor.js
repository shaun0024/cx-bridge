'use strict';

/**
 * MODULES AND OTHER DEPENDENCIES
 */
const { SQSClient, SendMessageCommand } = require('@aws-sdk/client-sqs');
const sqs = new SQSClient({ 'apiVersion': '2012-11-05' });
const { S3Client, HeadObjectCommand } = require('@aws-sdk/client-s3');
const s3 = new S3Client({ 'apiVersion': '2006-03-01'});
const connect = require('./platform/connect');
const session = require('./base/session');

/**
 * LOGGER
 */
const logger = require('pino')({
    'base': null,
    'timestamp': false,
    'name': 'STREAM_PROCESSOR', 
    'level': ('LOGGING_LEVEL' in process.env) ? process.env.LOGGING_LEVEL.toLowerCase() : 'info'
});

/**
 * MANDATORY ENVIRONMENT VARIABLE CHECKS
 */
if (
    !process.env.CHAT_MESSAGE_QUEUE_URL || 
    !process.env.SESSIONS_TABLE || 
    !process.env.SESSIONS_TABLE_INDEX
) throw 'One or more environment variables and/or parameters not defined';

/**
 * GLOBAL VARIABLES
 */
const chatMessageQueueUrl = process.env.CHAT_MESSAGE_QUEUE_URL;
const sessionsTable = process.env.SESSIONS_TABLE;
const sessionsTableIndex = process.env.SESSIONS_TABLE_INDEX;
const typingThrottleInterval = process.env.TYPING_THROTTLE_INTERVAL || 7000;
const typingThrottle = {};



exports.handler = async (event) => {
    /**
     * Basic handler to forward events to logical functions.
     * 
     * @event {object} An SQS formatted event.
     * @return {null} Returns nothing.
     */

    if ('warmup' in event) return;

    const loggerBase = { 'operation': 'SNS_EVENT' };

    logger.debug({ 
        'event': event, 
        ...loggerBase
    });

    await controller(event);

    return;
};



async function controller(event) {
    /**
     * Checks if an active chat session exists based on the attributes of the SNS event received.
     * 
     * @event {object} An API Gateway formatted event.
     * @return {object} Response object with HTTML status code and message.
     */

    const loggerBase = { 'operation': 'CONTROLLER' };

    let record, topicArn, message, messageAttributes, instanceId, initialContactId, contactId, sessionData;
    for (let i = 0; i < event.Records.length; i++) {
        if (event.Records[i].Sns.Type == 'Notification') {
            try {
                record = event.Records[i].Sns;
                topicArn = record.TopicArn;
                message = record.Message;
                messageAttributes = record.MessageAttributes;

                if ('ContentType' in messageAttributes && messageAttributes.ContentType.Value == 'application/vnd.amazonaws.connect.event.message.metadata') return;

                instanceId = messageAttributes.InstanceId.Value;
                initialContactId = messageAttributes.InitialContactId.Value;
                contactId = `${instanceId}:${initialContactId}`;
        
                sessionData = await session.getSession(sessionsTable, sessionsTableIndex, 'contactId', contactId);
        
                if (sessionData == null) throw 404;
        
                if (instanceId != sessionData.Config.Platform.InstanceId) throw 403;
        
                if (topicArn != sessionData.Config.Platform.SnsStreamArn) throw 403;
    
                await queueMessage(sessionData, message);

            } catch (error) {
                logger.error({ 
                    'error': error.stack,
                    ...loggerBase
                });
            }
        }
    }

    return;
}



async function queueMessage(sessionData, rawMessage) {
    /**
     * Queues messages in SQS for further processing.
     * 
     * @sessionData {object} The connection data of the active session.
     * @rawMessage {string} The raw content of the SNS message received.
     * @return {null}
     */

    const loggerBase = { 'operation': 'QUEUE_MESSAGE' };

    const message = JSON.parse(rawMessage);
    const patronChannelId = sessionData.SessionName.split('::')[4];

    const channel = require(`./channels/${sessionData.Config.Channel.SubCategory}`);

    let isAgent, endPrompt, agentJoinedPrompt, agentLeftPrompt;
    if ('ContentType' in message) {
        switch(message.ContentType) {
            case 'text/plain':
                switch(message.ParticipantRole) {
                    case 'SYSTEM':
                        await systemMessageHandler(sessionData.SessionName, sessionData.Config, patronChannelId, sessionData.Metadata, message);
                        break;
    
                    case 'AGENT':
                        await agentMessageHandler(sessionData.SessionName, sessionData.Config, patronChannelId, sessionData.Metadata, message);
                        break;
    
                    case 'CUSTOMER':
                        await customerMessageHandler(sessionData.SessionName, sessionData.Config, patronChannelId, sessionData.Metadata, message);
                        break;
                }
                break;
    
            case 'application/vnd.amazonaws.connect.event.chat.ended':
                endPrompt = { 
                    'Id': message.Id, 
                    'Content': 'This conversation has ended.'
                };
                await systemMessageHandler(sessionData.SessionName, sessionData.Config, patronChannelId, sessionData.Metadata, endPrompt, true);
                await session.deleteSession(sessionsTable, sessionData.SessionName);
                if (sessionData.SessionName in typingThrottle) {
                    delete typingThrottle[sessionData.SessionName];
                }
                break;
    
            case 'application/vnd.amazonaws.connect.event.participant.joined':
                isAgent = (message.ParticipantRole == 'AGENT') ? true : false;
                await session.updateCurrentParticipant(sessionsTable, sessionData.SessionName, isAgent);
                if (isAgent) {
                    agentJoinedPrompt = {
                        'Id': message.Id, 
                        'Content': `${message.DisplayName} has joined the conversation.`
                    };
                    await systemMessageHandler(sessionData.SessionName, sessionData.Config, patronChannelId, sessionData.Metadata, agentJoinedPrompt, true);
                }
                break;
                
            case 'application/vnd.amazonaws.connect.event.participant.left':
                isAgent = (message.ParticipantRole == 'AGENT') ? false : true;
                await session.updateCurrentParticipant(sessionsTable, sessionData.SessionName, isAgent);
                if (!isAgent) {
                    agentLeftPrompt = {
                        'Id': message.Id, 
                        'Content': `${message.DisplayName} has left the conversation.`
                    };
                    await systemMessageHandler(sessionData.SessionName, sessionData.Config, patronChannelId, sessionData.Metadata, agentLeftPrompt, true);
                }
                break;
    
            case 'application/vnd.amazonaws.connect.event.transfer.succeeded':
                break;
    
            case 'application/vnd.amazonaws.connect.event.transfer.failed':
                break;
    
            case 'application/vnd.amazonaws.connect.event.typing':
                const prob = { '0': 0.3, '1': 0.7 };
                let i, sum = 0, r = Math.random();
                for (i in prob) {
                    sum += prob[i];
                    if (r <= sum) {
                        logger.debug({
                            'sendingTypingIndicator': true,
                            ...loggerBase
                        })
                        await channel.setSenderAction(sessionData.Config, patronChannelId, 'typing', sessionData.Metadata);
                        break;
                    } else {
                        logger.debug({
                            'sendingTypingIndicator': false,
                            ...loggerBase
                        })
                    }
                }

                break;
        }
    } else if ('Attachments' in message) {
        switch(message.ParticipantRole) {
            case 'SYSTEM':
                await systemAttachmentHandler(sessionData.SessionName, sessionData.Config, sessionData.ConnectionToken, patronChannelId, sessionData.Metadata, message);
                break;
            
            case 'AGENT':
                await agentAttachmentHandler(sessionData.SessionName, sessionData.Config, sessionData.ConnectionToken, patronChannelId, sessionData.Metadata, message);
                break;

            case 'CUSTOMER':
                await customerAttachmentHandler(sessionData.SessionName, sessionData.Config, sessionData.ConnectionToken, patronChannelId, sessionData.Metadata, message);
                break;
        }
    }

    return;
}


async function systemMessageHandler(sessionName, config, patronChannelId, metadata, message, skipUpdate = false) {
    /**
     * Handler for plaintext messages sent by system.
     * 
     * @sessionName {object} The name of the session.
     * @config {object} Formatted object with configuration of the channel.
     * @patronChannelId {string} The unique identifier of customer from the channel the contact was initiated from.
     * @metadata {object} Formatted object of the metadata of the active session.
     * @message {object} Formatted object containing the details of the message.
     * @return {null}
     */

    const loggerBase = {
        'operation': 'SYSTEM_MESSAGE_HANDLER',
        'sessionName': sessionName
    };

    logger.debug({ 
        'config': config, 
        'metadata': metadata,
        'patronChannelId': patronChannelId,
        'message': message,
        ...loggerBase
    });

    const messageData = {
        'QueueUrl': chatMessageQueueUrl,
        'MessageGroupId': sessionName,
        'MessageBody': JSON.stringify({
            'direction': 'toChannel',
            'config': config,
            'message': {
                'Patron': {
                    'ChannelId': patronChannelId
                },
                'MessageId': message.Id,
                'ContentType': 'text/plain',
                'Content': message.Content,
                'Metadata': metadata,
                'SkipUpdate': skipUpdate
            }
        })
    };

    logger.debug({ 
        'messageData': messageData, 
        ...loggerBase
    });

    await sqs.send(new SendMessageCommand(messageData));
}



async function systemAttachmentHandler(sessionName, config, connectionToken, patronChannelId, metadata, message, skipUpdate = false) {
    /**
     * Handler for attachments sent by system.
     * 
     * @sessionName {object} The name of the session.
     * @config {object} Formatted object with configuration of the channel.
     * @connectionToken {string} The connection token of the active session.
     * @patronChannelId {string} The unique identifier of customer from the channel the contact was initiated from.
     * @metadata {object} Formatted object of the metadata of the active session.
     * @message {object} Formatted object containing the details of the message.
     * @return {null}
     */

     const loggerBase = {
        'operation': 'SYSTEM_ATTACHMENT_HANDLER',
        'sessionName': sessionName
    };

    logger.debug({
        'config': config, 
        'metadata': metadata,
        'patronChannelId': patronChannelId,
        'message': message,
        ...loggerBase
    });


    let attachmentData, bucket, key, contentLength, messageData, params;
    for (let i = 0; i < message.Attachments.length; i++) {
        attachmentData = await connect.getAttachmentUrl(message.Attachments[i].AttachmentId, connectionToken);

        // s3 = new AWS.S3({ 'apiVersion': '2006-03-01'});
        bucket = attachmentData.Url.substring(attachmentData.Url.indexOf('https://') + 8, attachmentData.Url.indexOf('.'));
        key = decodeURIComponent(attachmentData.Url.replace('https://', '').substring(attachmentData.Url.replace('https://', '').indexOf('/') + 1, attachmentData.Url.replace('https://', '').indexOf('?')));
        params = { 'Bucket': bucket, 'Key': key };
        contentLength = (await s3.send(new HeadObjectCommand(params))).ContentLength;

        messageData = {
            'QueueUrl': chatMessageQueueUrl,
            'MessageGroupId': sessionName,
            'MessageBody': JSON.stringify({
                'direction': 'toChannel',
                'config': config,
                'message': {
                    'Patron': {
                        'ChannelId': patronChannelId
                    },
                    'MessageId': message.Attachments[i].AttachmentId,
                    'ContentType': 'attachment',
                    'Content': {
                        'Filename': message.Attachments[i].AttachmentName,
                        'Type': message.Attachments[i].ContentType,
                        'Length': contentLength,
                        'Url': attachmentData.Url,
                        'UrlExpiry': attachmentData.UrlExpiry
                    },
                    'Metadata': metadata,
                    'SkipUpdate': skipUpdate
                }
            })
        };

        logger.debug({ 
            'messageData': messageData, 
            ...loggerBase
        });

        await sqs.send(new SendMessageCommand(messageData));
    }
}



async function agentMessageHandler(sessionName, config, patronChannelId, metadata, message, skipUpdate = false) {
    /**
     * Handler for plaintext messages sent by agents.
     * 
     * @sessionName {object} The name of the session.
     * @config {object} Formatted object with configuration of the channel.
     * @patronChannelId {string} The unique identifier of customer from the channel the contact was initiated from.
     * @metadata {object} Formatted object of the metadata of the active session.
     * @message {object} Formatted object containing the details of the message.
     * @return {null}
     */

    const loggerBase = {
        'operation': 'AGENT_MESSAGE_HANDLER',
        'sessionName': sessionName
    };

    logger.debug({ 
        'config': config, 
        'metadata': metadata,
        'patronChannelId': patronChannelId,
        'message': message,
        ...loggerBase
    });

    const messageData = {
        'QueueUrl': chatMessageQueueUrl,
        'MessageGroupId': sessionName,
        'MessageBody': JSON.stringify({
            'direction': 'toChannel',
            'config': config,
            'message': {
                'Patron': {
                    'ChannelId': patronChannelId
                },
                'MessageId': message.Id,
                'ContentType': 'text/plain',
                'Content': message.Content,
                'Metadata': metadata,
                'SkipUpdate': skipUpdate
            }        
        })
    };

    logger.debug({ 
        'messageData': messageData, 
        ...loggerBase 
    });

    await sqs.send(new SendMessageCommand(messageData));
}



async function agentAttachmentHandler(sessionName, config, connectionToken, patronChannelId, metadata, message, skipUpdate = false) {
    /**
     * Handler for attachments sent by agents.
     * 
     * @sessionName {object} The name of the session.
     * @config {object} Formatted object with configuration of the channel.
     * @connectionToken {string} The connection token of the active session.
     * @patronChannelId {string} The unique identifier of customer from the channel the contact was initiated from.
     * @metadata {object} Formatted object of the metadata of the active session.
     * @message {object} Formatted object containing the details of the message.
     * @return {null}
     */

    const loggerBase = {
        'operation': 'AGENT_ATTACHMENT_HANDLER',
        'sessionName': sessionName
    };

    logger.debug({ 
        'config': config, 
        'metadata': metadata,
        'patronChannelId': patronChannelId,
        'message': message,
        ...loggerBase
    });


    let attachmentData, bucket, key, contentLength, messageData, params;
    for (let i = 0; i < message.Attachments.length; i++) {
        attachmentData = await connect.getAttachmentUrl(message.Attachments[i].AttachmentId, connectionToken);

        // s3 = new AWS.S3({ 'apiVersion': '2006-03-01'});
        bucket = attachmentData.Url.substring(attachmentData.Url.indexOf('https://') + 8, attachmentData.Url.indexOf('.'));
        key = decodeURIComponent(attachmentData.Url.replace('https://', '').substring(attachmentData.Url.replace('https://', '').indexOf('/') + 1, attachmentData.Url.replace('https://', '').indexOf('?')));
        params = { 'Bucket': bucket, 'Key': key };
        contentLength = (await s3.send(new HeadObjectCommand(params))).ContentLength;

        messageData = {
            'QueueUrl': chatMessageQueueUrl,
            'MessageGroupId': sessionName,
            'MessageBody': JSON.stringify({
                'direction': 'toChannel',
                'config': config,
                'message': {
                    'Patron': {
                        'ChannelId': patronChannelId
                    },
                    'MessageId': message.Attachments[i].AttachmentId,
                    'ContentType': 'attachment',
                    'Content': {
                        'Filename': message.Attachments[i].AttachmentName,
                        'Type': message.Attachments[i].ContentType,
                        'Length': contentLength,
                        'Url': attachmentData.Url,
                        'UrlExpiry': attachmentData.UrlExpiry
                    },
                    'Metadata': metadata,
                    'SkipUpdate': skipUpdate
                }
            })
        };

        logger.debug({ 
            'messageData': messageData, 
            ...loggerBase
        });

        await sqs.send(new SendMessageCommand(messageData));
    }
}



async function customerMessageHandler(sessionName, config, patronChannelId, metadata, message) {
    /**
     * Handler for attachments sent by customers. Placeholder, currently not required.
     * 
     * @sessionName {object} The name of the session.
     * @config {object} Formatted object with configuration of the channel.
     * @patronChannelId {string} The unique identifier of customer from the channel the contact was initiated from.
     * @metadata {object} Formatted object of the metadata of the active session.
     * @message {object} Formatted object containing the details of the message.
     * @return {null}
     */

    return;
}



async function customerAttachmentHandler(sessionName, config, connectionToken, patronChannelId, metadata, message) {
    /**
     * Handler for attachments sent by customers. Placeholder, currently not required.
     * 
     * @sessionName {object} The name of the session.
     * @config {object} Formatted object with configuration of the channel.
     * @connectionToken {string} The connection token of the active session.
     * @patronChannelId {string} The unique identifier of customer from the channel the contact was initiated from.
     * @metadata {object} Formatted object of the metadata of the active session.
     * @message {object} Formatted object containing the details of the message.
     * @return {null}
     */

    return;
}