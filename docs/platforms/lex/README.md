# AMAZON LEX AS THE PLATFORM

To integrate Amazon Lex as the bot platform with the CX Bridge, a CloudFormation stack need to be deployed that will include the required permissions. If there are more than one Amazon Lex bots to be integrated, deploy multiple instances of this account. If the Amazon Lex bot is built into Amazon Connect contact flows, this document is not relevant, refer to the document [Amazon Connect as the Platform](../connect/README.md) instead.


## Obtaining the Amazon Lex Bot and Alias IDs

1. From the Amazon Lex service page, select the bot to obtain the bot id.
![Get Amazon Lex bot id](imgs/01-get-bot-id.png)

2. From the same bot page, select ```Aliases``` to find the alias id and locales.
![Get Amazon Lex bot alias id](imgs/02-get-bot-alias-id-and-locale.png)


## Deploy the CloudFormation Stack
1. Review the parameters required for deployment in the table below. Some of these would have been obtained from the CX Bridge stack deployed earlier.

| Parameter Name | Required? | Description |
|---|---|---|
| BotId | Yes | The id of the Amazon Lex bot. |
| BotAliasId | Yes |The alias id of the Amazon Lex bot. |
| SecretsPrefix | Yes | The Secrets Manager prefix / path that stores the credentials / secrets for the channels. Exclude any initial or trailing slashes (i.e. '/'). This template will grant permission to everything under this prefix and as such, it is suggested that you create the secrets for each of the channels to be integrated under a common path. Secret creation will be covered in the channel configuration. |
| BridgeRoleName | Yes | The IAM role ARN name for the CX Bridge. |

2. Once done, identify the region to be used and click on the link to launch the CloudFormation service page. Regions listed below are for those that support Amazon Connect.

| Region | Launch Link |
|---|---|
| US East (N. Virginia) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://us-east-1.console.aws.amazon.com/cloudformation/home?region=us-east-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/lex-bot-to-bridge.yaml&stackName=stackName=AmzLexToCXBridgeIntegration) |
| US West (Oregon) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://us-west-2.console.aws.amazon.com/cloudformation/home?region=us-west-2#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/lex-bot-to-bridge.yaml&stackName=stackName=AmzLexToCXBridgeIntegration) |
| Asia Pacific (Seoul) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-northeast-2.console.aws.amazon.com/cloudformation/home?region=ap-northeast-2#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/lex-bot-to-bridge.yaml&stackName=stackName=AmzLexToCXBridgeIntegration) |
| Asia Pacific (Singapore) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-southeast-1.console.aws.amazon.com/cloudformation/home?region=ap-southeast-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/lex-bot-to-bridge.yaml&stackName=stackName=AmzLexToCXBridgeIntegration) |
| Asia Pacific (Sydney) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-southeast-2.console.aws.amazon.com/cloudformation/home?region=ap-southeast-2#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/lex-bot-to-bridge.yaml&stackName=stackName=AmzLexToCXBridgeIntegration) |
| Asia Pacific (Tokyo) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-northeast-1.console.aws.amazon.com/cloudformation/home?region=ap-northeast-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/lex-bot-to-bridge.yaml&stackName=stackName=AmzLexToCXBridgeIntegration) |
| Canada (Central) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ca-central-1.console.aws.amazon.com/cloudformation/home?region=ca-central-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/lex-bot-to-bridge.yaml&stackName=stackName=AmzLexToCXBridgeIntegration) |
| Europe (Frankfurt) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://eu-central-1.console.aws.amazon.com/cloudformation/home?region=eu-central-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/lex-bot-to-bridge.yaml&stackName=stackName=AmzLexToCXBridgeIntegration) |
| Europe (London) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://eu-west-2.console.aws.amazon.com/cloudformation/home?region=eu-west-2#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/lex-bot-to-bridge.yaml&stackName=stackName=AmzLexToCXBridgeIntegration) |


## Create a DynamoDB Configuration Item

For each Amazon Lex bot that is to be integrated with the CX Bridge, a configuration item needs to be created in the DynamoDB Configuration table that was deployed as part of the CX Bridge stack.

The schema for these items are listed below:

| Key | Type | Description |
|---|---|---|
| PKey | String | This will be in the format of ```lex::{platformname}```. |
| SKey | String | This will be in the format of ```lex::{platformname}```. |
| FriendlyName | String | The name of the Amazon Lex bot. This is just a name and does not need to match the actual name of the bot in Amazon Lex but should be unique. It must the the same as the ```{platformname}``` in the ```PKey``` and ```SKey```. |
| Category | String | This must be set as ```platform```. |
| SubCategory | String | This must be set as ```lex```. |
| BotId | String | The id of the Amazon Lex bot (e.g. RVLZVDH96L). |
| BotAliasId | String | The alias of the Amazon Lex bot (e.g. BGWOQMHSSI). |
| LocaleId | String | The locale of the Amazon Lex bot (e.g. en_US). Refer to [this link](https://docs.aws.amazon.com/lexv2/latest/dg/how-languages.html) for locale codes. If your bot supports multiple different languages, you need to create one item for each language. |
| Uid | String | A unique string to identify the channel. Only use alphanumeric string. |

### Examples

To create a configuration item for an Amazon Lex bot with two locales.

```
{
    "PKey": "lex::my-bot-us",
    "SKey": "lex::my-bot-us",
    "FriendlyName": "my-bot-us",
    "Category": "platform",
    "SubCategory": "lex",
    "BotId": "RVLZVDH96L",
    "BotAliasId": "BGWOQMHSSI",
    "LocaleId": "en_US",
    "Uid": "fTDAukqsgvLGWsP6sKq7Wn2t9jdb8s6SVcSvP7CYeSLnSSrUEakN9QejGfCkcnp8nnQRXFa7jZhvMRxs6PYh696EWxgcJYFdx8Qdfk8Fwa5kfH3d8Azge65gtL2k8nsu"
}
```

```
{
    "PKey": "lex::my-bot-au",
    "SKey": "lex::my-bot-au",
    "FriendlyName": "my-bot-au",
    "Category": "platform",
    "SubCategory": "lex",
    "BotId": "RVLZVDH96L",
    "BotAliasId": "BGWOQMHSSI",
    "LocaleId": "en_AU",
    "Uid": "fTDAukqsgvLGWsP6sKq7Wn2t9jdb8s6SVcSvP7CYeSLnSSrUEakN9QejGfCkcnp8nnQRXFa7jZhvMRxs6PYh696EWxgcJYFdx8Qdfk8Fwa5kfH3d8Azge65gtL2k8nsu"
}
```



## Sending Quick Replies

To send quick replies, which are essentially buttons in messages with predefined values, use a JSON formatted Message Object from the Amazon Connect contact flows.

### Message Object

| Key | Type | Description |
|---|---|---|
| txt | String | This is the main message. |
| btns | Object | This is an array of button objects. |

### Button Object

| Key | Type | Description |
|---|---|---|
| text | String | This is the display message of the button. |
| value | String | This is the actual value that will be sent back to the CX Bridge. |

### Examples

```
{
    "txt": "Would you like to participate in a short survey?",
    "btns: [
        {
            "text": "Yes",
            "value": "yes"
        }
    ]
}
```

```
{
    "txt": "How would you rate your overall experience?",
    "btns: [
        {
            "text": "Very satisfied",
            "value": "5"
        },
        {
            "text": "Satisfied",
            "value": "4"
        },
        {
            "text": "Neither satisfied nor dissatisfied",
            "value": "3"
        },
        {
            "text": "Dissatisfied",
            "value": "2"
        },
        {
            "text": "Very dissatisfied",
            "value": "1"
        }
    ]
}
```