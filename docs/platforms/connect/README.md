# AMAZON CONNECT AS THE PLATFORM

To integrate Amazon Connect as the contact center / bot platform with the CX Bridge, a CloudFormation stack need to be deployed that will include the required permissions and SNS topic required for the integration. If there are more than one Amazon Connect instance to be integrated, deploy multiple instances of this account.


## Obtaining the Amazon Connect Instance Information
1. From the Amazon Connect service page, select the Amazon Connect instance and obtain the ARN.
![Get Amazon Connect ARN](imgs/01-get-amazon-connect-arn.png)

2. From the S3 service page, select bucket used by the Amazon Connect instace and obtain the ARN.
![Get AWS S3 ARN ](imgs/02-get-s3-arn.png)


## Deploy the CloudFormation Stack
1. Review the parameters required for deployment in the table below. Some of these would have been obtained from the CX Bridge stack deployed earlier.

| Parameter Name | Required? | Description |
|---|---|---|
| ConnectInstanceArn | Yes | The ARN of the Amazon Connect instance. |
| ConnectBucketArn | Yes | The ARN of the S3 used by the Amazon Connect instance. |
| SecretsPrefix | Yes | The Secrets Manager prefix / path that stores the credentials / secrets for the channels. Exclude any initial or trailing slashes (i.e. '/'). This template will grant permission to everything under this prefix and as such, it is suggested that you create the secrets for each of the channels to be integrated under a common path. Secret creation will be covered in the channel configuration. |
| BridgeRoleName | Yes | The IAM role ARN name for the CX Bridge.  |
| ConnectChatStreamProcessorArn | Yes | The ARN of the Lambda used to process Amazon Connect chat events. |

2. Once done, identify the region to be used and click on the link to launch the CloudFormation service page. Regions listed below are for those that support Amazon Connect.

| Region | Launch Link |
|---|---|
| US East (N. Virginia) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://us-east-1.console.aws.amazon.com/cloudformation/home?region=us-east-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/connect-instance-to-bridge.yaml&stackName=AmzConnectToCXBridgeIntegration) |
| US West (Oregon) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://us-west-2.console.aws.amazon.com/cloudformation/home?region=us-west-2#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/connect-instance-to-bridge.yaml&stackName=AmzConnectToCXBridgeIntegration) |
| Asia Pacific (Seoul) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-northeast-2.console.aws.amazon.com/cloudformation/home?region=ap-northeast-2#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/connect-instance-to-bridge.yaml&stackName=AmzConnectToCXBridgeIntegration) |
| Asia Pacific (Singapore) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-southeast-1.console.aws.amazon.com/cloudformation/home?region=ap-southeast-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/connect-instance-to-bridge.yaml&stackName=AmzConnectToCXBridgeIntegration) |
| Asia Pacific (Sydney) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-southeast-2.console.aws.amazon.com/cloudformation/home?region=ap-southeast-2#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/connect-instance-to-bridge.yaml&stackName=AmzConnectToCXBridgeIntegration) |
| Asia Pacific (Tokyo) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-northeast-1.console.aws.amazon.com/cloudformation/home?region=ap-northeast-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/connect-instance-to-bridge.yaml&stackName=AmzConnectToCXBridgeIntegration) |
| Canada (Central) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ca-central-1.console.aws.amazon.com/cloudformation/home?region=ca-central-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/connect-instance-to-bridge.yaml&stackName=AmzConnectToCXBridgeIntegration) |
| Europe (Frankfurt) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://eu-central-1.console.aws.amazon.com/cloudformation/home?region=eu-central-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/connect-instance-to-bridge.yaml&stackName=AmzConnectToCXBridgeIntegration) |
| Europe (London) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://eu-west-2.console.aws.amazon.com/cloudformation/home?region=eu-west-2#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/connect-instance-to-bridge.yaml&stackName=AmzConnectToCXBridgeIntegration) |



## Create a DynamoDB Configuration Item

For each Amazon Connect instance that is to be integrated with the CX Bridge, a configuration item needs to be created in the DynamoDB Configuration table that was deployed as part of the CX Bridge stack.

The schema for these items are listed below:

| Key | Type | Description |
|---|---|---|
| PKey | String | This will be in the format of ```connect::{platformname}```. |
| SKey | String | This will be in the format of ```connect::{platformname}```. |
| FriendlyName | String | The alias of the Amazon Connect instance. It must the the same as the ```{platformname}``` in the ```PKey``` and ```SKey```. |
| Category | String | This must be set as ```platform```. |
| SubCategory | String | This must be set as ```connect```. |
| InstanceId | String | The unique id of the Amazon Connect instance. This is the last part of the ARN (e.g. dc669442-c20d-41d0-9ccb-5353b6001872). |
| Uid | String | A unique string to identify the channel. Only use alphanumeric string. |
| SnsStreamArn | String | The ARN of the SNS topic used to stream Amazon Connect chat events. |

### Examples

```
{
    "PKey": "connect::my-amazon-connect",
    "SKey": "connect::my-amazon-connect",
    "FriendlyName": "my-amazon-connect",
    "Category": "platform",
    "SubCategory": "connect",
    "InstanceId": "dc669442-c20d-41d0-9ccb-5353b6001872",
    "Uid": "fTDAukqsgvLGWsP6sKq7Wn2t9jdb8s6SVcSvP7CYeSLnSSrUEakN9QejGfCkcnp8nnQRXFa7jZhvMRxs6PYh696EWxgcJYFdx8Qdfk8Fwa5kfH3d8Azge65gtL2k8nsu",
    "SnsStreamArn": "arn:aws:sns:ap-southeast-2:0123456789012:chat-sns-topic"
}
```


## Sending Quick Replies

To send quick replies, which are essentially buttons in messages with predefined values, use a JSON formatted Message Object from the Amazon Connect contact flows.

### Message Object

| Key | Type | Description |
|---|---|---|
| txt | String | This is the main message. |
| btns | Object | This is an array of button objects. |

### Button Object

| Key | Type | Description |
|---|---|---|
| text | String | This is the display message of the button. |
| value | String | This is the actual value that will be sent back to the CX Bridge. |

### Examples

```
{
    "txt": "Would you like to participate in a short survey?",
    "btns: [
        {
            "text": "Yes",
            "value": "yes"
        }
    ]
}
```

```
{
    "txt": "How would you rate your overall experience?",
    "btns: [
        {
            "text": "Very satisfied",
            "value": "5"
        },
        {
            "text": "Satisfied",
            "value": "4"
        },
        {
            "text": "Neither satisfied nor dissatisfied",
            "value": "3"
        },
        {
            "text": "Dissatisfied",
            "value": "2"
        },
        {
            "text": "Very dissatisfied",
            "value": "1"
        }
    ]
}
```