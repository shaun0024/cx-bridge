# CX BRIDGE

## Table of Contents

1. [Deployment Guide](01-deployment.md)

2. Connecting to a Platform

    - [Amazon Connect as the Platform](platforms/connect/README.md)
    - [Amazon Lex as the Platform](platforms/lex/README.md)


2. Integrating with a Channel

    - [Facebook Messenger as a Channel](channels/fbmessenger/README.md)
    - [Microsoft Teams as a Channel](channels/msteams/README.md)
    - [Slack as a Channel](channels/slack/README.md)