# SALESFORCE REST API INTEGRATION

## CREATE PROFILE
1. Under ```Salesforce Setup```, navigate to ```Users -> Profiles``` and click on a ```New Profile```. ![Create new profile](imgs/Profile-01.png)

2. Select ```System Administrator``` under the drop down list for ```Existing Profile``` and provide the profile with a name. ![Create new profile from existing profile](imgs/Profile-02.png)

3. Once the profile has been created, select the profile and click on ```Edit```. ![Edit newly created profile](imgs/Profile-03.png)

4. Search for the ```Lightning Experience User``` permission and uncheck it. ![Remove permission](imgs/Profile-04.png)

5. User ```Password Policies```, set ```User passwords expire in``` to ```Never expires```. ![Set password to never expire](imgs/Profile-05.png)



## CREATE CONNECTED APP
1. Under ```Salesforce Setup```, navigate to ```Apps -> App Manager``` and click on a ```New Connected App```. ![Create new connected app](imgs/ConnectedApp-01.png)

2. Provide the app with a name and email. Under ```Selected OAuth Scopes```, add in ```Access the identity URL service (id, profile, email, address, phone)``` and ```Manage user data via APIs (api)```. Save the changes. ![Select OAuth scopes for connected app](imgs/ConnectedApp-02.png)

3. Once the app has been created, make a note of the ```Consumer Key``` and ```Consumer Secret``` before clicking on ```Manage```. ![Copy keys and manage connected app](imgs/ConnectedApp-03.png)

4. Click on ```Edit Policies```. ![Edit connected app policy](imgs/ConnectedApp-04.png)

5. Under the ```Permitted Users``` drop down list, select ```Admin approved users are pre-authorized```. Under the ```IP Relaxation``` drop down list, select ```Relax IP restrictions```. Save the changes. ![Edit OAuth policy permissions](imgs/ConnectedApp-05.png)

6. Click on ```Manage Profiles```. ![Manage connected app profiles](imgs/ConnectedApp-06.png)

7. Select the profile created earlier. Save the changes. ![Add profile to connected app](imgs/ConnectedApp-07.png)


## CREATE API USER
1. Under ```Salesforce Setup```, navigate to ```Users -> Users``` and click on a ```New User```. ![Create new user](imgs/ApiUser-01.png)

2. Provide details for the user. Under the ```User License``` drop down list, select ```Salesforce```. Under the ```Profile``` drop down list, select the profile created earlier. Save the changes ![Create new user](imgs/ApiUser-02.png)

3. Once the user has been created, an e-mail will be sent to the address used. Click on the ```Verify Account``` link to verify the account and set a password. ![Verify new user account](imgs/ApiUser-03.png)

4. Once the account verification process has completed, select ```My Settings``` towards the drop down list at the top right hand corner. ![Access user account settings](imgs/ApiUser-04.png)

5. Navigate to ```Personal -> Reset My Security Token```. Click on ```Reset Security Token```. The new security token will be sent to the e-mail address used to create the account. Make a note of this new security token. ![Reset security token](imgs/ApiUser-05.png)


## CREATE SECRET
1. In the ```AWS Management Console```, navigate to the ```Secrets Manager``` service and create a new secret. Select ```Other type of secret``` as the ```Secret type```. Under ```Key/value pairs```, create the keys as in the screenshot and replace the values accordingly with obtained in the earlier steps. ![Create new secret](imgs/SecretsManager-01.png)

2. Once the secret has been created, take note of the ```Secret ARN```. ![Get the secret ARN](imgs/SecretsManager-02.png)