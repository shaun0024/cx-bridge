# Facebook Messenger as a Channel

## Create a Facebook App

1. Access the [Facebook Developers](https://developers.facebook.com) page and create a new app. You may need to resgister yourself as a developer to gain access to this.
![Create a new Facebook app](imgs/01-create-new-app.png)

2. Select the option to create an app ```Business``` app. If you choose other options, you may need to make changes to the default permission sets it assigns.
![Create a business app](imgs/02-create-business-app.png)

3. Give the app an name and select a workspace to place the app in.
![Provide app a name and e-mail address](imgs/03-provide-basic-details.png)

4. Once the app has been created, select ```Settings -> Basic``` from the navigation pane and take note of the app id and secret.
![Get app id and secret](imgs/04-get-app-id-secret.png)

5. Select ```Add Product``` from the navigation pane and add ```Messenger```.
![Add Messenger as a product](imgs/05-select-product-type.png)

6. Assign a Facebook page that the app will be associated with and authorize it. Take note of the id of the page.
![Add a page to the app](imgs/06-add-page.png)
![Authorize the page](imgs/07-authorize-page.png)

7. Once the page has been added, generate an access token. Take note of this token.
![Generate access token for page](imgs/08-generate-access-token-and-get-page-id.png)

8. Create a secret in AWS Secrets Manager with the following keys based on the credentials collected in step 4, 6 and 7. You will also need to manually generate a verification token, which is a random alphanumeric and case sensitive string. Take note of this verification token. Once created, take note of the ARN of the secret.
![Create secret](imgs/09-create-secret.png)
![Get secret ARN](imgs/10-get-secret-arn.png)

9. Create an item in the DynamoDB configuration table with the details of the secrets. The contents will depend on the platform that you are connecting the app to and create a subscription URL. Refer to [this section](#dynamodb-configuration-item-format).

10. With the subscription URL, add a callback url together with the verification token.
![Add callback URL](imgs/11-add-callback-url.png)
![Enter subscription URL and verification token](imgs/12-provide-webhook-details.png)

11. Once the subscription URL has been verified, add subscription events.
![Add subscription events](imgs/13-add-subscriptions-to-webhook.png)
![Add messaging events](imgs/14-add-messages-subscription.png)

**IMPORTANT NOTE:** Until your app is submitted to Facebook for approval, your app may be limited to only users who are added into the Facebook app roles explicitly. Users who are not added may not trigger any events.



## Create a DynamoDB Configuration Item

For each Facebook Messenger app that is to be integrated with the CX Bridge, a configuration item needs to be created in the DynamoDB Configuration table that was deployed as part of the CX Bridge stack.

The schema for these items are listed below:

| Key | Description |
|---|---|
| PKey | The platform that Facebook Messenger will connect to. This will be in the format of ```{platform}::{platformname}``` (e.g. connect::my-connect-instance) and must have a corresponding platform item already created. |
| SKey | The platform and name of the Facebook Messenger channel. This will be in the format of ```{platform}::{platformname}::channel::fbmessenger::{channelname}``` (e.g. connect::my-connect-instance) and must have a corresponding platform item already created. |
| FriendlyName | This is just a name used to identify the Facebook Messenger channel. It must the the same as the ```{channelname}``` in the ```SKey```. |
| Category | This must be set as ```channel```. |
| SubCategory | This must be set as ```fbmessenger```. |
| SecretArn | The ARN of the secret created earlier that contains the credentials for the app. |
| Uid | A unique string to identify the channel. Only use alphanumeric string. |
| Timeout | The amount of time that the session is valid for. This is in minutes. |
| UploadAttachments | Set this to ```true``` to upload the files sent to Facebook. Set this to ```false``` to send the link of the attachment to Facebook. Generally used for Amazon Connect only. Custom logic required to be added for Amazon Lex. |
| AttachmentMessage | A message used when a link to an attachment is sent. |
| IntegrationDirectory | For future use. Set this to ```null`` currently. |
| RespondProactively | For future use. Set this to ```false``` currently. |


### Platform Specific Keys

#### Amazon Connect

| Key | Description |
|---|---|
| ContactFlowId | The id of the Amazon Connect contact flow to use to initiate the session. |


### Subscription URL Format
The subscription URL will be in the following format:

```https://{aws-api-gateway-execution-endpoint}/{aws-api-gateway-stage-name}/{platform}/{platform-name}/channel/fbmessenger/channelId/{channel-name}?channel_uid={channel-uid}```


### Examples

#### Facebook Messenger Integration with Amazon Lex

To integrate Facebook Messenger with an Amazon Lex bot with the name ```my-lex-bot```, a platform configuration item similar to below would have to be created first ([refer to this link](../../platforms/lex/README.md)).

```
{
    "PKey": "lex::my-lex-bot",
    "SKey": "lex::my-lex-bot",
    "FriendlyName": "my-lex-bot",
    "Category": "platform",
    "SubCategory": "lex",
    "BotId": "RVLZVDH96L",
    "BotAliasId": "BGWOQMHSSI",
    "LocaleId": "en_US",
    "Uid": "fTDAukqsgvLGWsP6sKq7Wn2t9jdb8s6SVcSvP7CYeSLnSSrUEakN9QejGfCkcnp8nnQRXFa7jZhvMRxs6PYh696EWxgcJYFdx8Qdfk8Fwa5kfH3d8Azge65gtL2k8nsu"
}
```

Once the platform configuration item has been created, create a channel configuration item similar to the below. 

```
{
    "PKey": "lex::my-lex-bot",
    "SKey": "lex::my-lex-bot::channel::fbmessenger::my-fbmessenger-app",
    "FriendlyName": "my-fbmessenger-app",
    "Category": "channel",
    "SubCategory": "fbmessenger",
    "SecretArn": "arn:aws:secretsmanager:ap-southeast-2:0123456789012:secret:fbmessenger-app-s55QQ4",
    "Uid": "IHk52bThUa6qtUetJsK2kjQkr9NgR8KhebvGvjkN7BYZGmMdG6TX2mbAZ7TmrcHPgrFyqpf3wS6q475jwhNQNuqRd9sE6cDJrWPvvG3Z8UF5M9j9JUJJPe8HyF03Vbhf",
    "Timeout": 10,
    "AttachmentMessage": "You can download the attachment from the link below.",
    "IntegrationDirectory": null,
    "RespondProactively": false,
    "UploadAttachments": false
}
```

Based on the example above, the subscription URL will be:

```
https://9xih7nchid.execute-api.ap-southeast-2.amazonaws.com/v1/lex/my-lex-bot/channel/fbmessenger/channelId/my-fbmessenger-app?channel_uid=IHk52bThUa6qtUetJsK2kjQkr9NgR8KhebvGvjkN7BYZGmMdG6TX2mbAZ7TmrcHPgrFyqpf3wS6q475jwhNQNuqRd9sE6cDJrWPvvG3Z8UF5M9j9JUJJPe8HyF03Vbhf
```


#### Facebok Messenger Integration with Amazon Connect

To integrate Facebook Messenger with an Amazon Connect instance with the alias ```my-connect-instance```, a platform configuration item similar to below would have to be created first ([refer to this link](../../platforms/connect/README.md)).

```
{
    "PKey": "connect::my-connect-instance",
    "SKey": "connect::my-connect-instance",
    "FriendlyName": "my-connect-instance",
    "Category": "platform",
    "SubCategory": "connect",
    "InstanceId": "dc669442-c20d-41d0-9ccb-5353b6001872",
    "Uid": "fTDAukqsgvLGWsP6sKq7Wn2t9jdb8s6SVcSvP7CYeSLnSSrUEakN9QejGfCkcnp8nnQRXFa7jZhvMRxs6PYh696EWxgcJYFdx8Qdfk8Fwa5kfH3d8Azge65gtL2k8nsu",
    "SnsStreamArn": "arn:aws:sns:ap-southeast-2:0123456789012:chat-sns-topic"
}
```

Once the platform configuration item has been created, create a channel configuration item similar to the below. 

```
{
    "PKey": "connect::my-connect-instance",
    "SKey": "connect::my-connect-instance::channel::fbmessenger::my-fbmessenger-app",
    "FriendlyName": "my-fbmessenger-app",
    "Category": "channel",
    "SubCategory": "fbmessenger",
    "SecretArn": "arn:aws:secretsmanager:ap-southeast-2:0123456789012:secret:fbmessenger-app-s55QQ4",
    "Uid": "IHk52bThUa6qtUetJsK2kjQkr9NgR8KhebvGvjkN7BYZGmMdG6TX2mbAZ7TmrcHPgrFyqpf3wS6q475jwhNQNuqRd9sE6cDJrWPvvG3Z8UF5M9j9JUJJPe8HyF03Vbhf",
    "Timeout": 10,
    "AttachmentMessage": "You can download the attachment from the link below.",
    "ContactFlowId": "dce0db81-0e43-403e-bbgg-a6f9bdd03705"
    "IntegrationDirectory": null,
    "RespondProactively": false,
    "UploadAttachments": false
}
```
Based on the example above, the subscription URL will be:

```
https://9xih7nchid.execute-api.ap-southeast-2.amazonaws.com/v1/connect/my-lex-bot/channel/fbmessenger/channelId/my-fbmessenger-app?channel_uid=IHk52bThUa6qtUetJsK2kjQkr9NgR8KhebvGvjkN7BYZGmMdG6TX2mbAZ7TmrcHPgrFyqpf3wS6q475jwhNQNuqRd9sE6cDJrWPvvG3Z8UF5M9j9JUJJPe8HyF03Vbhf
```