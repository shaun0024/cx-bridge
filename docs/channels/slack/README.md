# Slack as a Channel

## Create a Slack App

1. Access the [Slack API](https://api.slack.com) and create a new app.
![Create a new Slack App](imgs/01-create-new-slack-app.png)

2. Select the option to create an app ```From scratch```.
![Create app from scratch](imgs/02-create-from-scratch.png)

3. Give the app an name and select a workspace to place the app in.
![Provide app a name and select workspace to develop in](imgs/03-provide-app-name.png)

4. Once the app has been created, select ```Basic Information``` from the navigation pane and take note of the app credentials. This would include:
    - App ID
    - Client ID
    - Client Secret
    - Signing Secret
    - Verification Token
![Get app credentials](imgs/04-get-app-credentials.png)

5. Select ```OAuth & Permissions``` from the navigation pane and select the following scopes:
    - ```chat:write```
    - ```files:write```
    - ```im:history```
    - ```im:write```
    - ```reactions:write```
    - ```users.profile:read```
![Access OAuth and permissions](imgs/05-access-oauth-permissions.png)
![Select scopes](imgs/06-select-oauth-permissions.png)

6. Add the app to a workspace and obtain the token once it has been added.
![Install app to workspace](imgs/07-install-app-to-workspace.png)
![Add app to workspace](imgs/08-add-app-to-workspace.png)
![Get OAuth token](imgs/09-get-app-oauth-token.png)

7. Select ```App Home``` from the navigation pane and set the bot to be always online and to allow direct messages.
![Enable direct messaging](imgs/10-enable-direct-messaging.png)

8. Get the ids of the bot. The simplest way to obtain this is to go to [https://api.slack.com/method/auth.test/test] and use the token obtained from step 6. The results will be displayed on the screen. Take note of:
    - ```user_id```
    - ```bot_id```
![Get bot and user id](imgs/11-get-bot-id.png)

9. Create a secret in AWS Secrets Manager with the following keys based on the credentials collected in step 4, 6 and 8. Once created, take note of the ARN of the secret.
![Create secret](imgs/12-create-secret.png)
![Get secret ARN](imgs/15-get-secret-arn.png)

10. Create an item in the DynamoDB configuration table with the details of the secret. The contents will depend on the platform that you are connecting the app to and create a subscription URL. Refer to [this section](#dynamodb-configuration-item-format).

11. With the subscription URL, add it to subscription and interactivity events.
![Subscribe to events](imgs/16-subscribe-to-event.png)
![Enable interactivity](imgs/17-enable-interactivity-components.png)



## Create a Slack App Using a Manifest

Alternatively a Slack app can also be created using a manifest file. Below is a sample of a manifest.

```
display_information:
  name: My Slack App
features:
  bot_user:
    display_name: My Slack App
    always_online: true
oauth_config:
  scopes:
    bot:
      - chat:write
      - files:write
      - im:history
      - im:write
      - reactions:write
      - users.profile:read
settings:
  event_subscriptions:
    request_url: https://9xih7nchid.execute-api.ap-southeast-2.amazonaws.com/v1/connect/my-lex-bot/channel/slack/channelId/my-slack-app?channel_uid=IHk52bThUa6qtUetJsK2kjQkr9NgR8KhebvGvjkN7BYZGmMdG6TX2mbAZ7TmrcHPgrFyqpf3wS6q475jwhNQNuqRd9sE6cDJrWPvvG3Z8UF5M9j9JUJJPe8HyF03Vbhf
    bot_events:
      - message.im
  interactivity:
    is_enabled: true
    request_url: https://9xih7nchid.execute-api.ap-southeast-2.amazonaws.com/v1/connect/my-lex-bot/channel/slack/channelId/my-slack-app?channel_uid=IHk52bThUa6qtUetJsK2kjQkr9NgR8KhebvGvjkN7BYZGmMdG6TX2mbAZ7TmrcHPgrFyqpf3wS6q475jwhNQNuqRd9sE6cDJrWPvvG3Z8UF5M9j9JUJJPe8HyF03Vbhf
  org_deploy_enabled: false
  socket_mode_enabled: false
  token_rotation_enabled: false

```



## Create a DynamoDB Configuration Item

For each Slack app that is to be integrated with the CX Bridge, a configuration item needs to be created in the DynamoDB Configuration table that was deployed as part of the CX Bridge stack.

The schema for these items are listed below:

| Key | Type | Description |
|---|---|---|
| PKey | String | The platform that Slack will connect to. This will be in the format of ```{platform}::{platformname}``` (e.g. connect::my-connect-instance) and must have a corresponding platform item already created. |
| SKey | String | The platform and name of the Slack channel. This will be in the format of ```{platform}::{platformname}::channel::slack::{channelname}``` (e.g. connect::my-connect-instance) and must have a corresponding platform item already created. |
| FriendlyName | String | This is just a name used to identify the Slack channel. It must the the same as the ```{channelname}``` in the ```SKey```. |
| Category | String | This must be set as ```channel```. |
| SubCategory | String | This must be set as ```slack```. |
| SecretArn | String | The ARN of the secret created earlier that contains the credentials for the app. |
| Uid | String | A unique string to identify the channel. Only use alphanumeric string. |
| Timeout | String | The amount of time that the session is valid for. This is in minutes. |
| UploadAttachments | String | Set this to ```true``` to upload the files sent to Slack. Set this to ```false``` to send the link of the attachment to Slack. Generally used for Amazon Connect only. Custom logic required to be added for Amazon Lex. |
| AttachmentMessage | String | A message used when a link to an attachment is sent. |
| IntegrationDirectory | String | For future use. Set this to ```null`` currently. |
| RespondProactively | String | For future use. Set this to ```false``` currently. |


### Platform Specific Keys

#### Amazon Connect

| Key | Description |
|---|---|
| ContactFlowId | The id of the Amazon Connect contact flow to use to initiate the session. |


### Subscription URL Format
The subscription URL will be in the following format:

```https://{aws-api-gateway-execution-endpoint}/{aws-api-gateway-stage-name}/{platform}/{platform-name}/channel/slack/channelId/{channel-name}?channel_uid={channel-uid}```


### Examples

#### Slack Integration with Amazon Lex

To integrate Slack with an Amazon Lex bot with the name ```my-lex-bot```, a platform configuration item similar to below would have to be created first ([refer to this link](../../platforms/lex/README.md)).

```
{
    "PKey": "lex::my-lex-bot",
    "SKey": "lex::my-lex-bot",
    "FriendlyName": "my-lex-bot",
    "Category": "platform",
    "SubCategory": "lex",
    "BotId": "RVLZVDH96L",
    "BotAliasId": "BGWOQMHSSI",
    "LocaleId": "en_US",
    "Uid": "fTDAukqsgvLGWsP6sKq7Wn2t9jdb8s6SVcSvP7CYeSLnSSrUEakN9QejGfCkcnp8nnQRXFa7jZhvMRxs6PYh696EWxgcJYFdx8Qdfk8Fwa5kfH3d8Azge65gtL2k8nsu"
}
```

Once the platform configuration item has been created, create a channel configuration item similar to the below. 

```
{
    "PKey": "lex::my-lex-bot",
    "SKey": "lex::my-lex-bot::channel::slack::my-slack-app",
    "FriendlyName": "my-slack-app",
    "Category": "channel",
    "SubCategory": "slack",
    "SecretArn": "arn:aws:secretsmanager:ap-southeast-2:0123456789012:secret:slack-app-s55QQ4",
    "Uid": "IHk52bThUa6qtUetJsK2kjQkr9NgR8KhebvGvjkN7BYZGmMdG6TX2mbAZ7TmrcHPgrFyqpf3wS6q475jwhNQNuqRd9sE6cDJrWPvvG3Z8UF5M9j9JUJJPe8HyF03Vbhf",
    "Timeout": 10,
    "BotId": "B04QK9CKM5U",
    "UserId": "U04QWED4B76",
    "AttachmentMessage": "You can download the attachment from the link below.",
    "IntegrationDirectory": null,
    "RespondProactively": false,
    "UploadAttachments": false
}
```

Based on the example above, the subscription URL will be:

```
https://9xih7nchid.execute-api.ap-southeast-2.amazonaws.com/v1/lex/my-lex-bot/channel/slack/channelId/my-slack-app?channel_uid=IHk52bThUa6qtUetJsK2kjQkr9NgR8KhebvGvjkN7BYZGmMdG6TX2mbAZ7TmrcHPgrFyqpf3wS6q475jwhNQNuqRd9sE6cDJrWPvvG3Z8UF5M9j9JUJJPe8HyF03Vbhf
```


#### Slack Integration with Amazon Connect

To integrate Slack with an Amazon Connect instance with the alias ```my-connect-instance```, a platform configuration item similar to below would have to be created first ([refer to this link](../../platforms/connect/README.md)).

```
{
    "PKey": "connect::my-connect-instance",
    "SKey": "connect::my-connect-instance",
    "FriendlyName": "my-connect-instance",
    "Category": "platform",
    "SubCategory": "connect",
    "InstanceId": "dc669442-c20d-41d0-9ccb-5353b6001872",
    "Uid": "fTDAukqsgvLGWsP6sKq7Wn2t9jdb8s6SVcSvP7CYeSLnSSrUEakN9QejGfCkcnp8nnQRXFa7jZhvMRxs6PYh696EWxgcJYFdx8Qdfk8Fwa5kfH3d8Azge65gtL2k8nsu",
    "SnsStreamArn": "arn:aws:sns:ap-southeast-2:0123456789012:chat-sns-topic"
}
```

Once the platform configuration item has been created, create a channel configuration item similar to the below. 

```
{
    "PKey": "connect::my-connect-instance",
    "SKey": "connect::my-connect-instance::channel::slack::my-slack-app",
    "FriendlyName": "my-slack-app",
    "Category": "channel",
    "SubCategory": "slack",
    "SecretArn": "arn:aws:secretsmanager:ap-southeast-2:0123456789012:secret:slack-app-s55QQ4",
    "Uid": "IHk52bThUa6qtUetJsK2kjQkr9NgR8KhebvGvjkN7BYZGmMdG6TX2mbAZ7TmrcHPgrFyqpf3wS6q475jwhNQNuqRd9sE6cDJrWPvvG3Z8UF5M9j9JUJJPe8HyF03Vbhf",
    "Timeout": 10,
    "BotId": "B04QK9CKM5U",
    "UserId": "U04QWED4B76",
    "AttachmentMessage": "You can download the attachment from the link below.",
    "ContactFlowId": "dce0db81-0e43-403e-bbgg-a6f9bdd03705"
    "IntegrationDirectory": null,
    "RespondProactively": false,
    "UploadAttachments": false
}
```
Based on the example above, the subscription URL will be:

```
https://9xih7nchid.execute-api.ap-southeast-2.amazonaws.com/v1/connect/my-lex-bot/channel/slack/channelId/my-slack-app?channel_uid=IHk52bThUa6qtUetJsK2kjQkr9NgR8KhebvGvjkN7BYZGmMdG6TX2mbAZ7TmrcHPgrFyqpf3wS6q475jwhNQNuqRd9sE6cDJrWPvvG3Z8UF5M9j9JUJJPe8HyF03Vbhf
```