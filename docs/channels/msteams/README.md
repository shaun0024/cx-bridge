# Microsoft Teams as a Channel

## Known Issues
- When enabling attachment uploads to Microsoft Teams, after accepting or declining the consent request, an error message may appear but the functionality is not impacted (i.e. file being uploaded to the OneDrive folder after accepting or a link to the file after declining).

## Create a Microsoft Teams App

1. Access the [Microsoft Azure Portal](https://portal.azure.com) and create a new ```Resource Group``` (or use an existing one).
![Create new Resource Group](imgs/01-create-resource-group.png)
![Select subscription, region and provide name to Resource Group](imgs/02-provide-name-to-resource-group.png)

2. Create a new bot in the ```Azure Bot Service```. Select the subscription, plan and resource group in which the service will be deployed into. Ensure that ```Multi tenant``` is selected for the type of app.
![Create new Azure Bot service](imgs/03-create-bot-service.png)
![Select the Azure Bot](imgs/04-create-azure-bot-1.png)
![Select the Azure Bot](imgs/05-create-azure-bot-2.png)
![Select subscription, region and provide name to Resource Group](imgs/06-select-resource-group-and-plan.png)

3. Once the bot has been created, obtain the ```Microsoft App ID``` and ```App Tenant ID```. Create a new client secret by clicking on ```Manage Password``` next to the ```Microsoft App ID```. Take note of the new secret created.
![Get Microsoft App ID](imgs/07-get-app-id.png)
![Create new client secret](imgs/08-create-app-secret.png)
![Obtain client secret](imgs/09-obtain-app-secret.png)

4. Add ```Microsoft Teams``` as a channel.
![Add Microsoft Teams as a channel](imgs/10-add-teams-as-a-channel.png)

5. Create a secret in AWS Secrets Manager with the following keys based on the credentials collected in steps 3 and 4. Once created, take note of the ARN of the secret.
![Create secret](imgs/11-create-secret.png)
![Get secret ARN](imgs/12-get-secret-arn.png)

6. Create an item in the DynamoDB configuration table with the details of the secret. The contents will depend on the platform that you are connecting the app to and create a subscription URL. Refer to [this section](#dynamodb-configuration-item-format).

7. With the subscription URL, go back to the bot configuration and add the subscription URL to the ```Messaging Endpoint```.
![Enable messaging endpoint](imgs/07-get-app-id.png)

8. Create a new app in ```Microsoft Teams``` by uploading the app package. Refer to [this section](#creating-a-microsoft-teams-app-package).

**IMPORTANT NOTE:** These steps may not be suitable for an organization wide deployment where the app will be deployed to all users.



## Creating a Microsoft Teams App Package

To create a Microsoft Teams app, a JSON formatted manifest file will need to be created. This file will need to be called ```manifest.json```.

Two icons is also required for the app, one 32x32 pixels and another 192x192 pixels.

Once these 3 items (the manifest file and two icons) have been created, place them all into the same folder and zip all three of them up to crete the app package.

For full details, refer to [this link](https://learn.microsoft.com/en-us/microsoftteams/platform/resources/schema/manifest-schema).

### Sample Manifest File

```
{
  "$schema": "https://developer.microsoft.com/json-schemas/teams/v1.12/MicrosoftTeams.schema.json",
  "manifestVersion": "1.12",
  "version": "0.0.1",
  "id": "af9a22b7-6587-42f7-892b-e125e99c392d",
  "developer": {
    "name": "My Company Pty Ltd",
    "websiteUrl": "https://dev.botframework.com",
    "privacyUrl": "https://privacy.microsoft.com/en-us/privacystatement",
    "termsOfUseUrl": "https://www.botframework.com/Content/Microsoft-Bot-Framework-Preview-Online-Services-Agreement.htm"
  },
  "name": {
    "short": "My Teams App",
    "full": "My Teams App"
  },
  "description": {
    "short": "Description for My Teams App",
    "full": "Description for My Teams App"
  },
  "icons": {
    "outline": "icon-32x32px.png",
    "color": "icon-192x192px.png"
  },
  "accentColor": "#037bff",
  "bots": [
    {
      "botId": "af9a22b7-6587-42f7-892b-e125e99c392d",
      "scopes": [
        "personal"
      ],
      "isNotificationOnly": false,
      "supportsCalling": false,
      "supportsVideo": false,
      "supportsFiles": true
    }
  ],
  "isFullScreen": false,
  "showLoadingIndicator": false,
  "defaultInstallScope": "personal"
}
```



## Create a DynamoDB Configuration Item

For each Microsoft Teams app that is to be integrated with the CX Bridge, a configuration item needs to be created in the DynamoDB Configuration table that was deployed as part of the CX Bridge stack.

The schema for these items are listed below:

| Key | Type | Description |
|---|---|---|
| PKey | String | The platform that Microsoft Teams will connect to. This will be in the format of ```{platform}::{platformname}``` (e.g. connect::my-connect-instance) and must have a corresponding platform item already created. |
| SKey | String | The platform and name of the Microsoft Teams channel. This will be in the format of ```{platform}::{platformname}::channel::msteams::{channelname}``` (e.g. connect::my-connect-instance) and must have a corresponding platform item already created. |
| FriendlyName | String | This is just a name used to identify the Microsoft Teams channel. It must the the same as the ```{channelname}``` in the ```SKey```. |
| Category | String | This must be set as ```channel```. |
| SubCategory | String | This must be set as ```msteams```. |
| SecretArn | String | The ARN of the secret created earlier that contains the credentials for the app. |
| Uid | String | A unique string to identify the channel. Only use alphanumeric string. |
| Timeout | String | The amount of time that the session is valid for. This is in minutes. |
| UploadAttachments | String | Set this to ```true``` to upload the files sent to Microsoft Teams. Set this to ```false``` to send the link of the attachment to Microsoft Teams. Generally used for Amazon Connect only. Custom logic required to be added for Amazon Lex. |
| AttachmentMessage | String | A message used when a link to an attachment is sent. Used when ```UploadAttachments``` is set to ```false```. Generally used for Amazon Connect only. Custom logic required to be added for Amazon Lex. |
| DeclineAttachmentMessage | String | A message used when the consent to upload the attachment is declined. Used when ```UploadAttachments``` is set to ```true```. Generally used for Amazon Connect only. Custom logic required to be added for Amazon Lex. |
| ExpiredAttachmentMessage | String | A message used when a link is expired. Used when ```UploadAttachments``` is set to ```true```. Generally used for Amazon Connect only. Custom logic required to be added for Amazon Lex. |
| IntegrationDirectory | String | For future use. Set this to ```null`` currently. |
| RespondProactively | String | For future use. Set this to ```false``` currently. |


### Platform Specific Keys

#### Amazon Connect

| Key | Description |
|---|---|
| ContactFlowId | The id of the Amazon Connect contact flow to use to initiate the session. |


### Subscription URL Format
The subscription URL will be in the following format:

```https://{aws-api-gateway-execution-endpoint}/{aws-api-gateway-stage-name}/{platform}/{platform-name}/channel/msteams/channelId/{channel-name}?channel_uid={channel-uid}```


### Examples

#### Microsoft Teams Integration with Amazon Lex

To integrate Microsoft Teams with an Amazon Lex bot with the name ```my-lex-bot```, a platform configuration item similar to below would have to be created first ([refer to this link](../../platforms/lex/README.md)).

```
{
    "PKey": "lex::my-lex-bot",
    "SKey": "lex::my-lex-bot",
    "FriendlyName": "my-lex-bot",
    "Category": "platform",
    "SubCategory": "lex",
    "BotId": "RVLZVDH96L",
    "BotAliasId": "BGWOQMHSSI",
    "LocaleId": "en_US",
    "Uid": "fTDAukqsgvLGWsP6sKq7Wn2t9jdb8s6SVcSvP7CYeSLnSSrUEakN9QejGfCkcnp8nnQRXFa7jZhvMRxs6PYh696EWxgcJYFdx8Qdfk8Fwa5kfH3d8Azge65gtL2k8nsu"
}
```

Once the platform configuration item has been created, create a channel configuration item similar to the below. 

```
{
    "PKey": "lex::my-lex-bot",
    "SKey": "lex::my-lex-bot::channel::msteams::my-teams-app",
    "FriendlyName": "my-teams-app",
    "Category": "channel",
    "SubCategory": "msteams",
    "SecretArn": "arn:aws:secretsmanager:ap-southeast-2:0123456789012:secret:msteams-app-s55QQ4",
    "Uid": "IHk52bThUa6qtUetJsK2kjQkr9NgR8KhebvGvjkN7BYZGmMdG6TX2mbAZ7TmrcHPgrFyqpf3wS6q475jwhNQNuqRd9sE6cDJrWPvvG3Z8UF5M9j9JUJJPe8HyF03Vbhf",
    "Timeout": 10,
    "BotId": "B04QK9CKM5U",
    "UserId": "U04QWED4B76",
    "IntegrationDirectory": null,
    "RespondProactively": false,
    "UploadAttachments": false
}
```

Based on the example above, the subscription URL will be:

```
https://9xih7nchid.execute-api.ap-southeast-2.amazonaws.com/v1/lex/my-lex-bot/channel/msteams/channelId/my-teams-app?channel_uid=IHk52bThUa6qtUetJsK2kjQkr9NgR8KhebvGvjkN7BYZGmMdG6TX2mbAZ7TmrcHPgrFyqpf3wS6q475jwhNQNuqRd9sE6cDJrWPvvG3Z8UF5M9j9JUJJPe8HyF03Vbhf
```


#### Microsoft Teams Integration with Amazon Connect

To integrate Microsoft Teams with an Amazon Connect instance with the alias ```my-connect-instance```, a platform configuration item similar to below would have to be created first ([refer to this link](../../platforms/connect/README.md)).

```
{
    "PKey": "connect::my-connect-instance",
    "SKey": "connect::my-connect-instance",
    "FriendlyName": "my-connect-instance",
    "Category": "platform",
    "SubCategory": "connect",
    "InstanceId": "dc669442-c20d-41d0-9ccb-5353b6001872",
    "Uid": "fTDAukqsgvLGWsP6sKq7Wn2t9jdb8s6SVcSvP7CYeSLnSSrUEakN9QejGfCkcnp8nnQRXFa7jZhvMRxs6PYh696EWxgcJYFdx8Qdfk8Fwa5kfH3d8Azge65gtL2k8nsu",
    "SnsStreamArn": "arn:aws:sns:ap-southeast-2:0123456789012:chat-sns-topic"
}
```

Once the platform configuration item has been created, create a channel configuration item similar to the below. 

```
{
    "PKey": "connect::my-connect-instance",
    "SKey": "connect::my-connect-instance::channel::msteams::my-teams-app",
    "FriendlyName": "my-teams-app",
    "Category": "channel",
    "SubCategory": "msteams",
    "SecretArn": "arn:aws:secretsmanager:ap-southeast-2:0123456789012:secret:msteams-app-s55QQ4",
    "Uid": "IHk52bThUa6qtUetJsK2kjQkr9NgR8KhebvGvjkN7BYZGmMdG6TX2mbAZ7TmrcHPgrFyqpf3wS6q475jwhNQNuqRd9sE6cDJrWPvvG3Z8UF5M9j9JUJJPe8HyF03Vbhf",
    "Timeout": 10,
    "BotId": "B04QK9CKM5U",
    "UserId": "U04QWED4B76",
    "AttachmentMessage": "You can download the attachment from the link below.",
    "DeclineAttachmentMessage": "Sorry but we need your consent to upload the file. Please download the file directly from the link below.",
    "ExpiredAttachmentMessage": "Sorry but this file is no longer available.",
    "ContactFlowId": "dce0db81-0e43-403e-bbgg-a6f9bdd03705"
    "IntegrationDirectory": null,
    "RespondProactively": false,
    "UploadAttachments": true
}
```
Based on the example above, the subscription URL will be:

```
https://9xih7nchid.execute-api.ap-southeast-2.amazonaws.com/v1/connect/my-lex-bot/channel/msteams/channelId/my-teams-app?channel_uid=IHk52bThUa6qtUetJsK2kjQkr9NgR8KhebvGvjkN7BYZGmMdG6TX2mbAZ7TmrcHPgrFyqpf3wS6q475jwhNQNuqRd9sE6cDJrWPvvG3Z8UF5M9j9JUJJPe8HyF03Vbhf
```