# DEPLOYMENT


## Deploy the CloudFormation Stack
1. Review the parameters required for deployment in the table below.

| Parameter Name | Required? | Accepted Values | Description |
|---|---|---|---|
| LoggingLevel | No | ```info``` (default), ```error```, ```warning```, ```debug``` | The logging level for the Lambda functions. |

2. Once done, identify the region to be used and click on the link to launch the CloudFormation service page. Regions listed below are for those that support Amazon Connect and Amazon Lex.

| Region | Launch Link |
|---|---|
| US East (N. Virginia) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://us-east-1.console.aws.amazon.com/cloudformation/home?region=us-east-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/cx-bridge-packaged.yaml&stackName=CXBridge) |
| US West (Oregon) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://us-west-2.console.aws.amazon.com/cloudformation/home?region=us-west-2#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/cx-bridge-packaged.yaml&stackName=CXBridge) |
| Asia Pacific (Seoul) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-northeast-2.console.aws.amazon.com/cloudformation/home?region=ap-northeast-2#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/cx-bridge-packaged.yaml&stackName=CXBridge) |
| Asia Pacific (Singapore) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-southeast-1.console.aws.amazon.com/cloudformation/home?region=ap-southeast-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/cx-bridge-packaged.yaml&stackName=CXBridge) |
| Asia Pacific (Sydney) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-southeast-2.console.aws.amazon.com/cloudformation/home?region=ap-southeast-2#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/cx-bridge-packaged.yaml&stackName=CXBridge) |
| Asia Pacific (Tokyo) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-northeast-1.console.aws.amazon.com/cloudformation/home?region=ap-northeast-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/cx-bridge-packaged.yaml&stackName=CXBridge) |
| Canada (Central) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ca-central-1.console.aws.amazon.com/cloudformation/home?region=ca-central-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/cx-bridge-packaged.yaml&stackName=CXBridge) |
| Europe (Frankfurt) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://eu-central-1.console.aws.amazon.com/cloudformation/home?region=eu-central-1#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/cx-bridge-packaged.yaml&stackName=CXBridge) |
| Europe (London) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://eu-west-2.console.aws.amazon.com/cloudformation/home?region=eu-west-2#/stacks/create/review?templateURL=https://jlyfish-nonprod-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/43443410/cx-bridge-packaged.yaml&stackName=CXBridge) |

3. Once the stack deployment has completed, refer to the main stack (i.e. not the nested stacks) and take note of the following CloudFormation outputs:
- ```BridgeRoleArn```
- ```BridgeRoleName```
- ```ConnectChatStreamProcessorLambdaArn```

![Get CloudFormation stack outputs](imgs/get-cloudformation-stack-outputs.png)

4. Continue the configuration based on the [platforms and channels](./00-toc.md) that are to be integrated.